var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1280" deviceHeight="800">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1280" height="870">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1677255252909.css" />\
      <div class="freeLayout">\
      <div id="t-Rectangle_28" class="rectangle manualfit firer ie-background commentable non-processed" customid="Background" rotationdeg="180.0"  datasizewidth="1280.0px" datasizeheight="800.0px" datasizewidthpx="1280.0" datasizeheightpx="800.0000000000003" dataX="0.0" dataY="0.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-t-Rectangle_28_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-ae13c4b1-02df-4827-bc48-80c2b10ef78a" class="screen growth-vertical devWeb canvas PORTRAIT firer ie-background commentable non-processed" alignment="left" name="Hours " width="1280" height="800">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/ae13c4b1-02df-4827-bc48-80c2b10ef78a-1677255252909.css" />\
      <div class="freeLayout">\
      <div id="s-Path_24" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="568.2px" datasizeheight="3.0px" dataX="690.0" dataY="436.5"  >\
        <div class="borderLayer">\
        	<div class="imageViewport">\
          	<?xml version="1.0" encoding="UTF-8"?>\
          	<svg xmlns="http://www.w3.org/2000/svg" width="568.1625366210938" height="2.0" viewBox="690.0 436.50000000000006 568.1625366210938 2.0" preserveAspectRatio="none">\
          	  <g>\
          	    <defs>\
          	      <path id="s-Path_24-ae13c" d="M691.0 437.50000000000006 L1257.1625532018988 437.50000000000006 "></path>\
          	    </defs>\
          	    <g style="mix-blend-mode:normal">\
          	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_24-ae13c" fill="none" stroke-width="1.0" stroke="#E3E1FF" stroke-linecap="square"></use>\
          	    </g>\
          	  </g>\
          	</svg>\
\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Group_32" class="group firer ie-background commentable non-processed" customid="Projects" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Rectangle_10" class="rectangle manualfit firer commentable non-processed" customid="Rectangle "   datasizewidth="440.0px" datasizeheight="311.0px" datasizewidthpx="440.00000000000034" datasizeheightpx="311.0000000000002" dataX="452.0" dataY="114.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_10_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_26" class="group firer ie-background commentable non-processed" customid="Circle 3" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Subtraction_1" class="path firer commentable non-processed" customid="Subtraction 1"   datasizewidth="139.0px" datasizeheight="179.5px" dataX="522.0" dataY="206.3"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="138.9749755859375" height="179.53778076171875" viewBox="522.02685546875 206.27349853515625 138.9749755859375 179.53778076171875" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Subtraction_1-ae13c" d="M599.5487060546875 206.27349853515625 L597.2461547851562 208.64984130859375 C632.5441589355469 220.61639404296875 658.17578125 254.29449462890625 658.0009155273438 293.39892578125 L658.0009155273438 293.412353515625 C658.0021057128906 293.677734375 658.0021057128906 293.94287109375 658.0009460449219 294.207763671875 C657.783447265625 342.8358154296875 617.6751708984375 382.811279296875 569.0006408691406 382.811279296875 C568.8681030273438 382.811279296875 568.7355346679688 382.81097412109375 568.6028747558594 382.81036376953125 C552.4873352050781 382.73828125 537.3325805664062 378.29168701171875 524.2682800292969 370.61041259765625 L522.02685546875 372.768798828125 C535.6953430175781 380.97332763671875 551.62646484375 385.7344970703125 568.5894470214844 385.81036376953125 C568.7266540527344 385.81097412109375 568.8638000488281 385.811279296875 569.0008544921875 385.811279296875 C619.3280639648438 385.811279296875 660.7760620117188 344.4993896484375 661.0009155273438 294.22119140625 C661.0021362304688 293.9495849609375 661.0021362304688 293.677734375 661.0009460449219 293.4056396484375 C661.1765747070312 253.47576904296875 635.3408813476562 219.03204345703125 599.5487365722656 206.27349853515625 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Subtraction_1-ae13c" fill="#FFE373" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="shapewrapper-s-Ellipse_12" customid="Ellipse 1" class="shapewrapper shapewrapper-s-Ellipse_12 non-processed"   datasizewidth="12.0px" datasizeheight="12.0px" datasizewidthpx="12.0" datasizeheightpx="12.0" dataX="587.0" dataY="200.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_12" class="svgContainer" style="width:100%; height:100%;">\
                  <g>\
                      <g clip-path="url(#clip-s-Ellipse_12)">\
                              <ellipse id="s-Ellipse_12" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 1" cx="6.0" cy="6.0" rx="6.0" ry="6.0">\
                              </ellipse>\
                      </g>\
                  </g>\
                  <defs>\
                      <clipPath id="clip-s-Ellipse_12" class="clipPath">\
                              <ellipse cx="6.0" cy="6.0" rx="6.0" ry="6.0">\
                              </ellipse>\
                      </clipPath>\
                  </defs>\
              </svg>\
              <div class="paddingLayer">\
                  <div id="shapert-s-Ellipse_12" class="content firer" >\
                      <div class="valign">\
                          <span id="rtr-s-Ellipse_12_0"></span>\
                      </div>\
                  </div>\
              </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_25" class="group firer ie-background commentable non-processed" customid="Circle 2" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Subtraction_3" class="path firer commentable non-processed" customid="Subtraction 3"   datasizewidth="93.6px" datasizeheight="63.1px" dataX="519.0" dataY="243.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="93.6024169921875" height="63.07427978515625" viewBox="518.9999694824219 243.0 93.6024169921875 63.07427978515625" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Subtraction_3-ae13c" d="M569.0003967285156 243.0 C541.6460266113281 243.0 519.1227416992188 265.4495849609375 519.00048828125 292.77716064453125 C518.9998474121094 292.92376708984375 518.9998474121094 293.0704345703125 519.0004577636719 293.21728515625 C518.981201171875 297.65264892578125 519.5523071289062 301.96337890625 520.6393432617188 306.07427978515625 L523.1465148925781 303.70263671875 C522.3814086914062 300.3267822265625 521.9844055175781 296.81884765625 522.00048828125 293.22412109375 L522.00048828125 293.210693359375 C521.9998474121094 293.07049560546875 521.9998474121094 292.93048095703125 522.00048828125 292.79058837890625 C522.1153564453125 267.11322021484375 543.2989196777344 246.0 569.0006103515625 246.0 C569.0705871582031 246.0 569.140625 246.00018310546875 569.2106628417969 246.00048828125 C586.2192077636719 246.0765380859375 601.2030334472656 255.38275146484375 609.396240234375 269.1103515625 L612.6023864746094 268.65814208984375 C604.0404052734375 253.465576171875 587.7676391601562 243.08343505859375 569.2240905761719 243.00048828125 C569.1494750976562 243.00018310546875 569.0749206542969 243.0 569.0003967285156 243.0 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Subtraction_3-ae13c" fill="#E7A496" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="shapewrapper-s-Ellipse_11" customid="Ellipse 1" class="shapewrapper shapewrapper-s-Ellipse_11 non-processed"   datasizewidth="12.0px" datasizeheight="12.0px" datasizewidthpx="12.0" datasizeheightpx="12.0" dataX="607.0" dataY="268.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_11" class="svgContainer" style="width:100%; height:100%;">\
                  <g>\
                      <g clip-path="url(#clip-s-Ellipse_11)">\
                              <ellipse id="s-Ellipse_11" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 1" cx="6.0" cy="6.0" rx="6.0" ry="6.0">\
                              </ellipse>\
                      </g>\
                  </g>\
                  <defs>\
                      <clipPath id="clip-s-Ellipse_11" class="clipPath">\
                              <ellipse cx="6.0" cy="6.0" rx="6.0" ry="6.0">\
                              </ellipse>\
                      </clipPath>\
                  </defs>\
              </svg>\
              <div class="paddingLayer">\
                  <div id="shapert-s-Ellipse_11" class="content firer" >\
                      <div class="valign">\
                          <span id="rtr-s-Ellipse_11_0"></span>\
                      </div>\
                  </div>\
              </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_24" class="group firer ie-background commentable non-processed" customid="Circle 1" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Subtraction_2" class="path firer commentable non-processed" customid="Subtraction 2"   datasizewidth="115.9px" datasizeheight="91.6px" dataX="499.0" dataY="223.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="115.9080810546875" height="91.56072998046875" viewBox="499.0 222.99993896484375 115.9080810546875 91.56072998046875" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Subtraction_2-ae13c" d="M569.5007934570312 222.99993896484375 C530.9334106445312 222.99993896484375 499.1730041503906 254.6561279296875 499.0007019042969 293.1859130859375 C498.9997863769531 293.3934326171875 498.9997863769531 293.60125732421875 499.00067138671875 293.8092041015625 C498.9691467285156 301.01348876953125 500.0424499511719 307.984619140625 502.0600280761719 314.5606689453125 L503.7519836425781 309.32373046875 C502.58428955078125 304.3348388671875 501.97686767578125 299.1417236328125 502.0007019042969 293.81597900390625 L502.0007019042969 293.80255126953125 C501.9997863769531 293.60125732421875 501.9997863769531 293.40020751953125 502.00067138671875 293.19927978515625 C502.1655578613281 256.3197021484375 532.5863037109375 226.0 569.5010375976562 226.0 C569.6015014648438 226.0 569.7019958496094 226.000244140625 569.8025512695312 226.00067138671875 C585.6428833007812 226.071533203125 600.2597045898438 231.715576171875 611.7874450683594 241.050537109375 L614.9080810546875 239.740234375 C602.6859436035156 229.38177490234375 586.93701171875 223.0772705078125 569.8159790039062 223.00067138671875 C569.7108764648438 223.00018310546875 569.6058044433594 222.99993896484375 569.5007934570312 222.99993896484375 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Subtraction_2-ae13c" fill="#BECF7A" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="shapewrapper-s-Ellipse_13" customid="Ellipse 1" class="shapewrapper shapewrapper-s-Ellipse_13 non-processed"   datasizewidth="12.0px" datasizeheight="12.0px" datasizewidthpx="12.0" datasizeheightpx="12.0" dataX="502.0" dataY="314.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_13" class="svgContainer" style="width:100%; height:100%;">\
                  <g>\
                      <g clip-path="url(#clip-s-Ellipse_13)">\
                              <ellipse id="s-Ellipse_13" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 1" cx="6.0" cy="6.0" rx="6.0" ry="6.0">\
                              </ellipse>\
                      </g>\
                  </g>\
                  <defs>\
                      <clipPath id="clip-s-Ellipse_13" class="clipPath">\
                              <ellipse cx="6.0" cy="6.0" rx="6.0" ry="6.0">\
                              </ellipse>\
                      </clipPath>\
                  </defs>\
              </svg>\
              <div class="paddingLayer">\
                  <div id="shapert-s-Ellipse_13" class="content firer" >\
                      <div class="valign">\
                          <span id="rtr-s-Ellipse_13_0"></span>\
                      </div>\
                  </div>\
              </div>\
          </div>\
        </div>\
\
        <div id="s-Mask_9" class="clippingmask firer ie-background commentable non-processed" customid="Picture"   datasizewidth="0.0px" datasizeheight="0.0px" dataX="545.9" dataY="272.9"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="52.24379348754883" height="52.468482971191406" viewBox="544.8783072144792 271.8554041323751 52.24379348754883 52.468482971191406" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <clipPath id="s-Path_35-ae13c_clipping">\
            	        <path d="M545.8785605970247 298.0896428918887 C545.8169284900152 311.87096652564117 557.1065286487848 323.26199780532534 570.8878522825373 323.32362991233487 C584.7541477609277 323.38564202593597 596.1838514165846 311.95593837027906 596.1218393029835 298.08964289188873 C596.183471409993 284.3083192581361 584.8938712512235 272.91728797845195 571.1125476174709 272.8556558714424 C557.2462521390806 272.7936437578414 545.8165484834237 284.2233474134983 545.8785605970247 298.0896428918886 Z " fill="black"></path>\
            	      </clipPath>\
            	    </defs>\
            	    <image xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADIAJ8DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD1fbRsqTFLipAi2Uu2pMUYoAj20bakxRigCPbS7afijFAxm2k2Cn45oHegBm0Uu2nUZoAbto206jFAhu2jbT8UYoAZsFJsqTFGKAI9tJ5YqXFGKADFLTgKKAG4oxTqKAG4op1NoASkZ1VSSeByaDycdu9U9Ra1W3KXG5t/ComSzH2A5oAnMwK/KwPI6HpUm4bST6ZxXj2rfEe4sJmltLC8ayVzGXuAMZBwRwSfx96XxN8SIm0mxuNKuM3EgO6Mk8DHU/Q/rQB6dqms6folm93qNxHDEnLMx/QDqTXlesfGe6eUxaRYwW6Z+WW6y7n/AICOB+teZ6rr1xqcscuoXFxPJnLlnz3/AIR0HFRW3iS606NodOKxRueZGiRpSP8AeI/lQB6Hpfxd8QWtyrajBBd2pOGxH5bY9j0z9a9p0vU7bV9Ngv7N99vMu5T/AEPoRXzba58QRvDFqE7XrqStvOF/e4GcBlxzjoCO1df8MdQv9HFnK8xbTL67ezljbpHIMbWHpnOKAPcRzRSCn0wCjFLRQAmKbin4oxQAUYpaMUAJRTsUlACUlKajZgAcnFADJJUhiaWRlRF6s3AFef698TdG0l3WGIT3B43d/wDHH5VZ+IV9cNo7Q2ZZM8FzwFz/ABf0r55xFNczpdzmBgCRuBO5/Q9x9aQD7nU9/nMM+dI5bzQ5Bwe2OmKzYp/LfIzt9KHXJOeQPSkRdpyMYHNMQ8wPKN2QS3PXtT0ihU7Wbceny0STfIBtGR6VZh0yaWwa7jKbU+8gJLY9enT8aBmtoEGnxQzXWoXW0QA+VBCT5kkh+7j0A65/CvRPCWkS3c1lokLmVbW+W/vZFbKRYX7mRwWJ4x9TXJ+GvAmrajIl7LBHHp6IswkkwySAngDHXP6d6+hNE0a20TTltbZFUDk7VwPoBSGaABz1p4oFOFMQmKMU6igBMUmKdRigBBRS0UAGKTFLRQAztVDUUnERntnVZowflbo49Pr6GtHpVadtkZJ7DJoA8s8ZeP4bCyaLTpf9PLDKTxbgo/i3Z6//AFq8Qvr+S9u2nmjgSQnrFEFU/gK6nx9rSan4nvnjbcglKIcfwjjH55rlUiHU4IPrSAgClurY9hTlgPld/WrZgRdpXPvU6xMSvljeORimFjKCcE/WrWn6ld6U0otpAPNTZIrKGDA9iKnNsYgGKn5uSMdBzVNIC7M4yRnAoCx3nh3xzq0WmQ6HZW1uIE2jLgnOCDz9T19c9q9j0TTdfvrpNQ129kjEZyttGdu4/wC0BwAPTknue1fNNhPcWlx5sErRt0J9R6V7p4G1vxXqmgLdWlxpuoxwt5T29wpilBHbepPbGCRSA9QXPenAZrN0bV4tWWdTBJbXVs/l3NtLjdG2MjkcEEcgjrWoKYBiilooATFGKXFFACYoxS0UAJijFLRQAwiszWmEejXzM+wCF8t/d+U81qnpXKfEC9/s/wAD6xNkgtbGNT7t8o/nQB8vTEzyu2c5OTmpre3eQ7U5qCPc8gCLkk9q9L8JeFzdgSzLtjA5AHJNRKSirs0hBzdkcbDo91IPu8dyTxWhaaON4Mr+WR1Ir2ZfC9k9rshQwt6r3+tUE+HyvMGmuDsHIRFx+Z5NYe2udHsEjhZtIsJIHAu1JZQCQufwFVrzwvDY6aXjIYtgkjqB6Yr1lfB9hCqYhyF4xjii+0K3eyeIIFGcjjpUe2aNPZRZ8+TQxRZUAcetdp8JfEJ0nxUdJdj9n1AbQPSQfd/PkVS8W+HHtQ06RYKk7sVyGjXEtvrtpcRsVkinRh9Qa6oSUldHHOLi7M+r9LsVt7i8uyd015JvdsY4UbVA9hg1qBaigwUXAwMCrAFWZhijFOApcYoAbikxUmKQigCGjFPAooAZijFPooAj7V558YY5ZvBT7PuJOhce3P8AXFeikcHFct47tjdeEb+ILuBVd/b5QeaTdlccVd2PDPB+grf3W8puA68dPxr2DSbNbRAi8DAz7muC+HZVDfxJyqOMMe4ruJNZitJfJjtri7nAyY4EyQPc9BXFVbcrHfQSUbnRwjaOBVtXx2xXJDxtp9s+zU7W8sG9ZYiV/MV0VjqVlqNv59nOk8X95DmpSaG9S6X46VSuDvzxU1xe21pC01zMkMa9Wc4ArmLvxno3mlLOWa+kHVbaIt+tDvLYI2TItdsEv7KaIqNzLgGvF9J03z/G1nZeUCTdKGA9jk/yr2m31aDUyyok0My/einUqwHrjuK5TwdDYf8AC19REzbDHuEA/vMccZ/OtcPdNpmWJs0mj2mEYQDrgdqsLxUUSDHAqcDAx2rrOMWiiigAooooAjopwWl2igBlFP2ijAoAZXMeM7z7PoV0ijPmLsc+i9/0rppMBDWBrlkL6zMLDIklQY9gQT+goA8z8G6VcaTPfQ3cRjmdkYKfQjI/Q1seIIte8nbo0sMYONzH7y/TtXUeItOMV+uoIRscLGy+h5wfyp9iizR4IBrgqXUz0qWtO55jY6T4tmuSbzUrshcnbIitHJ6DHbP1rutFiNm0SfZktXm5kVAACw78V0K2MMfO1fyrHnJk1VFHG08AdqmTvuhwS1sUvEuLmI27Wq3ZVsqrLlQfU15/Ivimyv2gsr5goOV8u12R7cE9Oue1enqhW/cMNwJwQa0BpsJOQmPbJog7dAkr9Tk9Ek1S6sBNq9usVyoxkEEmufm8L3667e6lbZSaWRprfBxuCqDj8eSPpXf6hthXy1GOMVsQWYcWGV+5EM/98nP/AKFj8a2w+smY4nSKHeG9T/tbRLa7xh2XbIO6uDhh+YrZFc7oUf2PV9TshwrFLkD0JG1vzKZ/GuiFdZxBRSgZp20YoAZRTgvrRtoAbRRRQAUUYooAbIu5CKpMgeZc/wAJHFaAXNV5U2Tq3ZuPxoAzPEUZbSQR/DIuayLBio449a6m5t1u7SaDIG4cZ7HtXMxRbZGicbW6H2PeuLFJqSZ34WS5HEmmvDscRHkDr71i2QVdQjaSVtzfe3etR6pNfaZdpLsEunnAby1y8Z9cZ5FadlFFfR+fbGC6VejRyYI5x0PT8awUZPU6PdiiK8jX7edkrHvkdquW1828wyt8wGVP94VBqDR2QImkjjfsine7dPy696oadbXtz+9vWUKRuCquNvPr9KJJrUatJFy7HnTdc12CRBEHrgCsHTLMT3YYj5EO45/QV0Z4HNdeFT5W2cWKldqPYxoVx4tkI/isQT/38IrbC+tZ1om7V55v+mCr9BuOP5Z/GtOuk5AxiiiimA7FFFABpARqKeBgUlOzTAbt5p2KWikAlMkTehX8vY0+jvQBWJYLvAyTwR71zOtTGLVgNjIGiB5HcEjNdJc3VtZxtLczLEi85Y4rjL3URqurGZAREAFTPXHrXPiWlCx04VPnuWWZLuFlfnjkVBFp0AXgK2e5UZ/OlWJ4nyvKntWhbrAozhs+lcUWd93FaFKSyh3AkAn6Af8A66kaQIhGOB1xVuWIO37tME9Se1V7iEQwMOpxyaHqJyvuWvDFxNcafP8AaeJorphgDHynBX9CK3G3MMHv0rzG38Q6joOtqfINzY3ISOQKPmjIyAw9eDj8BXolretcws7wm3cZCq5GfrXpUpJx0PMqRcZO5ctIxiSYf8tG4+g4H8v1qehFCIFXoowKcOtaGYAZp2KKKAADFFFFADO1JRRTAAadVOa+SM4jG9vbpVRry5b+6q+3WkBqSTJEuWP4Vi63fXcenM9rhHZgoJ5xnvUyqS2WJP1qLWTs0p5cZEeHP4HmoldRbRcEnJXOLksJZpvOupXlfqWc5Oat2UeZi3tT5LqKY5R12/WpLF4vtDqD2rzHdvU9PZaGrbKJIzmrKRFOlQ2eFyPXmrtXFaGUnqNC45PWqV4MqR61eyKo3fPfpTYo7mA9mJLoAD5vMUAY6812FxCGXH+1msTSo/tWsqBysILn69B/U/hXRyrlvbtXTho2i2Y4l3kkRRzSxHhsr6Gp11JQMyKQO2KrkffqIx7m5HA6V0HMakV7bzHCuAfQ8VYrCMIqaC4ltu5ZP7ppiNjFLimxSrNGGQ5FPoAr5FUZ52mJVDhP506ebd+6Q8dzQkQ6UAQpGPSpBCTkY61YVQDgU9EINIZVSP5Bx7VOIhJE8UgyGGCD3FLt2hh6GnJkdMGgDg7zw/awSOgYo6HoOOO1Q22ntC58tmPua7HWtLF4iXMYPnRdh/EvpWZCqAcDiuCrT5ZHo0qvNEisg6gBznHerzSshwaaqqBmkkO7qeKlKwPVkgYiMk9+lZl0/UscDvVzflduean07TjeTCeQfuEOV/2yP6fzpqLm7IXMoK7LOjWH2KyMrriaX5iD1HoPwH86tGrEvPA7VARXfFKKsjgbcndjIk3yY7bsmk28t7VLbrhnPpTMYRqYhhTapPtQ0f3RUjp8q+pIpWOGYk8DgUAQRO8EuY+c9RWrE4kjDjvVHb5Yx0dhlvanWkgjcox+VuefWmIjSDaAQOD3qxGmRz16U6MYG0/dPSnbCp/Q0ANK7QGHY07Jzmn7RtI9aYg+TBNADG/iI7gUfdxTh90H2/rSHg80gFR9pwcnNZ9/pe9jPbYDnllPRv8AA1o98805ScZpOKkrMqMnF3Ryss7QcTwSRH/aU4/PpUcc4uCVgSSZj2RSa7EEEdOKBgDArH2HmbrE+Rg2eiSykS3xCr2iU9f94/0FbLMqKETAHTinSPtHNQr6mtYxUVZGMpubuwPXHTFNwPrS56mlFUSNhHzOPU1F13D1NTwj5z9aSJMu2RQAj8bCegP9KbCm5t7/AHU6+7danKFgPWkVQQIl+6o59/U0CI0UuGmb+P7o9qZKmVz3qw7bn2r0UVCxycDt3pgTx/MpA4PUVKvK896KKAFxxTQvWiigBijKD8aY3PqaKKADt/WlXIPJoopAS8daa0mByfwoopgVz87Z5pw6UUUhjc+nWlU+tFFAD4OTShdsr/SiigRJghTt9OKgaTy0CJ8zt196KKYAVMce3I3N1NNYBTsUZ9aKKAP/2Q==" width="56.505893716254434" height="71.07659586950228" x="542.1100635404832" y="264.0628309453373" id="s-Image_10-ae13c" clip-path="url(#s-Path_35-ae13c_clipping)"></image>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_29" class="group firer ie-background commentable non-processed" customid="Total" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_59" class="richtext autofit firer ie-background commentable non-processed" customid="Total"   datasizewidth="37.0px" datasizeheight="18.0px" dataX="762.0" dataY="325.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_59_0">Total</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="shapewrapper-s-Ellipse_9" customid="Ellipse 1" class="shapewrapper shapewrapper-s-Ellipse_9 non-processed"   datasizewidth="12.0px" datasizeheight="12.0px" datasizewidthpx="12.0" datasizeheightpx="12.0" dataX="737.0" dataY="331.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_9" class="svgContainer" style="width:100%; height:100%;">\
                  <g>\
                      <g clip-path="url(#clip-s-Ellipse_9)">\
                              <ellipse id="s-Ellipse_9" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 1" cx="6.0" cy="6.0" rx="6.0" ry="6.0">\
                              </ellipse>\
                      </g>\
                  </g>\
                  <defs>\
                      <clipPath id="clip-s-Ellipse_9" class="clipPath">\
                              <ellipse cx="6.0" cy="6.0" rx="6.0" ry="6.0">\
                              </ellipse>\
                      </clipPath>\
                  </defs>\
              </svg>\
              <div class="paddingLayer">\
                  <div id="shapert-s-Ellipse_9" class="content firer" >\
                      <div class="valign">\
                          <span id="rtr-s-Ellipse_9_0"></span>\
                      </div>\
                  </div>\
              </div>\
          </div>\
          <div id="s-Paragraph_67" class="richtext manualfit firer ie-background commentable non-processed" customid="1250"   datasizewidth="53.1px" datasizeheight="18.0px" dataX="762.0" dataY="346.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_67_0">1250</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_28" class="group firer ie-background commentable non-processed" customid="Patterns" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_24" class="richtext autofit firer ie-background commentable non-processed" customid="Goal"   datasizewidth="33.9px" datasizeheight="18.0px" dataX="762.0" dataY="267.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_24_0">Goal</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="shapewrapper-s-Ellipse_8" customid="Ellipse 1" class="shapewrapper shapewrapper-s-Ellipse_8 non-processed"   datasizewidth="12.0px" datasizeheight="12.0px" datasizewidthpx="12.0" datasizeheightpx="12.0" dataX="737.0" dataY="271.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_8" class="svgContainer" style="width:100%; height:100%;">\
                  <g>\
                      <g clip-path="url(#clip-s-Ellipse_8)">\
                              <ellipse id="s-Ellipse_8" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 1" cx="6.0" cy="6.0" rx="6.0" ry="6.0">\
                              </ellipse>\
                      </g>\
                  </g>\
                  <defs>\
                      <clipPath id="clip-s-Ellipse_8" class="clipPath">\
                              <ellipse cx="6.0" cy="6.0" rx="6.0" ry="6.0">\
                              </ellipse>\
                      </clipPath>\
                  </defs>\
              </svg>\
              <div class="paddingLayer">\
                  <div id="shapert-s-Ellipse_8" class="content firer" >\
                      <div class="valign">\
                          <span id="rtr-s-Ellipse_8_0"></span>\
                      </div>\
                  </div>\
              </div>\
          </div>\
          <div id="s-Paragraph_32" class="richtext manualfit firer ie-background commentable non-processed" customid="1000"   datasizewidth="51.1px" datasizeheight="26.0px" dataX="762.0" dataY="287.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_32_0">1000</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_27" class="group firer ie-background commentable non-processed" customid="Icons" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_26" class="richtext autofit firer ie-background commentable non-processed" customid="Hours"   datasizewidth="45.0px" datasizeheight="18.0px" dataX="763.0" dataY="212.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_26_0">Hours</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="shapewrapper-s-Ellipse_10" customid="Ellipse 1" class="shapewrapper shapewrapper-s-Ellipse_10 non-processed"   datasizewidth="12.0px" datasizeheight="12.0px" datasizewidthpx="12.0" datasizeheightpx="12.0" dataX="738.0" dataY="216.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_10" class="svgContainer" style="width:100%; height:100%;">\
                  <g>\
                      <g clip-path="url(#clip-s-Ellipse_10)">\
                              <ellipse id="s-Ellipse_10" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 1" cx="6.0" cy="6.0" rx="6.0" ry="6.0">\
                              </ellipse>\
                      </g>\
                  </g>\
                  <defs>\
                      <clipPath id="clip-s-Ellipse_10" class="clipPath">\
                              <ellipse cx="6.0" cy="6.0" rx="6.0" ry="6.0">\
                              </ellipse>\
                      </clipPath>\
                  </defs>\
              </svg>\
              <div class="paddingLayer">\
                  <div id="shapert-s-Ellipse_10" class="content firer" >\
                      <div class="valign">\
                          <span id="rtr-s-Ellipse_10_0"></span>\
                      </div>\
                  </div>\
              </div>\
          </div>\
          <div id="s-Paragraph_29" class="richtext autofit firer ie-background commentable non-processed" customid="50"   datasizewidth="18.4px" datasizeheight="18.0px" dataX="762.0" dataY="233.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_29_0">50</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Paragraph_68" class="richtext autofit firer ie-background commentable non-processed" customid="by Emily Adams"   datasizewidth="97.1px" datasizeheight="14.0px" dataX="771.0" dataY="138.4" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_68_0">by Emily Adams</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_16" class="richtext autofit firer ie-background commentable non-processed" customid="Project progress"   datasizewidth="101.7px" datasizeheight="19.0px" dataX="475.0" dataY="136.4" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_16_0">Project progress</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_39" class="group firer ie-background commentable non-processed" customid="Meetings" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Paragraph_17" class="richtext manualfit firer ie-background commentable non-processed" customid="Active Hours"   datasizewidth="169.5px" datasizeheight="29.8px" dataX="261.8" dataY="440.5" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_17_0">Active Hours</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Rectangle_18" class="rectangle manualfit firer commentable non-processed" customid="Rectangle 2"   datasizewidth="804.6px" datasizeheight="336.7px" datasizewidthpx="804.5891113281255" datasizeheightpx="336.7258500108797" dataX="259.0" dataY="483.3" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_18_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Rectangle_21" class="rectangle manualfit firer commentable non-processed" customid="Rectangle "   datasizewidth="83.3px" datasizeheight="76.4px" datasizewidthpx="83.34210859064831" datasizeheightpx="76.41086596400714" dataX="934.9" dataY="501.4" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_21_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Rectangle_20" class="rectangle manualfit firer commentable non-processed" customid="Rectangle "   datasizewidth="107.4px" datasizeheight="299.2px" datasizewidthpx="107.35593648964857" datasizeheightpx="299.16796674043496" dataX="759.1" dataY="500.8" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_20_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_4" class="group firer ie-background commentable non-processed" customid="View all" datasizewidth="67.6px" datasizeheight="16.6px" >\
          <div id="s-Paragraph_56" class="richtext manualfit firer ie-background commentable non-processed" customid="View all"   datasizewidth="85.9px" datasizeheight="19.4px" dataX="965.7" dataY="438.2" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_56_0">View all</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_8" class="path firer commentable non-processed" customid="Arrow right"   datasizewidth="19.3px" datasizeheight="14.5px" dataX="1037.8" dataY="442.1"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="19.267566680908203" height="14.473593711853027" viewBox="1037.7903631540225 442.0693958312054 19.267566680908203 14.473593711853027" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_8-ae13c" d="M1057.057928597691 449.310863827253 C1057.057928597691 449.0215363379073 1056.9255599557982 448.7416581118023 1056.6913995256798 448.53631586045105 L1050.0042771908181 442.4053342517685 C1049.7396591839304 442.17204569300105 1049.4648507331797 442.0693958312054 1049.1798507341502 442.0693958312054 C1048.5284398355918 442.0693958312054 1048.0602349390058 442.4893184771972 1048.0602349390058 443.05856672040784 C1048.0602349390058 443.3571839938941 1048.1926013720672 443.60913768274577 1048.3961893661813 443.7957727825358 L1050.686260525381 445.9234497775338 L1053.6379330270017 448.39637624111566 L1051.2766414111654 448.2656752713524 L1038.940505551919 448.2656752713524 C1038.2585564542435 448.2656752713524 1037.7903631540225 448.69494136746766 1037.7903631540225 449.310863827253 C1037.7903631540225 449.9174423306319 1038.2585564542435 450.3467094393129 1038.940505551919 450.3467094393129 L1051.2766414111654 450.3467094393129 L1053.6379330270017 450.21600745698396 L1050.686260525381 452.6889349331315 L1048.3961893661813 454.81668584542666 C1048.1926013720672 455.003234877131 1048.0602349390058 455.2551895785483 1048.0602349390058 455.5538600117346 C1048.0602349390058 456.1230657271853 1048.5284398355918 456.5429888794599 1049.1798507341502 456.5429888794599 C1049.4648507331797 456.5429888794599 1049.7396591839304 456.4404220480528 1049.9840111623273 456.2257358402952 L1056.6913995256798 450.07606885021426 C1056.9255599557982 449.87083190569723 1057.057928597691 449.59084736019224 1057.057928597691 449.310863827253 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_8-ae13c" fill="#9F97E0" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_11" class="group firer ie-background commentable non-processed" customid="Monday" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Rectangle_9" class="rectangle manualfit firer commentable non-processed" customid="Rectangle 2"   datasizewidth="83.3px" datasizeheight="76.4px" datasizewidthpx="83.34210859064814" datasizeheightpx="76.41086596400714" dataX="293.6" dataY="501.4" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Rectangle_9_0"></span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Group_17" class="group firer ie-background commentable non-processed" customid="Group 17" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Paragraph_51" class="richtext manualfit firer ie-background commentable non-processed" customid="1"   datasizewidth="11.9px" datasizeheight="24.6px" dataX="329.3" dataY="540.3" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_51_0">1</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_60" class="richtext autofit firer ie-background commentable non-processed" customid="MON"   datasizewidth="26.1px" datasizeheight="12.0px" dataX="319.2" dataY="524.4" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_60_0">MON</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
        </div>\
\
\
        <div id="s-Group_12" class="group firer ie-background commentable non-processed" customid="Tuesday" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Rectangle_14" class="rectangle manualfit firer commentable non-processed" customid="Rectangle 2"   datasizewidth="83.3px" datasizeheight="76.4px" datasizewidthpx="83.34210859064814" datasizeheightpx="76.41086596400714" dataX="441.7" dataY="501.4" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Rectangle_14_0"></span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Group_16" class="group firer ie-background commentable non-processed" customid="Group 16" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Paragraph_52" class="richtext manualfit firer ie-background commentable non-processed" customid="2"   datasizewidth="11.9px" datasizeheight="24.6px" dataX="477.4" dataY="540.3" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_52_0">2</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_61" class="richtext manualfit firer ie-background commentable non-processed" customid="TUE"   datasizewidth="32.1px" datasizeheight="16.8px" dataX="467.3" dataY="520.8" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_61_0">TUE</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
        </div>\
\
\
        <div id="s-Group_19" class="group firer ie-background commentable non-processed" customid="Wednesday" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Rectangle_19" class="rectangle manualfit firer commentable non-processed" customid="Rectangle 2"   datasizewidth="83.3px" datasizeheight="76.4px" datasizewidthpx="83.3421085906482" datasizeheightpx="76.41086596400726" dataX="606.7" dataY="500.8" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Rectangle_19_0"></span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Group_13" class="group firer ie-background commentable non-processed" customid="Group 13" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Paragraph_53" class="richtext manualfit firer ie-background commentable non-processed" customid="3"   datasizewidth="11.9px" datasizeheight="24.6px" dataX="643.1" dataY="540.3" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_53_0">3</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_62" class="richtext manualfit firer ie-background commentable non-processed" customid="WED"   datasizewidth="42.1px" datasizeheight="19.4px" dataX="627.3" dataY="520.8" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_62_0">WED</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
        </div>\
\
\
        <div id="s-Group_14" class="group firer ie-background commentable non-processed" customid="Thursday" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_55" class="richtext manualfit firer ie-background commentable non-processed" customid="4"   datasizewidth="11.9px" datasizeheight="24.6px" dataX="802.4" dataY="540.3" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_55_0">4</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_63" class="richtext autofit firer ie-background commentable non-processed" customid="THU"   datasizewidth="21.8px" datasizeheight="12.0px" dataX="794.4" dataY="524.4" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_63_0">THU</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_15" class="group firer ie-background commentable non-processed" customid="Friday" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_54" class="richtext autofit firer ie-background commentable non-processed" customid="5"   datasizewidth="8.5px" datasizeheight="18.0px" dataX="970.6" dataY="545.6" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_54_0">5</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_64" class="richtext manualfit firer ie-background commentable non-processed" customid="FRI"   datasizewidth="32.1px" datasizeheight="16.8px" dataX="960.5" dataY="520.8" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_64_0">FRI</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_34" class="group firer ie-background commentable non-processed" customid="Hours" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Group_8" class="group firer ie-background commentable non-processed" customid="Fri" datasizewidth="51.4px" datasizeheight="121.1px" >\
            <div id="s-Paragraph_45" class="richtext autofit firer ie-background commentable non-processed" customid="9:00 AM"   datasizewidth="48.9px" datasizeheight="14.0px" dataX="949.9" dataY="627.3" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_45_0">9:00 AM</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_46" class="richtext autofit firer ie-background commentable non-processed" customid="10:00 AM"   datasizewidth="53.9px" datasizeheight="14.0px" dataX="945.1" dataY="696.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_46_0">10:00 AM</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_47" class="richtext autofit firer ie-background commentable non-processed" customid="11:00 AM"   datasizewidth="50.3px" datasizeheight="14.0px" dataX="945.1" dataY="764.7" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_47_0">11:00 AM</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
\
          <div id="s-Group_9" class="group firer ie-background commentable non-processed" customid="Thu" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Paragraph_48" class="richtext autofit firer ie-background commentable non-processed" customid="9:00 AM"   datasizewidth="48.9px" datasizeheight="14.0px" dataX="778.9" dataY="627.3" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_48_0">9:00 AM</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_49" class="richtext autofit firer ie-background commentable non-processed" customid="10:00 AM"   datasizewidth="53.9px" datasizeheight="14.0px" dataX="774.2" dataY="695.4" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_49_0">10:00 AM</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
\
          <div id="s-Group_6" class="group firer ie-background commentable non-processed" customid="Wed" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Paragraph_43" class="richtext autofit firer ie-background commentable non-processed" customid="10:00 AM"   datasizewidth="53.9px" datasizeheight="14.0px" dataX="616.9" dataY="695.4" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_43_0">10:00 AM</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_44" class="richtext autofit firer ie-background commentable non-processed" customid="11:00 AM"   datasizewidth="50.3px" datasizeheight="14.0px" dataX="616.9" dataY="763.6" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_44_0">11:00 AM</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
\
          <div id="s-Group_5" class="group firer ie-background commentable non-processed" customid="Tue" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Paragraph_39" class="richtext manualfit firer ie-background commentable non-processed" customid="9:00 AM"   datasizewidth="63.0px" datasizeheight="36.3px" dataX="453.8" dataY="623.1" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_39_0">9:00 AM</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_41" class="richtext autofit firer ie-background commentable non-processed" customid="11:00 AM"   datasizewidth="50.3px" datasizeheight="14.0px" dataX="449.0" dataY="763.6" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_41_0">11:00 AM</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
\
          <div id="s-Group_7" class="group firer ie-background commentable non-processed" customid="Mon" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Paragraph_31" class="richtext autofit firer ie-background commentable non-processed" customid="9:00 AM"   datasizewidth="48.9px" datasizeheight="14.0px" dataX="304.3" dataY="627.3" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_31_0">9:00 AM</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_37" class="richtext autofit firer ie-background commentable non-processed" customid="10:00 AM"   datasizewidth="53.9px" datasizeheight="14.0px" dataX="299.5" dataY="695.4" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_37_0">10:00 AM</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_38" class="richtext autofit firer ie-background commentable non-processed" customid="11:00 AM"   datasizewidth="50.3px" datasizeheight="14.0px" dataX="299.5" dataY="763.6" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_38_0">11:00 AM</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Path_25" class="path firer ie-background commentable non-processed" customid="Path 17"   datasizewidth="80.4px" datasizeheight="3.0px" dataX="450.1" dataY="699.9"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="80.3740463256836" height="2.0" viewBox="450.1106231920023 699.8508530730735 80.3740463256836 2.0" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_25-ae13c" d="M451.1106231920023 700.8508530730735 L529.4846671150243 700.8508530730735 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_25-ae13c" fill="none" stroke-width="1.0" stroke="#ECEAFF" stroke-linecap="square"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_26" class="path firer ie-background commentable non-processed" customid="Path 17"   datasizewidth="80.4px" datasizeheight="3.0px" dataX="611.1" dataY="631.2"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="80.3740463256836" height="2.0" viewBox="611.1445279264751 631.2105836477792 80.3740463256836 2.0" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_26-ae13c" d="M612.1445279264751 632.2105836477792 L690.5185718494976 632.2105836477792 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_26-ae13c" fill="none" stroke-width="1.0" stroke="#ECEAFF" stroke-linecap="square"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_27" class="path firer ie-background commentable non-processed" customid="Path 17"   datasizewidth="80.4px" datasizeheight="3.0px" dataX="772.2" dataY="768.5"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="80.3740463256836" height="2.0" viewBox="772.1784326609497 768.4911224983682 80.3740463256836 2.0" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_27-ae13c" d="M773.1784326609497 769.4911224983682 L851.5524765839716 769.4911224983682 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_27-ae13c" fill="none" stroke-width="1.0" stroke="#FFFFFF" stroke-linecap="square"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
      </div>\
\
\
      <div id="s-Group_52" class="group firer ie-background commentable non-processed" customid="Top bar" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Paragraph_66" class="richtext manualfit firer ie-background commentable non-processed" customid="Together everyone achives"   datasizewidth="273.5px" datasizeheight="15.0px" dataX="308.0" dataY="51.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_66_0">Together everyone achives more</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_70" class="richtext manualfit firer ie-background commentable non-processed" customid="Hours"   datasizewidth="135.5px" datasizeheight="26.0px" dataX="202.0" dataY="44.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_70_0">Hours</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_51" class="group firer ie-background commentable non-processed" customid="Top Icons" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Union_10" class="path firer commentable non-processed" customid="Logout_icn"   datasizewidth="17.6px" datasizeheight="16.0px" dataX="1239.9" dataY="49.4"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="18.471176147460938" height="15.984161376953125" viewBox="1239.9095407089744 49.44858170667348 18.471176147460938 15.984161376953125" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Union_10-ae13c" d="M1252.8154555981241 54.03276951919389 C1252.719542369117 54.03276951919389 1252.623601794927 54.06938471909149 1252.550371395132 54.1426151188864 C1252.4040336488652 54.288939192562 1252.4040336488652 54.526213344548864 1252.550371395132 54.672674144138966 L1255.2700139052185 57.38857036416857 L1245.7643497899319 57.38857036416857 C1245.5574014457763 57.38857036416857 1245.3897754746251 57.55619633532007 1245.3897754746251 57.763117334292474 C1245.3897754746251 57.970065678448066 1245.5574014457763 58.13769164959939 1245.7643497899319 58.13769164959939 L1255.2700139052185 58.13769164959939 L1252.5541450303717 60.85358786962894 C1252.4745842207365 60.92123985211322 1252.4268942217661 61.01897153578284 1252.4225736828685 61.123238718164316 C1252.418130090648 61.22750590054625 1252.4574661362437 61.32890183872411 1252.5310246782333 61.40297993918858 C1252.6014795419644 61.47394068880334 1252.697105646551 61.51359120400241 1252.796765165615 61.51359120400241 C1252.8011540674697 61.51359120400241 1252.805556641916 61.51353651363638 1252.8099592163621 61.51337244253915 C1252.914226398744 61.50970818803131 1253.0123409149742 61.46267447345025 1253.0804167477932 61.383646894881394 L1256.4388427303259 58.02921330905133 C1256.4569999317687 58.011575666083615 1256.4733250059576 57.99213324104488 1256.4875718462463 57.97107745021492 L1256.4874351203316 57.971132140581005 C1256.4874351203316 57.971132140581005 1256.496841863248 57.95245538066291 1256.502461298333 57.94304863774664 L1256.5023245724185 57.94304863774664 C1256.5167081386217 57.92070762332088 1256.5268942192504 57.89601492316609 1256.532376928421 57.87000965423226 C1256.532376928421 57.858770784061676 1256.532376928421 57.84936404114569 1256.5417836713373 57.83812517097567 L1256.5416606180142 57.83812517097567 C1256.5513271401678 57.78991561353013 1256.5513271401678 57.74025676139138 1256.5416606180142 57.692047203946174 C1256.5416606180142 57.680808333775815 1256.5416606180142 57.67140159085983 1256.532253875098 57.660162720689584 L1256.532376928421 57.660299446603915 C1256.5268942192504 57.6342941776702 1256.5167081386217 57.60949209678364 1256.5023245724185 57.58715108235799 C1256.5023245724185 57.57774433944172 1256.4929178295024 57.56844697725711 1256.4872983944172 57.5590402343409 L1256.4874351203316 57.5590402343409 C1256.4737215111095 57.53828524052318 1256.4580390487188 57.51881547030109 1256.4405381316656 57.50104110141888 L1253.0804304203848 54.1426151188864 C1253.007268383547 54.06938471909149 1252.9113688271314 54.03276951919389 1252.8154555981241 54.03276951919389 Z M1241.2842517501956 50.44858170667351 C1241.0772897334487 50.44858170667351 1240.9095407089744 50.61620767782483 1240.9095407089744 50.82315602198037 L1240.9095407089744 64.05816985318944 C1240.9095407089744 64.1575969381986 1240.948999807893 64.25284021022475 1241.0192906005268 64.32314467544995 C1241.0895813931604 64.39331241476066 1241.1848246651866 64.43274416849675 1241.2842517501956 64.43274416849675 L1250.7113121372483 64.43274416849675 C1250.9181374280809 64.43274416849675 1251.0858864525553 64.26511819734543 1251.0858864525553 64.05816985318944 C1251.0858864525553 63.85135823494875 1250.9181374280809 63.683595537882525 1250.7113121372483 63.683595537882525 L1241.6588534106854 63.683540847516895 L1241.6588534106854 51.19773033728717 L1250.7113121372483 51.19773033728717 C1250.9181374280809 51.19773033728717 1251.0858864525553 51.029967640221344 1251.0858864525553 50.82315602198037 C1251.0858864525553 50.61620767782483 1250.9181374280809 50.44858170667351 1250.7113121372483 50.44858170667351 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Union_10-ae13c" fill="#FFFFFF" fill-opacity="1.0" stroke-width="1.0" stroke="#3C2CD1" stroke-linecap="butt"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_50" class="path firer commentable non-processed" customid="Notification_icn"   datasizewidth="15.7px" datasizeheight="17.9px" dataX="1201.2" dataY="48.2"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="16.24847412109375" height="18.440032958984375" viewBox="1201.2282971653403 48.22064573646523 16.24847412109375 18.440032958984375" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_50-ae13c" d="M1212.98055658397 48.97064573646523 C1210.9178378668244 48.97064573646523 1209.2342030373206 50.65218093855483 1209.2342030373206 52.71379648924719 C1209.2342030373206 53.706382723717724 1209.629159163339 54.658754427512385 1210.3317384000513 55.36065745549936 C1211.034175294895 56.06256048348632 1211.9872943444102 56.456804900160336 1212.9805567197275 56.456804900160336 C1213.9738190950447 56.456804900160336 1214.9269026439413 56.06256051742573 1215.6295173133935 55.36065745549936 C1216.3319542082372 54.658754427512385 1216.7267679923866 53.70638265583892 1216.7267679923866 52.71379648924719 C1216.7267679923866 51.721067912907834 1216.3318118663683 50.76855386724432 1215.6295173133935 50.06679324900501 C1214.9269380766812 49.36489022101805 1213.9739613690347 48.97064580434402 1212.9805567197275 48.97064580434402 Z M1208.5281360514882 49.78529173791041 C1205.585431527825 50.01730799987821 1203.276052068348 52.46969810419151 1203.276052068348 55.463008454349996 L1203.276052068348 60.04674896568926 L1202.1240314692564 62.46894246774054 L1202.1241704761644 62.46894246774054 C1202.0507701300414 62.62338746816159 1202.061613808446 62.80452706717405 1202.1529466145876 62.9490963792319 C1202.244279779214 63.09353402336407 1202.4033156442993 63.18111264119971 1202.574166749953 63.18111264119971 L1206.0815466939764 63.18111264119971 C1206.1719083006813 64.70139846711328 1207.443074160358 65.91068013870506 1208.987951258054 65.91068013870506 C1210.5328283557499 65.91068013870506 1211.8031438042149 64.70139833135565 1211.8940712741514 63.18111264119971 L1215.4017001976574 63.18111264119971 C1215.5726900815425 63.18111264119971 1215.7315835877894 63.09339522816317 1215.822920333023 62.948957584030985 C1215.9141147024484 62.8045199398988 1215.9249583766107 62.62324510932303 1215.8515576783664 62.46894601440845 L1214.6996793532649 60.04675251235719 L1214.6996793532649 57.59911341579535 C1214.6996793532649 57.46704973121856 1214.6472688370363 57.34026732367281 1214.5538502104557 57.2468486843648 C1214.4602927929163 57.15329126682561 1214.3336491720868 57.10088074635467 1214.201446700794 57.10088074635467 C1214.0693830162172 57.10088074635467 1213.9426006086715 57.153291262583174 1213.8491819693634 57.2468486843648 C1213.7556245518242 57.340267306703126 1213.7032140313531 57.467049722733705 1213.7032140313531 57.59911341579535 L1213.7032140313531 60.15924643845404 C1213.7034920807655 60.233201956895805 1213.7201738707224 60.30632472110177 1213.7521475954352 60.373191103687184 L1214.6139121494073 62.18483614117341 L1211.9003235957707 62.18483614117341 L1211.9003235957707 62.184697098668906 L1210.9038582908286 62.184697098668906 L1210.9038582908286 62.18483614117341 L1207.0735169324703 62.18483614117341 L1207.0735169324703 62.184697098668906 L1206.0770516275284 62.184697098668906 L1206.0770516275284 62.18483614117341 L1203.3637478933867 62.18483614117341 L1204.2252277636212 60.373191103687184 C1204.257201488334 60.30632472110177 1204.2738837415438 60.233201965380644 1204.2741613277033 60.15924643845404 L1204.2741613277033 55.46304756951544 C1204.2741613277033 52.979962700338604 1206.1706127953719 50.97112428358095 1208.6084295563169 50.778949049125764 C1208.882706623628 50.75726240504392 1209.0876226092844 50.517458754850026 1209.0660562549422 50.24317099662579 C1209.055630024868 50.11138490245081 1208.993349876339 49.98918977896193 1208.8928421181442 49.90327666959783 C1208.7923343599498 49.81750235543466 1208.6617974056132 49.77510276337509 1208.5300219938663 49.785530060419454 Z M1212.98055658397 49.96601140465222 C1213.7102538812426 49.96601140465222 1214.4094878464616 50.25544187372024 1214.9252654069949 50.77076393348544 C1215.4410073650913 51.28597209260403 1215.7304449614346 51.98424513142028 1215.7304449614346 52.71280362586356 C1215.7304449614346 53.441255346935456 1215.4410144923665 54.13938600994349 1214.9252654069949 54.65484331824168 C1214.4095234488984 55.17005147736023 1213.7102539491216 55.459453437327205 1212.98055658397 55.459453437327205 C1212.251001628566 55.459453437327205 1211.5518744367184 55.17002296825919 1211.0361324446824 54.65484331824168 C1210.520390486586 54.139528419691146 1210.2309528902426 53.44121971055916 1210.2309528902426 52.71280362586356 C1210.2309528902426 51.19016905729589 1211.4559646619782 49.96597590403351 1212.9806988579599 49.96597590403351 Z M1207.0804145762486 63.17985267469069 L1210.895453063706 63.17985267469069 C1210.8088459953071 64.16062365162288 1209.9993460393298 64.9130972770017 1208.9880049569722 64.9130972770017 C1207.9768062164835 64.9130972770017 1207.1670358143494 64.16058801524656 1207.0804144404908 63.17985267469069 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_50-ae13c" fill="#3C2CD1" fill-opacity="1.0" stroke-width="0.5" stroke="#3C2CD1" stroke-linecap="butt"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
      </div>\
\
\
      <div id="s-Group_59" class="group firer ie-background commentable non-processed" customid="Menu lateral" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Rectangle_8" class="rectangle manualfit firer commentable non-processed" customid="Background"   datasizewidth="180.0px" datasizeheight="828.0px" datasizewidthpx="180.00000000000023" datasizeheightpx="828.0" dataX="0.4" dataY="44.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_8_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Rectangle_2" class="rectangle manualfit firer commentable non-processed" customid="Rectangle "   datasizewidth="180.0px" datasizeheight="83.0px" datasizewidthpx="180.0" datasizeheightpx="83.0" dataX="0.4" dataY="0.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_2_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_49" class="group firer ie-background commentable non-processed" customid="Support" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_10" class="richtext manualfit firer ie-background commentable non-processed" customid="Support"   datasizewidth="110.0px" datasizeheight="17.0px" dataX="50.8" dataY="383.7" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_10_0">Support</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_12" class="path firer commentable non-processed" customid="Support_icn"   datasizewidth="15.9px" datasizeheight="12.1px" dataX="20.9" dataY="383.7"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="16.666664123535156" height="12.903030395507812" viewBox="20.853364792988316 383.7333348592124 16.666664123535156 12.903030395507812" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_12-ae13c" d="M35.063914484946366 389.591979063047 L34.53120657782 389.591979063047 C34.47966043350391 386.6877863715981 32.10807448000341 384.3333333333335 29.186635170942225 384.3333333333335 C26.26519586188104 384.3333333333335 23.893619357665926 386.6877863715981 23.84206376406445 389.591979063047 L23.30935585693811 389.591979063047 C22.278187442843688 389.591979063047 21.45336326710941 390.4169567101013 21.45336326710941 391.4479716528757 L21.45336326710941 392.4962637670493 C21.45336326710941 393.5101550097446 22.278218172226957 394.352256356878 23.30935585693811 394.352256356878 L24.271691681628454 394.352256356878 C24.512277174270196 394.352256356878 24.70131959919243 394.16321393195574 24.70131959919243 393.92262843931405 L24.70131959919243 389.6779043890183 C24.70131959919243 387.2032169317289 26.7119781503753 385.19258881726853 29.186635170942225 385.19258881726853 C31.661292191509176 385.19258881726853 33.67195074269202 387.2032473684514 33.67195074269202 389.6779043890183 L33.67195074269202 394.1632199607681 C33.67195074269202 394.73032879790486 33.207921895117636 395.1771112034634 32.65805949999671 395.1771112034634 L31.231694827731985 395.1771112034634 L31.231694827731985 394.79903864537226 C31.231694827731985 394.1116953306786 30.68170969067438 393.56171025215315 29.99436643451284 393.56171025215315 C29.30702311981915 393.56171025215315 28.757038041293697 394.11169538921075 28.757038041293697 394.79903864537226 L28.757038041293697 395.60673912102743 C28.757038041293697 395.8473246136692 28.946080466215932 396.0363670385914 29.186665958857674 396.0363670385914 L32.658059617061014 396.0363670385914 C33.63767267168265 396.0363670385914 34.42812664678979 395.28019119302576 34.514052206889744 394.3178553683355 L35.064037343947376 394.3178553683355 C36.077928586642685 394.3178553683355 36.92002993377608 393.49300046321787 36.92002993377608 392.4618627785067 L36.92002993377608 391.4135706643332 C36.919910067588916 390.4169259221858 36.095052286721824 389.59197894598276 35.06391454347852 389.59197894598276 Z M22.31255822879237 392.4962944379004 L22.31255822879237 391.4480023237269 C22.31255822879237 390.8980171866692 22.759340575818783 390.4512655690261 23.30929498349309 390.4512655690261 L23.842002890619455 390.4512655690261 L23.842002890619455 393.4758153629043 L23.30929498349309 393.4758153629043 C22.759309846435485 393.4759352597865 22.31255822879237 393.0290330158779 22.31255822879237 392.49632510875153 Z M29.616232827380173 395.1944190457137 L29.616232827380173 394.8163464876225 C29.616232827380173 394.61016191035816 29.788133099960305 394.43827392953136 29.994305385471364 394.43827392953136 C30.200477670982423 394.43827392953136 30.372377943562526 394.61017420211147 30.372377943562526 394.8163464876225 L30.372377943562526 395.1944190457137 Z M36.060651590840024 392.4962944379004 C36.060651590840024 393.046279574958 35.61386924381364 393.49303119260117 35.063914836139304 393.49303119260117 L34.53120692901297 393.49303119260117 L34.53120692901297 390.45123478111066 L35.063914836139304 390.45123478111066 C35.613899973196936 390.45123478111066 36.060651590840024 390.8980171281371 36.060651590840024 391.4479715358114 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_12-ae13c" fill="#6C6C6C" fill-opacity="1.0" stroke-width="0.2" stroke="#6C6C6C" stroke-linecap="butt"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_53" class="group firer ie-background commentable non-processed" customid="Schedule" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Union_4" class="path firer commentable non-processed" customid="Schedule_icn"   datasizewidth="14.5px" datasizeheight="14.5px" dataX="22.0" dataY="343.5"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="14.504798889160156" height="14.50482177734375" viewBox="21.99744652186149 343.4790488603014 14.504798889160156 14.50482177734375" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Union_4-ae13c" d="M34.50580604881314 345.84650016813214 C34.72727240670012 345.84650016813214 34.938810611807895 345.93449555742757 35.095487236527916 346.090635243457 C35.251619103061856 346.24744219310196 35.33962752484976 346.4588396472908 35.33962752484976 346.6803294636643 L35.33962752484976 347.57871651399705 L22.998232472190097 347.57871651399705 L22.998232472190097 346.6803294636643 C22.998232472190097 346.4588396472908 23.08623568098102 346.24731708117406 23.242370154013447 346.090635243457 C23.399171890661382 345.93449555742757 23.61059019683824 345.84650016813214 23.832051341728228 345.84650016813214 L26.097114175557085 345.84650016813214 L26.097114175557085 345.9807452667776 C26.097114175557085 346.256908995647 26.321260013900105 346.4810678664825 26.597449807754426 346.4810678664825 C26.873642208107253 346.4810678664825 27.097788046450273 346.256908995647 27.097788046450273 345.9807452667776 L27.097788046450273 345.84650016813214 L32.929893598383075 345.84650016813214 L32.929893598383075 345.9807452667776 C32.929893598383075 346.256908995647 33.154039436726094 346.4810678664825 33.43022923058042 346.4810678664825 C33.706421630933235 346.4810678664825 33.930567469276255 346.256908995647 33.930567469276255 345.9807452667776 L33.930567469276255 345.84650016813214 Z M32.06367334601522 351.67838416769257 C31.78748355216089 351.67838416769257 31.563337713817877 351.90254303852805 31.563337713817877 352.17870676739744 L31.563337713817877 353.84649047038954 C31.563337713817877 354.12265419925893 31.78748355216089 354.3468130700944 32.06367334601522 354.3468130700944 L33.7314257710254 354.3468130700944 C34.007615564879714 354.3468130700944 34.231761403222734 354.12265419925893 34.231761403222734 353.84649047038954 C34.231761403222734 353.5702850375443 34.007615564879714 353.3461261667087 33.7314257710254 353.3461261667087 L32.56401158471107 353.3461261667087 L32.56401158471107 352.17870676739744 C32.56401158471107 351.90254303852805 32.33986574636805 351.67838416769257 32.06367334601522 351.67838416769257 Z M32.666235849307554 351.312515186513 C34.23123228402763 351.312515186513 35.501514540067475 352.582651478637 35.501514540067475 354.1478016967684 C35.501514540067475 355.71278509899605 34.23134696996155 356.98304650304783 32.666235849307554 356.98304650304783 C31.10123941458747 356.98304650304783 29.830957158547623 355.71291021092395 29.830957158547623 354.1478016967684 C29.830957158547623 352.5827765905649 31.101124728653556 351.312515186513 32.666235849307554 351.312515186513 Z M26.597449807754426 343.4790488603014 C26.321260013900105 343.4790488603014 26.097114175557085 343.7032077311369 26.097114175557085 343.9793714600063 L26.097114175557085 344.8456047448666 L23.832051341728228 344.8456047448666 C23.345668295991143 344.8456047448666 22.878724516052795 345.0390277854127 22.534523356887874 345.3826685474005 C22.190976428845975 345.72689316505193 21.99744652186149 346.19393599193324 21.99744652186149 346.6802043517364 L21.99744652186149 355.68626137027945 C21.99744652186149 356.1726548420106 22.190879988401548 356.6396142609399 22.534523356887874 356.98379717461535 C22.878724516052795 357.3273545286512 23.34578298192506 357.52086097714925 23.832051341728228 357.52086097714925 L28.50176542995251 357.52086097714925 C28.777957830305336 357.52086097714925 29.00210366864835 357.29674381028974 29.00210366864835 357.0205383774444 C29.00210366864835 356.744332944599 28.777957830305336 356.5202157777395 28.50176542995251 356.5202157777395 L23.832051341728228 356.5202157777395 C23.610584983841242 356.5202157777395 23.399046778733474 356.4321786844681 23.242370154013447 356.2760807024147 C23.086238287479517 356.1192737527697 22.998232472190097 355.9078345946049 22.998232472190097 355.68638648220735 L22.998232472190097 348.57940341738265 L35.33962752484976 348.57940341738265 L35.33962752484976 350.01598027760053 C35.33962752484976 350.2921857104458 35.56377075669428 350.51634458128126 35.83996576354561 350.51634458128126 C36.116155557399935 350.51634458128126 36.34029878924446 350.2921857104458 36.34029878924446 350.01598027760053 L36.34041347517838 350.0158968696485 L36.34041347517838 348.0898404434779 C36.34049167013332 348.0862539015445 36.34052816111229 348.08266735961115 36.34052816111229 348.0790391137018 C36.34052816111229 348.07541086779247 36.34049167013332 348.0718243258591 36.34041347517838 348.0682377839257 L36.34041347517838 346.6802043517364 C36.34041347517838 346.19381088000523 36.14698000863832 345.72689316505193 35.803334033653485 345.3826685474005 C35.459132874488574 345.0391528973406 34.9920770151148 344.8456047448666 34.50580604881314 344.8456047448666 L33.930567469276255 344.8456047448666 L33.930567469276255 343.9793714600063 C33.930567469276255 343.7032077311369 33.706421630933235 343.4790488603014 33.43022923058042 343.4790488603014 C33.154039436726094 343.4790488603014 32.929893598383075 343.7032077311369 32.929893598383075 343.9793714600063 L32.929893598383075 344.8456047448666 L27.097788046450273 344.8456047448666 L27.097788046450273 343.9793714600063 C27.097788046450273 343.7032077311369 26.873642208107253 343.4790488603014 26.597449807754426 343.4790488603014 Z M32.666235849307554 350.3118282831274 C30.54883897489083 350.3118282831274 28.83022594468748 352.03028231692235 28.83022594468748 354.1478434007445 C28.83022594468748 356.2652376686626 30.54866694598996 357.98385851836144 32.666235849307554 357.98385851836144 C34.78363272372427 357.98385851836144 36.502245753927625 356.2654044845665 36.502245753927625 354.1478434007445 C36.502245753927625 352.0304491328262 34.78380214612665 350.3118282831274 32.666235849307554 350.3118282831274 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Union_4-ae13c" fill="#3C2CD1" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_12" class="richtext manualfit firer ie-background commentable non-processed" customid="Hourss"   datasizewidth="110.0px" datasizeheight="17.0px" dataX="51.1" dataY="343.8" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_12_0">Hourss</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_55" class="group firer ie-background commentable non-processed" customid="Products" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_21" class="richtext manualfit firer ie-background commentable non-processed" customid="My Pags"   datasizewidth="110.0px" datasizeheight="17.0px" dataX="52.1" dataY="301.1" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_21_0">My Pags</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Union_6" class="path firer commentable non-processed" customid="Product_icn"   datasizewidth="15.9px" datasizeheight="14.8px" dataX="22.0" dataY="300.6"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="15.852066040039062" height="14.80975341796875" viewBox="21.9974394667222 300.55469564502 15.852066040039062 14.80975341796875" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Union_6-ae13c" d="M34.742864949807085 301.5887748378672 C35.297572652128 302.73621709227194 35.85452250360947 303.8835995560325 36.40927504891363 305.03104181043744 L24.338545461785486 305.03104181043744 C24.03113186421257 305.03104181043744 23.722791511653213 305.0264977214719 23.414496002077158 305.02440504892223 C23.965720899368648 303.98057998105713 24.515764931435683 302.93795072607753 25.06788668839141 301.8931092172597 C25.12050245535903 301.7917042845583 25.174104767957374 301.69023956121276 25.22761739458923 301.5887748378672 Z M31.138057110757757 306.0684692793645 L31.138057110757757 308.6764773922759 C30.879238359324518 308.4205136441071 30.62001602104243 308.16454989593825 30.360031352045723 307.9078088693938 C30.26019592375684 307.8079584934439 30.127804489656683 307.7586910025561 29.995278526606853 307.7586910025561 C29.86158664599361 307.7586910025561 29.72773034110861 307.8088553531081 29.626998053155432 307.9078088693938 C29.357387090429427 308.17076812294357 29.087806023025564 308.4329500981177 28.817851264094884 308.69507228264746 L28.817851264094884 306.0684692793645 Z M35.50790119104897 306.0684692793645 C35.90446263924554 306.0684692793645 36.30712273315879 306.0838354749446 36.70772004984434 306.0838354749446 C36.74233883288332 306.0838354749446 36.7769426682612 306.08371589365623 36.811516608316936 306.0834767310791 L36.811516608316936 314.3193390276239 L24.338545461785486 314.3193390276239 C23.94198401358892 314.3193390276239 23.539308972014624 314.3039728320436 23.13872660299012 314.3039728320436 C23.104107819951196 314.3039728320436 23.069503984573316 314.30409241333217 23.03491509685648 314.3043315759093 L23.03491509685648 306.0684692793645 L27.780378952184662 306.0684692793645 L27.780378952184662 309.9203021652624 C27.780378952184662 310.2285229365331 28.04127542850665 310.4310936393585 28.30933183448144 310.4310936393585 C28.435295774321276 310.4310936393585 28.56284416623464 310.386370237436 28.66563923141547 310.2868188147074 C29.10634112275278 309.8569838729683 29.54696827578482 309.42924160377896 29.98925461919555 309.00036331234827 C30.42194456419918 309.42930139442336 30.8550530437127 309.8569838729683 31.290284091098272 310.2868188147074 C31.391614285494256 310.38702793452325 31.518176131776954 310.43181112708993 31.64363185114047 310.43181112708993 C31.912121739286192 310.43181112708993 32.17552942266798 310.22666942656036 32.17552942266798 309.9203021652624 L32.17552942266798 306.0684692793645 Z M24.90888841758607 300.55469564502 C24.73256580760011 300.55469564502 24.545734991881716 300.65155648875583 24.45938235387831 300.8106593931888 C23.764181585157075 302.1456051080601 23.058651982636206 303.4770231749189 22.356709818772288 304.8117895178572 C22.295035769195977 304.9306533186888 22.231986534801194 305.049636700809 22.16931099193323 305.1685602922848 C22.064542835491068 305.26434490442347 21.99745773260736 305.40120568918377 21.99745773260736 305.54972564957865 L21.99745773260736 305.6315790416001 C21.99744278494626 305.63331297028407 21.99744278494626 305.6350468989685 21.99745773260736 305.63678082765244 L21.99745773260736 314.83808265740953 C21.99745773260736 315.1181420352238 22.236007455629192 315.35682628719513 22.51618641473192 315.35682628719513 L35.50790119104897 315.35682628719513 C35.9067047884061 315.35682628719513 36.30706294251456 315.3644794896631 36.70689792848543 315.3644794896631 C36.90335503793318 315.3644794896631 37.09966267077016 315.36262597969034 37.295611559741474 315.3571252404167 C37.300155648706834 315.357244821705 37.30469973767225 315.3573644029936 37.30927372195981 315.3573644029936 C37.31623933201854 315.3573644029936 37.323234837399525 315.35718503106085 37.330260238102596 315.35682628719513 C37.59993099147283 315.35682628719513 37.86274076841181 315.11826161651237 37.84898892022716 314.83808265740953 L37.84898892022716 305.54972564957865 C37.84898892022716 305.3642550710178 37.744295502090324 305.196901057674 37.59215820771624 305.10500283741334 C36.99151634292616 303.8626130398897 36.38824368978766 302.6202232423661 35.787990464185384 301.3777138635539 C35.6981400734914 301.18751982409475 35.60467234881844 301.00085343264794 35.51480701046347 300.8106593931888 C35.43533029655242 300.65155648875583 35.23814075171225 300.55469564502 35.06530094675577 300.55469564502 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Union_6-ae13c" fill="#6C6C6C" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_56" class="group firer ie-background commentable non-processed" customid="Dashboard" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_40" class="richtext manualfit firer ie-background commentable non-processed" customid="Dashboard"   datasizewidth="110.0px" datasizeheight="17.0px" dataX="52.1" dataY="261.3" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_40_0">Dashboard</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_43" class="path firer commentable non-processed" customid="Dashboard_icn"   datasizewidth="16.3px" datasizeheight="16.3px" dataX="21.4" dataY="259.7"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="17.065711975097656" height="17.066011428833008" viewBox="21.397448047740397 259.6547138974473 17.065711975097656 17.066011428833008" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_43-ae13c" d="M29.18647605967584 266.2044320609018 L21.99744652186149 266.2044320609018 L21.99744652186149 260.25471427891705 L29.18647605967584 260.25471427891705 Z M22.98901608581957 265.21283241118766 L28.19490649571773 265.21283241118766 L28.19490649571773 261.24625329779457 L22.98901608581957 261.24625329779457 Z M29.18647605967584 276.1206984111991 L21.99744652186149 276.1206984111991 L21.99744652186149 267.6920567193191 L29.18647605967584 267.6920567193191 Z M22.98901608581957 275.12909876148495 L28.19490649571773 275.12909876148495 L28.19490649571773 268.6835961975211 L22.98901608581957 268.6835961975211 Z M37.86316034166353 268.6835961975211 L30.67413080384921 268.6835961975211 L30.67413080384921 260.25495450564114 L37.86316034166353 260.25495450564114 Z M31.665700367807318 267.69208691990633 L36.87159077770542 267.69208691990633 L36.87159077770542 261.2465843559425 L31.665700367807318 261.2465843559425 Z M37.86316034166353 276.1207286117863 L30.67413080384921 276.1207286117863 L30.67413080384921 270.17101082980156 L37.86316034166353 270.17101082980156 Z M31.665700367807318 275.1291289620722 L36.87159077770542 275.1291289620722 L36.87159077770542 271.162549848679 L31.665700367807318 271.162549848679 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_43-ae13c" fill="#6C6C6C" fill-opacity="1.0" stroke-width="0.2" stroke="#6C6C6C" stroke-linecap="butt"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Path_15" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="181.4px" datasizeheight="3.0px" dataX="-1.0" dataY="233.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="181.3787078857422" height="2.0" viewBox="-0.9999991599279383 232.99999999999997 181.3787078857422 2.0" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_15-ae13c" d="M0.0 234.0000000000001 L179.37871580725903 234.0000000000001 "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_15-ae13c" fill="none" stroke-width="1.0" stroke="#BCBCBC" stroke-linecap="square" opacity="0.5"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_57" class="group firer ie-background commentable non-processed" customid="User" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_42" class="richtext manualfit firer ie-background commentable non-processed" customid="Monitor"   datasizewidth="81.2px" datasizeheight="15.0px" dataX="42.2" dataY="203.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_42_0">Monitor</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_50" class="richtext manualfit firer ie-background commentable non-processed" customid="Jacob Devies"   datasizewidth="86.0px" datasizeheight="16.4px" dataX="39.8" dataY="185.3" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_50_0">Jacob Devies</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Mask_10" class="clippingmask firer ie-background commentable non-processed" customid="User Picture"   datasizewidth="0.0px" datasizeheight="0.0px" dataX="48.8" dataY="109.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="70.00068664550781" height="70.30477905273438" viewBox="47.79848203109616 107.99999576045009 70.00068664550781 70.30477905273438" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <clipPath id="s-Path_23-ae13c_clipping">\
              	        <path d="M48.79882535385007 143.1523929162118 C48.715411544389156 161.80424110827738 63.99492415918321 177.22103210935262 82.64677235124857 177.30444591881354 C101.41362270050246 177.3883740348423 116.882753469879 161.91924326546558 116.79882535385009 143.1523929162117 C116.882239163311 124.50054472414622 101.60272654851693 109.083753723071 82.95087835645157 109.00033991361008 C64.18402800719767 108.91641179758135 48.71489723782114 124.38554256695804 48.79882535385006 143.1523929162119 Z " fill="black"></path>\
              	      </clipPath>\
              	    </defs>\
              	    <image xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADIASwDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD2QCnAU0U4V0GA4ClApBSg0gFApwFNBpQaQxwFLTc0uaBi5pM0owaNtIBM0uaMUlAC5opKM0AHNLRmjNABRRRQMWikzRmkAtGaTNFAD6XNMozQA/NGaSjNADqUU3NGaQx1FJmjNADqKQGjNAC0YozRmkMXFGKKBQAUUtFAGbQKMUorUyHUopBSikMKWiigBaUUlKKAClBpKKQx2aMU2lDYpALim1KHU9RQQjCi47EWaXNVr2+tNPiMt3dQ26gZzI4X+dcvcfEjw1bsVXUDKQcZjiLf/rouFmdjRmuFj+Knh0yFHa5UD+Ix9a2LHxv4e1DiLUoFb+7Idh/WgLM6LNFMRw4BUggjII70/NAhaKSloGFFFFAC0UlLQA6im06kACiloxQMKWkxS4oAM0oNJRQA7NGaQUUhj80bqbRQBSxRilpau5mJS0YpcUAJSijFLigYlLRRQAUZoopALupMikqOZgkZZiAFGSTQA24uobWB55nVIkGWZjgCvK/FXxY2M9roGDjg3br/AOgj+prn/iB4xuNbvDY2rlNOiJCgH/WEdWP9K8+kY5OMgepppFJFy+1W7v53nvLmSaRuSztk1QMzMeuBUXPOB9TTSwHrTKJRKSc5JPtU8TZJ4OR+Oapq4b/61PilKvwTQB6F4c8e3+h28NuH82BTxBKxxj0Hp+FevaH410bWo4VjuFiunH/HvIcNn0BPBr5wS7fbtBPTt1qaWY+V50ZaNx14IqeUp2Z7x4/8dr4WtIYLSNZNQuAWCueI19SP5CvJx8Rdea4MzvA5Jzjy8fketcjqOsXeq3AnvZ2llChSxOScDAzTFlKjanbvTUdBI9+8FeN59ZkS3ulPzEIC3VW9M9xx9frXoAyeRXzP4Q1m40/Wbd8loxIpZc+hr6et3SaJJE5DDOfXNZtuLsDinqiPmkqyUGKYUFFyeUiFOFO2Uu2ncVhoop2yjbilcdhKKXFLgUBYSjFLtFLtouFhtFP20bKLjsNop200baLhYoilFGKMVZnYWikpaBi0tJQKQC0YoooAMUlLSZoASqOsRGXRrxQSuYm5H0q9msXxVdG20JsHBkcL+HU/yoBK7seM32g20E+zzN5VQDkd6oTaBCTkd63ZcyyGVu5zVW9uRa25bPzEcVzubb0PS5IqOpxWrWq20pVO3tWOcn1rQ1G5eWRiSTz61lls10x2OGW44DPfFAbac9/WmE0b8ep+tUSWEkJPersc26IxO559DWWrmrUDjfgjNAyJxtY49afCxLYzU80G8EpyfvGq6Icg0xGjayeTOrAng1778MfEE2p6bNZXDl3t8MjH+6TjH514HAhOMA5r3/4UaGbPww16wxJeSZyf7q8D9c1nNmiWh3u/3pDIaf8AZs96T7MfWs9CdRBIacCT2pwgxUgXFAJDBRUmKNtAyOlAFP20baAG4FGBRtoxQAUooAFLigAzRmjFGKAM4UUUoq7mdgxSijFLii47Bil20BTTtvFK47DcU4LTCcUoegQ7bTdtP3Umam47DNlcN8RLwQx21uzBUClmJ7Z/+sP1rvga8x+LWmC4FhcNcMqElTGO+3n8etLmNYR1OGGpQTttifOOgpLuFb2DaSQRVaKW1cB3sFj+bb8nUH8K1I408rchOPesXo7nbH3lZnn+rWJtJjuJIPesdsEcHmux8Q2ck43gY29K5D7NK0m0DJrqhK6OKrG0rIj2E9OTSY5Oa7nQLd9LitoDDCZrpizuy5IHZR796XxXpFvNYvexosdxAwV8DG8H+tT7X3rF/Vnycxw6/Spo2AJOenOKi8pkweoNOKZXk49q1Oexfsz5k+GbCk/N9K9v8BaZ4Z1rQ20q9063mnhywZ1AfYxyMEc8GvCrM7W49u/Wu18O6rNptyt3DKVdWBz7YrOom1oaQPXovhF4YS6EoF5sB5i835T+OM4/Gu7traG0to7e3jWKGNQqIowFA7Vk6FrS6vpMN2owxGHX0Nai3S96zTBroWqKiEyHoacJFPequTYfRSbhRmgQtJS0UwEoxS0lIYmKMUtFACYopaKLAJilpaKBGaFpwWpttJtpcxXKNAp4AxQFpcYpXHYbTSaeaTFFxWIiM0Bal2UBMU7i5RoSnhOKUCndBU3KSKN9d2+nWkl1cyiOCIZZjXiviTxA/iPVWmIZbdPlijP8K/4nrXp/jfS7zVdEENmvmMsgd4wcFgAeleJ39vJZTMjJNBNGcNFKhVh+FQ3c6aUVubENhbx6bwMu77iSfaomHlrtHFTxMzWyE8HGcVUvZxDbsx7CovdnSkooyNWnVIyG59RWBpq+ZekoASBzT7y8Esjbv1o0mSK31Eg4AcDH1rdK0Tlb5pGpdxTyXkNvbozgJukBX5c545q34i3RaUqbt5cruI5yFHJ/M/pV+0IxN50gWMfr9KpajE2oAMrCMKMKMZwKyT1Ohr3X5nGKNwbj1wKqSEhvYGta9tGthnJJU9azJNrSbsbT6V1J3PPkrE4CqQy9j+FaNrL+4mG45wdtZSfMm0ZLdc+1bFpbt5caxguxYYAHODQwSPbvhZdG40e7J+6HXH5V3pIrmvAOhjRPDMEEoIml+dgfeurZFxwK500VJXZBmlDEd6GjI6CmYNWmZtEyzMO9SrcMe1VOalifaeRmmCLSzEjpUgfNMUgjpUg5pJjsLmjNGKTFPUQuaM0mKMYpXGOzRSCjOKpMQ6ikzRmi4itvFG4VCaTNZmpY3Ck3A1CH9aeMHvQA+k5BpMEUc0BYduFGaZSgGkA4HikLYFGw03YaQxu/mvAPiBZ+I7fxVcaleQMyMSLdl5XYOgH+HvXvxBzXP+NYTL4Svtqhiihj8m44B5x6HFCdmUkeK6Pra3sOyUbJVHT1rN1i8Lyna2VHQZq5dadClt9utwySKcurDBx347Vy9zcqZSytweq9aqMU3dGk5NKzI5Gzv3Ae/PSqjSGNsg8rzmiWUbsdeOtUXlIbitkjnbO303VhcqEfqAOa248MuOK4DRZm8/aM/hXc2mQnPSsJqzOqlPmWpT1Oz82Iggc964+6spYZCCp4r0fAYdKqXWlLOCyrkn070QnYmcOY57w14fGt3iW8c4iuGBOxlODjnrXtPhfwPaac0dw0HmSkbkaQ/KPw715pb6PcWktvOFZQZxGjdGyeR/I171o2rC8t44psC5UYPoxHWnJOaunoZfAadtbpDGi53bR1I6k96tHBqHPzc07PvUrTQl6jtgNHlKRSc460oammhakbwDtULQkVc3D1qNiDTvYLXKys6nrxVtXGOtQlcmk2GlcLFnd70Z96rYcd6N7LTuFiwWxRvqDzz3FOEy9xSuwsShs0/NRo6tUmRTTExRRxTc0lO4rFDfSbhUAkNG+nYdycGng4qqHxS+ZRYdy6rU7cKo+YaPNNLlDmLhcZpysKo+ZThJilyhc0A1GQapecfWlE9KzHoWiM1FLF5kTp03qV/MU1Zx3qTzk6ZqbMdz5h8UalcwaxqFm42ASFCoHTb8uP0ripWJkPPWvoD4hfDNtcuJNT0dl+1uS0tuxx5h9QfWvItV8B+JtN/eXGjXaof4lXePzXNbwasTK7ZyzE9zg1EcjrjmtlfDGtTSrFHpV0GbGA0ZX+dW7LwldSSE3Loux9roDk1baSuSotuyF8O2jL+/cfe6CuriJUdarQWiwDYozgelWgm31NcspXZ2whyosJJxzU8NyEfLGqQkCoc9qzJ7tpJBFFlmY7QB1JpJXHex09rcNrPiG1iQ/6NZnzD6Fu1dYLiVLyTy2I2sCMfSsDw5YjTrIbj87nLH1NbPmeWjs33nNdUFaJyTd2dfo3iSK9Y2t2wS4Xox4Df/XreIYV4/M8sU3noSGHORV+28UXVq4milZVYfMpY7QR1yKTimSj1As3rSbmx1rhrPx5MbvbNCJ4CMnYApH09auX3jq3t7gCC3SWHjJMmG/Ko5R6nW7m9aA5HesCw8YaVfKfmaJh1B5xW1BPDdR77eVZV9VOaTVgJfMb1pfMNNxSYqR6jzKaUOWqPFG4igCTqaXYcVEHIpwmYUtR6DihHSlBYdTTPOPpS+eO4paj0JlfAp3mVX3oR6U35fWkFkUc07NNxRXQYjs0ZoxRigLC5ozSYpcUBYXNLmmgU4CgApRQBTZZYraB553WOJBlmboKAJAKGKxqXkZUUdSxwK5PUPHNvDKYLGAytj/WPwv1A6n9K5S81i91KV2uZTKVU7VI4A9h2oGkz0O68V6RZyFPPMzjtEMj8+lc9qfi+7vNy2J+ywA4yP8AWH8e34VxSS+bgs+3GSWPYVZOpRGHdDbzMq/3sLmmrFWNe3mYtJdTsXcKWLMck1xl+hs5hdD7vST3B7/hWpBrD3bm3NuI1bqS+SaZdQOwMTpw2QDSm7qxUNHcqgpIucc1E64ziqtykumuVAJhHf8Au/X2pkd+JDhuK5bNHYpJkd5L5cZ5xV7w/p21ft0y/M/+rB7L6/j/ACqG3sDqd+FYZt0w0h9fQfjXTy7II/nZUUcc1rBGE30Jo5grxqfuqeafJOTIjLyMYrFl1GJMiI72+nFVFncdAR+NbJmNjoh85PXBHSs10+dgRgHr/jVEXc4+67j8aPOuX6yt+dK4WJpB5LbZOTj8KiMrNwOBSlS+C5LEdyaNoApDFikMcmRWzZXkqsFglaJvvAq2ORWIoyc1csnCTEt0IxVRYmejaX4qcMsOoxfJ90XK9j/tCusxkZHI9a8wtpZrdxuIkgkHUjIYdq7/AEO8iutORVPzRDaQT0HapqRtqgiy/g0mD6U8mm7qxuaWG80lOzRmi4WG0lOpKLhYZRmnYpuKLisR7KTbUgajIq7i5SPbRipODSbadxco3FGKdijFFxcomKUClpRRcLABWD4p1KKCxk0/ZvedPmOeEHb8at63rUWi2YcgPPJxHGT1PqfYVwdxfTXZaedt7yNlj/n2xVxFYwLsGNxKPXFWLdhJLn1GKmuId8UgPbkVRs2KS4qS0MlUwz4HQ1ciYKoz0NNvYsqGFRKcoKAGXNt5Moni6Z5xUk8pzFMjHnqM06KfAKtyDSuiOvHSgRZLQXEWJ4g4I6965q+0DE4OmS7lJG6Nzjyx659K21yBipYTmKQfxZFK1yk7FYMNNsEhtgGOcFyOp7mqEm+Zt0rFj6k1fvE2xQE5zvbP5VXxjNMkhSPHRafsOOaduoJNAAqAU8YFR0UAOLelCqWNCing4NAx2McCpI14I74pEjy2TU9uR57Z9eKtIkn0zUXtR5THdCW5U9K7HSr4WF0k4z5LjDgenrXn6x+auYzye1dXo5efQhn78TFee9UtVYTPSgVdQykMpGQR3pMViaFfbUjtXyVkXdEfQ91rbJFc0otM1i7iUmaCwpm8bsVJQ7NGaTNN3Cgdh9FR7qTfRYBgal3Cq+6l3VpYgn3elG6oQ1KGosBMGpd1RA0oNFgJBUF9fQadZtc3BIVeAB1Y9gKe0qRAGR1QE4BY4ya4zxnrEUsqWMJ3G3fdIwPG4jGPw/rTSEcxrOrz6pey3U5xnhUHRF9BRHJvgxVG8GX3D7rjIpbWQqQpqiTWK7hx/EprKKbJz9a1YT8g9jVa4i+YmkxoG+eAiqIO0mrUTYytRSJz9aAI9uelIjYUinim4xn60CJF+/TbJtzXGf7wpy8N9RRZABrj3IoAW/x5MGf75/lVLtV7UMfZ4M/3z/KqOKAEpKUdKAMmgAxSiigUDFHSnRjc+KUDipbZf3uT2poRNL8jBR7GoGYxkkdnz+tTTD92G7hhUEgJcj3NUwGbjHIdvTqK7bRGSTSt6kAu3zD0NcQRzzV/TdRmswyLh42+8h6U4sTR1uoxt5KBCV2HIKnGDWloWvTn/R71jKBwJD94fX1rl01fKbQWHpk5qaK92SB0AyatpSJTaPSTioiQGrK0G/lvLV1mddyNhBn5iuOv0zV+QkSrzXO42djZSuTZpM0wmmbqOUfMSZpM1HupN1HKLmGUtNzRmgB1LTc0oNADhUi1jarr0GmEQj95ctjCdlB7muduPHc8tqEt4FinY4Lg52/T3qlFsTaKuv6t9q1+dHb9zC3lRjsMdT9c1hXCNFK4wWVzkkdjVKWctcZY5LEkk9+avLcs0fBzgcg079CbEIlCkQuflPTPb2pwtyj7hyOxpjCGTJPyn8xU6Sbdu11OBhgT1pDLlsxbd7EVJIuSfcVHblS+VIIYYNTHnFAFB1KNkU0nc3NWpl4NU26mkA3GKT1oBoPFABuqW2GTIQeuP61ARmp7D5pZF/2R/OgQl8S1pblxz5pH6VTIwK0NTBFomO0/9DWcTkUAABIO1SfoKUgjsfyNT2hwHz7VZ8wDPNAGZvA60odT3H51peYuetKNhJ4U8egppAU0AIzkVPAuI2b3q0EQJ91c/QUbQIzjpuq0hXIpF3DHY1FIvzA9OuassOD6Cq+oabqEUytKuIHUNGF5BHv70NARKIWPc+/Spgio4x0NQKnkr8xAPuelSG5i8vOc+/aklYY4MNxq4k6WloZ5OdoJUZ/z3rFkuWP3AAPWpoplWymaZBIOM57VaZLRvQaqszQXEb+XKqhiBx0HUfhXeWN017p1ncvjfIgLY9e9eRQ3O2w80cbhtA9if8K7zw7qqQaV++4UNuJ9M5/oKbXMK9jrCaTNZw1uwlt3mhnEgTkjBB/Ws+fxNCq5jQsTSUGxOdjfLADk0Zri7nxJPJCybdpzwRTR4julUD2q/Ysz9qjr91AaiiuY6xQ1UtQ1ux0xW+0TDeF3eWOSf/10UUC6HmY1N77UZ5rg/NO2SfT0H4VVmzZ3LuV3Kxz/ALpooq7kEE0RkG+M8jmlt52VxngiiipKReaIZDD7pqRYUbuRRRTQy1BAIjlASx9auEcUUU2IhlHy1QbqaKKkRFmhjmiigBAasadj7XKPVM/rRRQBdvo99uo/2/6GsRl2sRRRQBPajKvUvUUUUACjPBp8Y+bmiiqQi5GgZcdxTwmYF9yTRRWgkBTfKiAfedV/M12V/bR3ugpAAPPRy0Z9u4ooo6hLTY8+mtSjFHXoeDjofSqEgzIfbgUUVDLFEQHMhxntT9qLFIpYhHUgn07/ANKKKEIqRkNDFFgvjBA/vEf0rftLwW9m8LNuIO5j2JP9KKKuBmw0qVftEhC5gcbNucdT1FWpoZbe5ML/ADbeQw6MD0NFFbxMZFaYjeaN3A+lFFbLYwe5/9k=" width="127.0" height="84.66666666666667" x="25.298485438842633" y="106.99999999999999" id="s-Image_5-ae13c" clip-path="url(#s-Path_23-ae13c_clipping)"></image>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_58" class="group firer ie-background commentable non-processed" customid="Logo" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_69" class="richtext manualfit firer ie-background commentable non-processed" customid="Dashboard"   datasizewidth="113.9px" datasizeheight="24.0px" dataX="49.4" dataY="32.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_69_0">Dashboard</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_28" class="path firer commentable non-processed" customid="Logo"   datasizewidth="20.0px" datasizeheight="20.2px" dataX="20.4" dataY="31.7"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="20.0" height="20.189903259277344" viewBox="20.420029526595556 31.692122413438497 20.0 20.189903259277344" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_28-ae13c" d="M27.08701347617344 31.692122413438483 L22.64267479269941 31.692122413438483 C21.415400726166922 31.692122413438483 20.42041026608902 32.68711296434165 20.42041026608902 33.91438694004887 L20.42041026608902 38.3587256235229 C20.42041026608902 39.58599969005539 21.41540081699219 40.58099015013329 22.64267479269941 40.58099015013329 L27.08701347617344 40.58099015013329 C28.314287542705927 40.58099015013329 29.309278002783827 39.585999599230135 29.309278002783827 38.3587256235229 L29.309278002783827 33.91438694004887 C29.309278002783827 32.687112873516384 28.31428745188066 31.692122413438483 27.08701347617344 31.692122413438483 Z M22.64267479269941 33.91438694004887 L27.08701347617344 33.91438694004887 L27.08701347617344 38.3587256235229 L22.64267479269941 38.3587256235229 Z M38.197764999985196 42.80287393725021 L33.75342631651114 42.80287393725021 C32.52615224997865 42.80287393725021 31.53116178990075 43.797864488153365 31.53116178990075 45.025138463860586 L31.53116178990075 49.46947714733463 C31.53116178990075 50.69675121386712 32.52615234080392 51.69174167394502 33.75342631651114 51.69174167394502 L38.197764999985196 51.69174167394502 C39.425039066517655 51.69174167394502 40.420029526595584 50.69675112304185 40.420029526595584 49.46947714733463 L40.420029526595584 45.025138463860586 C40.420029526595584 43.79786439732811 39.42503897569239 42.80287393725021 38.197764999985196 42.80287393725021 Z M33.75342631651114 45.025138463860586 L38.197764999985196 45.025138463860586 L38.197764999985196 49.46947714733463 L33.75342631651114 49.46947714733463 Z M35.975690843121555 31.882406421122738 C38.43028656862319 31.882406421122738 40.420029526595584 33.872149379095134 40.420029526595584 36.32674510459677 C40.420029526595584 38.78134083009843 38.43028656862319 40.771274157817544 35.975690843121555 40.771274157817544 C33.52109511761989 40.771274157817544 31.53116178990075 38.78134064844791 31.53116178990075 36.32674510459677 C31.53116178990075 33.872149379095134 33.5210952992704 31.882406421122738 35.975690843121555 31.882406421122738 Z M35.975690843121555 34.10467094773311 C34.74841677658904 34.10467094773311 33.75342631651114 35.099471038064294 33.75342631651114 36.32674510459677 C33.75342631651114 37.554019171129255 34.748416867414306 38.549009631207156 35.975690843121555 38.549009631207156 C37.202964909654014 38.549009631207156 38.197764999985196 37.554019080304 38.197764999985196 36.32674510459677 C38.197764999985196 35.099471038064294 37.202964909654014 34.10467094773311 35.975690843121555 34.10467094773311 Z M24.864368210069586 42.99315794493447 C27.318963935571247 42.99315794493447 29.308897263290362 44.9830914543041 29.308897263290362 47.43768699815523 C29.308897263290362 49.89209217225962 27.31896375392074 51.88202568162927 24.864368210069586 51.88202568162927 C22.40977266621846 51.88202568162927 20.420029526595556 49.89209217225962 20.420029526595556 47.43768699815523 C20.420029526595556 44.98309127265358 22.409772484567952 42.99315794493447 24.864368210069586 42.99315794493447 Z M24.864368210069586 45.21542247154484 C23.637094143537126 45.21542247154484 22.642294053205944 46.21041302244801 22.642294053205944 47.43768699815523 C22.642294053205944 48.6648181965524 23.637094143537126 49.659761155018884 24.864368210069586 49.659761155018884 C26.091642276602073 49.659761155018884 27.086632736680002 48.664770604115716 27.086632736680002 47.43768699815523 C27.086632736680002 46.210412931622756 26.091642185776806 45.21542247154484 24.864368210069586 45.21542247154484 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_28-ae13c" fill="#FFFFFF" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
      </div>\
\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;