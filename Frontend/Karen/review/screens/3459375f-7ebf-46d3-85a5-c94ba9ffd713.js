var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1248" deviceHeight="708">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1280" height="870">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1677255252909.css" />\
      <div class="freeLayout">\
      <div id="t-Rectangle_28" class="rectangle manualfit firer ie-background commentable non-processed" customid="Background" rotationdeg="180.0"  datasizewidth="1280.0px" datasizeheight="800.0px" datasizewidthpx="1280.0" datasizeheightpx="800.0000000000003" dataX="0.0" dataY="0.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-t-Rectangle_28_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-3459375f-7ebf-46d3-85a5-c94ba9ffd713" class="screen growth-vertical devWeb canvas PORTRAIT firer ie-background commentable non-processed" alignment="center" name="Principal inicio" width="1248" height="708">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/3459375f-7ebf-46d3-85a5-c94ba9ffd713-1677255252909.css" />\
      <div class="freeLayout">\
      <div id="s-Image_1" class="image firer ie-background commentable non-processed" customid="LatinShowLogo"   datasizewidth="648.0px" datasizeheight="648.0px" dataX="300.0" dataY="128.0"   alt="image">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
        		<img src="./images/1ac7d77e-a486-407f-b64e-893645c0411c.jfif" />\
        	</div>\
        </div>\
      </div>\
\
      <div id="s-Table_18" class="table firer ie-background commentable non-processed" customid="Basic menu"  datasizewidth="517.1px" datasizeheight="60.5px" dataX="365.4" dataY="42.0" originalwidth="517.1253341674806px" originalheight="60.5px" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <table summary="">\
              <tbody>\
                <tr>\
                  <td id="s-Cell_46" customid="Home" class="cellcontainer firer ie-background non-processed"    datasizewidth="129.3px" datasizeheight="60.5px" dataX="0.0" dataY="0.0" originalwidth="129.28133354187008px" originalheight="60.5px" >\
                    <div class="cellContainerChild">\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                    	  <div class="layout scrollable">\
                    	    <div class="paddingLayer">\
                            <div class="center ghostHLayout">\
                            <table class="layout" summary="">\
                              <tr>\
                                <td class="layout horizontal insertionpoint verticalalign Cell_46 Table_18" valign="middle" align="center" hSpacing="6" vSpacing="0"><div id="s-Paragraph_111" class="richtext manualfit firer mouseenter mouseleave ie-background commentable non-processed" customid="Home"   datasizewidth="40.0px" datasizeheight="19.0px" dataX="7.1" dataY="0.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_111_0">Home</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div></td> \
                              </tr>\
                            </table>\
                            </div>\
\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </td>\
                  <td id="s-Cell_67" customid="Sing In" class="cellcontainer firer ie-background non-processed"    datasizewidth="129.3px" datasizeheight="60.5px" dataX="0.0" dataY="0.0" originalwidth="129.28133354187008px" originalheight="60.5px" >\
                    <div class="cellContainerChild">\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                    	  <div class="layout scrollable">\
                    	    <div class="paddingLayer">\
                            <div class="center ghostHLayout">\
                            <table class="layout" summary="">\
                              <tr>\
                                <td class="layout horizontal insertionpoint verticalalign Cell_67 Table_18" valign="middle" align="center" hSpacing="6" vSpacing="0"><div id="s-Paragraph_115" class="richtext manualfit firer mouseenter mouseleave ie-background commentable non-processed" customid="Sing in"   datasizewidth="61.3px" datasizeheight="34.0px" dataX="11.5" dataY="0.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_115_0">Sing in</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div></td> \
                              </tr>\
                            </table>\
                            </div>\
\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </td>\
                  <td id="s-Cell_9" customid="Support" class="cellcontainer firer ie-background non-processed"    datasizewidth="129.3px" datasizeheight="60.5px" dataX="0.0" dataY="0.0" originalwidth="129.28133354187008px" originalheight="60.5px" >\
                    <div class="cellContainerChild">\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                    	  <div class="layout scrollable">\
                    	    <div class="paddingLayer">\
                            <div class="center ghostHLayout">\
                            <table class="layout" summary="">\
                              <tr>\
                                <td class="layout horizontal insertionpoint verticalalign Cell_9 Table_18" valign="middle" align="center" hSpacing="6" vSpacing="0"><div id="s-Paragraph_134" class="richtext autofit firer mouseenter mouseleave ie-background commentable non-processed" customid="Support"   datasizewidth="52.5px" datasizeheight="17.0px" dataX="16.8" dataY="10.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_134_0">Support</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div></td> \
                              </tr>\
                            </table>\
                            </div>\
\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </td>\
                  <td id="s-Cell_10" customid="Contact" class="cellcontainer firer ie-background non-processed"    datasizewidth="129.3px" datasizeheight="60.5px" dataX="0.0" dataY="0.0" originalwidth="129.28133354187008px" originalheight="60.5px" >\
                    <div class="cellContainerChild">\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                    	  <div class="layout scrollable">\
                    	    <div class="paddingLayer">\
                            <div class="center ghostHLayout">\
                            <table class="layout" summary="">\
                              <tr>\
                                <td class="layout horizontal insertionpoint verticalalign Cell_10 Table_18" valign="middle" align="center" hSpacing="6" vSpacing="0"><div id="s-Paragraph_133" class="richtext autofit firer mouseenter mouseleave ie-background commentable non-processed" customid="Contact"   datasizewidth="51.7px" datasizeheight="17.0px" dataX="0.8" dataY="0.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_133_0">Contact</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div></td> \
                              </tr>\
                            </table>\
                            </div>\
\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </td>\
                </tr>\
              </tbody>\
            </table>\
          </div>\
        </div>\
      </div>\
      <div id="s-Paragraph_1" class="richtext autofit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="1.0px" datasizeheight="1.0px" dataX="453.0" dataY="42.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_1_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;