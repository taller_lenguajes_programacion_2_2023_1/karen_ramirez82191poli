var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1280" deviceHeight="1000">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1280" height="870">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1677255252909.css" />\
      <div class="freeLayout">\
      <div id="t-Rectangle_28" class="rectangle manualfit firer ie-background commentable non-processed" customid="Background" rotationdeg="180.0"  datasizewidth="1280.0px" datasizeheight="800.0px" datasizewidthpx="1280.0" datasizeheightpx="800.0000000000003" dataX="0.0" dataY="0.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-t-Rectangle_28_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-f305af33-bfa4-400d-a093-dac3e98c8d4a" class="screen growth-vertical devWeb canvas PORTRAIT firer ie-background commentable non-processed" alignment="left" name="Dashboard" width="1280" height="1000">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/f305af33-bfa4-400d-a093-dac3e98c8d4a-1677255252909.css" />\
      <div class="freeLayout">\
      <div id="s-Group_12" class="group firer ie-background commentable non-processed" customid="Card 4" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Rectangle_5" class="rectangle manualfit firer commentable non-processed" customid="Rectangle "   datasizewidth="252.0px" datasizeheight="101.3px" datasizewidthpx="252.0" datasizeheightpx="101.3" dataX="1008.0" dataY="111.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_5_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_22" class="group firer ie-background commentable non-processed" customid="Icon" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="shapewrapper-s-Ellipse_4" customid="Ellipse " class="shapewrapper shapewrapper-s-Ellipse_4 non-processed"   datasizewidth="45.0px" datasizeheight="45.0px" datasizewidthpx="45.0" datasizeheightpx="45.0" dataX="1204.0" dataY="124.5" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_4" class="svgContainer" style="width:100%; height:100%;">\
                  <g>\
                      <g clip-path="url(#clip-s-Ellipse_4)">\
                              <ellipse id="s-Ellipse_4" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse " cx="22.5" cy="22.5" rx="22.5" ry="22.5">\
                              </ellipse>\
                      </g>\
                  </g>\
                  <defs>\
                      <clipPath id="clip-s-Ellipse_4" class="clipPath">\
                              <ellipse cx="22.5" cy="22.5" rx="22.5" ry="22.5">\
                              </ellipse>\
                      </clipPath>\
                  </defs>\
              </svg>\
              <div class="paddingLayer">\
                  <div id="shapert-s-Ellipse_4" class="content firer" >\
                      <div class="valign">\
                          <span id="rtr-s-Ellipse_4_0"></span>\
                      </div>\
                  </div>\
              </div>\
          </div>\
          <div id="s-Union_9" class="path firer commentable non-processed" customid="Icon"   datasizewidth="21.8px" datasizeheight="30.0px" dataX="1217.0" dataY="131.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="21.8306884765625" height="29.98796844482422" viewBox="1217.0 131.0 21.8306884765625 29.98796844482422" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Union_9-f305a" d="M1236.18603515625 131.0 C1236.044189453125 131.0 1235.9031982421875 131.03368377685547 1235.7744140625 131.0992889404297 L1231.9481201171875 133.04725646972656 C1231.50244140625 133.27446746826172 1231.3248291015625 133.81916046142578 1231.551513671875 134.2656478881836 C1231.7115478515625 134.5800552368164 1232.0303955078125 134.76016998291016 1232.360107421875 134.76016998291016 C1232.4984130859375 134.76016998291016 1232.638671875 134.72847747802734 1232.7698974609375 134.66170501708984 L1234.7763671875 133.64006805419922 C1231.7689208984375 142.81576538085938 1225.203369140625 144.16932678222656 1221.5938720703125 144.16932678222656 C1220.1951904296875 144.16932678222656 1219.2403564453125 143.96607208251953 1219.1083984375 143.93596649169922 C1219.0394287109375 143.9201202392578 1218.9705810546875 143.91252899169922 1218.9029541015625 143.91252899169922 C1218.4893798828125 143.91252899169922 1218.1201171875 144.1967544555664 1218.023193359375 144.61511993408203 C1217.9111328125 145.10205841064453 1218.2156982421875 145.58814239501953 1218.702392578125 145.7009506225586 C1218.73974609375 145.70970916748047 1219.8167724609375 145.94379425048828 1221.448486328125 145.94379425048828 C1225.4822998046875 145.94379425048828 1232.8980712890625 144.5094451904297 1236.335205078125 134.69857025146484 L1237.08544921875 136.556884765625 C1237.22802734375 136.90917205810547 1237.5667724609375 137.122802734375 1237.9259033203125 137.122802734375 C1238.0386962890625 137.122802734375 1238.152587890625 137.10216522216797 1238.2640380859375 137.0573501586914 C1238.727294921875 136.87057495117188 1238.9520263671875 136.34319305419922 1238.7645263671875 135.87876892089844 L1237.025146484375 131.56746673583984 C1236.9305419921875 131.33150482177734 1236.7412109375 131.14723205566406 1236.50341796875 131.05821990966797 C1236.400390625 131.01914978027344 1236.29296875 131.0 1236.18603515625 131.0 Z M1236.906005859375 142.0 C1236.4061279296875 142.00005340576172 1236.0 142.40528106689453 1236.0 142.90599822998047 L1236.0 159.4203109741211 C1236.0 159.92017364501953 1236.4058837890625 160.32562255859375 1236.906005859375 160.32562255859375 C1237.40576171875 160.32562255859375 1237.811279296875 159.9203872680664 1237.811279296875 159.4203109741211 L1237.811279296875 142.90653228759766 C1237.811279296875 142.40604400634766 1237.40576171875 142.0 1236.906005859375 142.0 Z M1226.9052734375 147.0 C1226.4053955078125 147.0 1226.0 147.4052276611328 1226.0 147.90594482421875 L1226.0 159.6020965576172 C1226.0 160.10195922851562 1226.4052734375 160.5074005126953 1226.9052734375 160.5074005126953 C1227.4051513671875 160.5074005126953 1227.8106689453125 160.1021728515625 1227.8106689453125 159.6020965576172 L1227.8106689453125 147.90594482421875 C1227.8106689453125 147.40545654296875 1227.4051513671875 147.0 1226.9052734375 147.0 Z M1217.9052734375 152.0 C1217.4053955078125 152.0 1217.0 152.4052276611328 1217.0 152.90530395507812 L1217.0 160.08265686035156 C1217.0 160.58273315429688 1217.4053955078125 160.98796844482422 1217.9052734375 160.98796844482422 C1218.40576171875 160.98796844482422 1218.811279296875 160.58273315429688 1218.811279296875 160.08265686035156 L1218.811279296875 152.90530395507812 C1218.811279296875 152.4054412841797 1218.40576171875 152.0 1217.9052734375 152.0 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Union_9-f305a" fill="#FFFFFF" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Paragraph_42" class="richtext manualfit firer ie-background commentable non-processed" customid="80%"   datasizewidth="53.2px" datasizeheight="18.0px" dataX="1022.0" dataY="142.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_42_0">80%</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_23" class="richtext manualfit firer ie-background commentable non-processed" customid="PROGRESS"   datasizewidth="123.3px" datasizeheight="13.0px" dataX="1022.0" dataY="124.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_23_0">PROGRESS</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_9" class="group firer ie-background commentable non-processed" customid="Group 9" datasizewidth="106.3px" datasizeheight="11.0px" >\
          <div id="s-Paragraph_74" class="richtext manualfit firer ie-background commentable non-processed" customid="Since last month"   datasizewidth="91.3px" datasizeheight="13.0px" dataX="1068.0" dataY="184.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_74_0">Since last month</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_73" class="richtext autofit firer ie-background commentable non-processed" customid="20%"   datasizewidth="23.2px" datasizeheight="13.0px" dataX="1036.0" dataY="183.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_73_0">20%</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_74" class="path firer commentable non-processed" customid="Arrow_icn"   datasizewidth="8.4px" datasizeheight="4.1px" dataX="1025.0" dataY="187.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="8.376322746276855" height="4.139999866485596" viewBox="1025.0 187.0 8.376322746276855 4.139999866485596" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_74-f305a" d="M1025.0 187.0 L1029.14 191.14 L1033.3763231412115 187.12222300170944 Z "></path>\
              	    </defs>\
              	    <g transform="rotate(180.0 1029.1881615706059 189.07)" style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_74-f305a" fill="#67B585" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
      </div>\
\
\
      <div id="s-Group_11" class="group firer ie-background commentable non-processed" customid="Card 3" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Rectangle_4" class="rectangle manualfit firer commentable non-processed" customid="Rectangle 2"   datasizewidth="252.0px" datasizeheight="101.3px" datasizewidthpx="252.0" datasizeheightpx="101.3" dataX="738.0" dataY="111.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_4_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_22" class="richtext manualfit firer ie-background commentable non-processed" customid="TOTAL"   datasizewidth="115.3px" datasizeheight="18.0px" dataX="754.0" dataY="124.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_22_0">TOTAL </span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="shapewrapper-s-Ellipse_3" customid="Ellipse 2" class="shapewrapper shapewrapper-s-Ellipse_3 non-processed"   datasizewidth="45.0px" datasizeheight="45.0px" datasizewidthpx="45.0" datasizeheightpx="45.0" dataX="929.0" dataY="124.5" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_3" class="svgContainer" style="width:100%; height:100%;">\
                <g>\
                    <g clip-path="url(#clip-s-Ellipse_3)">\
                            <ellipse id="s-Ellipse_3" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 2" cx="22.5" cy="22.5" rx="22.5" ry="22.5">\
                            </ellipse>\
                    </g>\
                </g>\
                <defs>\
                    <clipPath id="clip-s-Ellipse_3" class="clipPath">\
                            <ellipse cx="22.5" cy="22.5" rx="22.5" ry="22.5">\
                            </ellipse>\
                    </clipPath>\
                </defs>\
            </svg>\
            <div class="paddingLayer">\
                <div id="shapert-s-Ellipse_3" class="content firer" >\
                    <div class="valign">\
                        <span id="rtr-s-Ellipse_3_0"></span>\
                    </div>\
                </div>\
            </div>\
        </div>\
        <div id="s-Paragraph_39" class="richtext manualfit firer ie-background commentable non-processed" customid="$20,000"   datasizewidth="94.8px" datasizeheight="18.0px" dataX="754.0" dataY="142.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_39_0">$20,000</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_40" class="richtext autofit firer ie-background commentable non-processed" customid="15%"   datasizewidth="21.7px" datasizeheight="13.0px" dataX="764.0" dataY="184.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_40_0">15%</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_41" class="richtext manualfit firer ie-background commentable non-processed" customid="Since last month"   datasizewidth="96.3px" datasizeheight="13.0px" dataX="794.0" dataY="184.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_41_0">Since last month</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Path_73" class="path firer commentable non-processed" customid="Path 65"   datasizewidth="8.4px" datasizeheight="4.1px" dataX="753.0" dataY="188.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="8.376322746276855" height="4.139999866485596" viewBox="753.0 188.0 8.376322746276855 4.139999866485596" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_73-f305a" d="M753.0 188.0 L757.14 192.14 L761.3763231412115 188.12222300170944 Z "></path>\
            	    </defs>\
            	    <g transform="rotate(180.0 757.1881615706058 190.07)" style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_73-f305a" fill="#67B585" fill-opacity="1.0"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
        <div id="s-Path_1" class="path firer commentable non-processed" customid="Path 70"   datasizewidth="28.0px" datasizeheight="28.0px" dataX="938.0" dataY="133.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="28.00262451171875" height="28.000003814697266" viewBox="938.0000000000002 133.00000000000003 28.00262451171875 28.000003814697266" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_1-f305a" d="M938.0000000000002 147.00150262529928 C938.0040278980555 143.28929588345574 939.4805519137085 139.73034914579037 942.1052873420539 137.1052873420537 C944.730185802826 134.48060605082512 948.2894039506645 133.00407011746944 952.0015026252995 133.00000000000003 C952.5683573670248 133.00000000000003 953.0920128632296 133.3023026441398 953.3754131137774 133.79327101866662 C953.6588513534685 134.2840386304053 953.6588513534685 134.8888826706162 953.3754131137774 135.37959598996895 C953.0919748740863 135.87057523332425 952.5683573670248 136.17286700863556 952.0015026252995 136.17286700863556 C949.1308676134188 136.17604693084496 946.3785421799064 137.3180818198229 944.3493704167296 139.34812195063586 C942.3199272433797 141.37837914747965 941.1787826531744 144.13070458099202 941.1762318944084 147.00133979989798 C941.1762318944084 147.56862872544133 940.873505935279 148.09288123086546 940.3823096258928 148.37655278807372 C939.8909070675479 148.66020268525963 939.2856667809059 148.66020268525963 938.794248022831 148.37655278807372 C938.3030570961027 148.09290289088784 938.0003257543156 147.56862872544133 938.0003257543156 147.00133979989798 Z M953.6092640316665 159.410438673417 C953.6084160560227 158.9896310966568 953.4405155199705 158.5863962934759 953.1424514213238 158.28964549866004 C952.844387309738 157.99263964867006 952.4405175340727 157.82622479089213 952.0197045487765 157.82707680352922 C951.5988969720163 157.82792477917292 951.1956621688355 157.99582531522512 950.8989113740196 158.29388941387188 C950.6019055240296 158.59195352545768 950.4354906662517 158.99582330112298 950.4363426788888 159.4166362864192 C950.4371906545324 159.83723220568456 950.6050911905846 160.24046160032935 950.9031552892313 160.53742946117612 C951.2012194008172 160.83443531116612 951.6050891764825 161.0006385373274 952.0259021617787 160.99999815630696 C952.4464980810441 160.99915018066326 952.8497274756888 160.83124964461103 953.1466953365357 160.5331855459643 C953.4437011865256 160.2351214343785 953.6101160443036 159.8312516587132 953.6092640316665 159.410438673417 Z M958.2074355604068 137.83251092283334 C958.6740365391328 137.82827104461492 959.1150001253328 137.61882106062595 959.4130534198465 137.26012058327052 C959.7111175314324 136.90121388283453 959.8361946117074 136.4295114642109 959.7550006074089 135.9701121712097 C959.6738066031104 135.51072374703696 959.3946123045093 135.11025130836063 958.9916325823807 134.87536878338358 C958.5644645154607 134.6286065254082 958.0467842094475 134.59468749966098 957.5911838307679 134.78336410534607 C957.1353989407866 134.9720407175007 956.7932455301436 135.36210406624028 956.6656014396161 135.8384357518003 C956.5379790738065 136.3149953722196 956.6391022686813 136.82376988959925 956.9394989181893 137.21489699904433 C957.239896670754 137.60623571422784 957.7049834664092 137.83564229053297 958.1983017851577 137.83564229053297 Z M944.4401104354131 156.97912191413573 C944.23023579077 157.3437541581253 944.1738488005046 157.7768431487292 944.2834479574307 158.1830573824455 C944.3928354568618 158.58923906143275 944.6591025132454 158.93540850418455 945.0237510345995 159.14527227999923 C945.3883832785891 159.35514692464233 945.821472269193 159.41153391490766 946.2276865029093 159.3021464154765 C946.6339007366255 159.1927589160453 946.9800376246483 158.92649185966172 947.1901184664938 158.5618433383076 C947.399993111137 158.19721109431802 947.4563801014024 157.7643392215013 947.3472042594659 157.35790786999786 C947.2378167600348 156.95172619101055 946.9715497036511 156.60555674825878 946.6071182483279 156.39569297244412 C946.2429147537422 156.18581832780106 945.8104281550155 156.12879093063705 945.404485279716 156.2371201684724 C944.9983036007287 156.3452377423434 944.6516999741589 156.60980613670205 944.4400995148284 156.97297308147068 Z M941.2671890826354 154.8182899248099 C941.7363298995426 154.81744194916624 942.1811033206491 154.60883858221734 942.4817072704162 154.24866735828772 C942.7823166804758 153.88849071288288 942.9082403648518 153.4136080750575 942.8253476920588 152.95187514107744 C942.7426721047053 152.49015307592583 942.4596571864735 152.0886494959684 942.0526451548715 151.855449465634 C941.6456331232695 151.62224943529958 941.1560974414782 151.58112867207123 940.7158014083698 151.74309336567862 C940.2757007036353 151.90526972971986 939.9297483269144 152.25378907787598 939.7709530354366 152.69537684536647 C939.6123802314648 153.13674749506976 939.6571099244816 153.6260064099082 939.8934848431161 154.03113526171384 C940.033187808225 154.2715370295294 940.2335217305674 154.47102433483167 940.4747755627764 154.60922402451547 C940.7160239605712 154.7474454130389 940.9892864925381 154.81952198414817 941.2672325579491 154.81825205211842 Z M964.1334965772161 153.991751130245 C964.3448528113837 153.62669557126583 964.4023035105982 153.19272744771004 964.2935509577733 152.7854820727127 C964.1847984178874 152.37803044875662 963.9185313485647 152.03058024396387 963.5534649466352 151.8194139166951 C963.1884093876561 151.60826934002228 962.7544412641003 151.55103029830258 962.347195889103 151.65999449568324 C961.9397442651468 151.7689586930639 961.592511126385 152.03522576238655 961.3815619686598 152.40051469063928 C961.1706290494817 152.76557024961846 961.1133900077621 153.19953837317425 961.2225658626375 153.6069465735729 C961.3317417045738 154.0141865400342 961.5982204184522 154.36141427026 961.9634659490257 154.57214636195442 C962.3280981930153 154.78350259612205 962.761838433468 154.84095329533656 963.1688667424346 154.73198909795587 C963.5761067088958 154.62323655807003 963.9231173213345 154.35675783125254 964.1334153327234 153.991686020787 Z M966.0026249451159 147.00171813864037 C966.0026249451159 146.5809105618802 965.8355751019145 146.17745869266847 965.5379342923792 145.8798395302165 C965.2405051273996 145.5824103652369 964.8368687210078 145.4153605090964 964.4162728534988 145.4153605090964 C963.9954652767386 145.4153605090964 963.5918505821255 145.5824103522978 963.294394245075 145.8798395302165 C962.9969650800954 146.17748032681268 962.8297035923382 146.5809051015878 962.8297035923382 147.00171813864037 C962.8297035923382 147.4225311756929 962.9969650930344 147.82597758461227 963.294394245075 148.12359674706425 C963.5918234100544 148.42102591204383 963.9954598164462 148.58807576818435 964.4162728534988 148.58807576818435 C964.8366571152694 148.58743977290598 965.239663983591 148.41996221603375 965.5370660282557 148.12253848546843 C965.8344951932353 147.82532097798364 966.0017566809925 147.42234122997684 966.0026033109716 147.00174531071144 Z M961.3771550598385 141.5859080401636 C961.6608049570244 142.07625233691263 962.1842108065912 142.3785278089813 962.7506856568843 142.37917905883018 C963.2198264737916 142.37917905883018 963.6645998948982 142.17184968678262 963.9660722123012 141.81254138313577 C964.2677398839567 141.45342302520496 964.3947218558068 140.9792187834214 964.3133161940134 140.51748579768503 C964.2319105322201 140.05555210091677 963.9503826185183 139.65339183669624 963.5441955051168 139.41894349553723 C963.1382254577462 139.1844789028919 962.6491673574523 139.14165411438935 962.2084370887694 139.3019255739165 C961.7677013856724 139.46240326946327 961.4206473755545 139.80944101515578 961.2601696929468 140.25019296973915 C961.0999036548948 140.6909286728362 961.1427230348613 141.18000840727436 961.3771876145676 141.58595138608655 Z M958.2254533866326 159.3312574985112 C958.7181258899462 159.32956154722385 959.1821869787346 159.09891488934994 959.4811085373718 158.7071474247626 C959.779807621442 158.31537996017525 959.8798714646034 157.80701786307856 959.7514024817536 157.33128323849425 C959.6229334989039 156.85557024805416 959.2807746538466 156.46653802765655 958.8252231331388 156.2785451812626 C958.369861558147 156.0905089630675 957.852805407546 156.12463963337044 957.4262939472547 156.37118482531497 C957.0196889532779 156.6060727329499 956.7379276703025 157.00907957539334 956.6571732583582 157.47140951859274 C956.5761909050849 157.93397821372355 956.7042360424427 158.40866006288257 957.0069625385439 158.76755043419774 C957.3096884976733 159.12666879212856 957.7559488780231 159.33293990258028 958.2254967066773 159.3312575114503 Z M955.5206145891663 151.73339573113688 C956.2127797804402 150.89773949044366 956.4968313325933 149.79702637271828 956.2956507673325 148.7307208087004 C956.0690282673107 147.5933751328168 955.3470034800778 146.61607305923553 954.3262306472902 146.06555364909337 C953.7617094947875 145.76664290752836 953.1802017090963 145.50081543055975 952.5849093505882 145.26908067356814 L952.3568052481027 145.17792261257074 C951.9953532491929 145.0386429495124 951.6555226107005 144.8482730903384 951.3479177379968 144.6127502619754 C951.1361382017786 144.45884471408323 951.0206018567433 144.20487532302974 951.0439164253626 143.94413844300848 C951.0882233565353 143.67638979710034 951.270962240374 143.45210093805736 951.5240850143874 143.35459864485597 C951.7068244384327 143.28103506274707 951.9016444617773 143.24181924285008 952.0985919288983 143.2390622998207 C952.9041823505518 143.19369546702345 953.7078190744149 143.35586856394664 954.4326550323677 143.71011344271727 C955.1833237629769 144.07474568670685 955.3444527471898 143.52482873313735 955.4112053650173 143.29375605600583 C955.511480859204 142.9897547433716 955.5996703326163 142.6859813655967 955.687859799559 142.36372355243978 L955.7942787049354 141.9959164977845 L955.7940667245954 141.99612847812455 C955.8589361820352 141.82208317227884 955.8500304063407 141.6293796850651 955.7698995546109 141.46190113127707 C955.6895521673525 141.29463963058083 955.5447640655887 141.16701726477118 955.368596789198 141.10850829102174 C954.8562078687986 140.87637733935526 954.3158587319664 140.71186734727422 953.7610520348112 140.61922769028274 C953.2596854003589 140.54015565652924 953.2596854003589 140.54015565652924 953.2565051811571 140.03560338139994 C953.2565051811571 138.9717942320059 953.1744645533489 138.80181921183768 952.1350608083076 138.80181921183768 L951.5728732967837 138.80181921183768 C950.9650986063746 138.82005082403717 950.7978371186174 138.9993961782116 950.7827499425898 139.59807512133204 L950.7827499425898 140.0144325080435 L950.7827499425898 140.23638775198577 C950.7827499425898 140.68624084945483 950.7827499425898 140.68624084945483 950.3360770643226 140.84416244239492 C948.713282308507 141.4337022405474 947.8408849248838 142.5460291672686 947.7435780635691 144.14467369682689 C947.6615374357609 145.51223451221713 948.2845892155659 146.57604366161115 949.6491760089568 147.39053441160587 L949.6489640286168 147.39053441160587 C950.1524471862609 147.675877542872 950.6779858881403 147.92072947485812 951.2202560645211 148.12296830171465 C951.4513287545917 148.21709495676672 951.6851583488744 148.30825301129457 951.9132896234311 148.41170333555553 L951.913077643091 148.41170333555553 C952.1854881106646 148.5240594355109 952.4411561831516 148.67330314879862 952.6728638198284 148.8554076262377 C952.9384904822525 149.0521379755715 953.0805271012794 149.3745803517866 953.046819733027 149.70338326991788 C953.0207444140536 149.86237937594012 952.9531163864949 150.01183474672266 952.8505126986828 150.13606522289666 C952.7481206554262 150.2605073436264 952.614349437543 150.35548062865766 952.4632008326802 150.41144430393345 C952.0498120335538 150.58485465023378 951.5974244805615 150.6437869130947 951.1532752407454 150.58167439221478 C950.4191589595066 150.50090369643638 949.7055569191175 150.28763712980995 949.0470937910726 149.95246290444675 C948.9101477560541 149.86808864859876 948.7547551971622 149.81805673348765 948.5942721060185 149.80661109373372 C948.2296398620289 149.80661109373372 948.1141035169935 150.17738673597415 948.0503194867513 150.3749854011877 C947.9318145541308 150.7548784895564 947.8253956552238 151.14090413322566 947.7189712960244 151.52378206066584 C947.5487412077431 152.1529935484339 947.6551601131197 152.38999259660264 948.2447161951062 152.67556361097175 L948.2449281754461 152.67556361097175 C948.892374555471 152.9696008733766 949.5794383327253 153.16717785268963 950.2840858041438 153.26217281068236 C950.39559527412 153.27425666739532 950.5056177330964 153.2975755900155 950.6122537239125 153.33213283919133 C950.6334528432837 153.44936785272108 950.6406610438362 153.56871938944255 950.6336650409852 153.68764763705258 L950.6336650409852 154.44151832452033 C950.6088618213378 154.6431221530143 950.6750039215448 154.8451547309121 950.8142824944856 154.99312306481679 C950.953350500049 155.14109684607473 951.1511391109788 155.21953390734384 951.3538066354828 155.2072362420138 L952.4813836153892 155.2072362420138 C952.6717534745632 155.21783580169944 952.8572498286987 155.14491207010863 952.989534054991 155.00796060714538 C953.1220299129 154.87080292757113 953.1883863748698 154.6829729324566 953.1712152174405 154.49302636239406 L953.1712152174405 154.1495975723681 L953.1712152174405 153.45064857594477 C953.1712152174405 153.21660729828903 953.2288775612108 153.14664726331057 953.4752165300747 153.07372407516118 C954.2808069517283 152.85452032689497 954.9964712746796 152.3864432318759 955.520560902946 151.73644609308514 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_1-f305a" fill="#FFFFFF" fill-opacity="1.0"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_10" class="group firer ie-background commentable non-processed" customid="Card 2" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Rectangle_3" class="rectangle manualfit firer commentable non-processed" customid="Rectangle 2"   datasizewidth="252.0px" datasizeheight="101.3px" datasizewidthpx="252.0" datasizeheightpx="101.3" dataX="470.0" dataY="111.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_3_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_21" class="richtext manualfit firer ie-background commentable non-processed" customid="Production of the day"   datasizewidth="145.3px" datasizeheight="24.0px" dataX="486.0" dataY="124.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_21_0">Production of the day</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="shapewrapper-s-Ellipse_2" customid="Ellipse 2" class="shapewrapper shapewrapper-s-Ellipse_2 non-processed"   datasizewidth="45.0px" datasizeheight="45.0px" datasizewidthpx="45.0" datasizeheightpx="45.0" dataX="661.0" dataY="124.5" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_2" class="svgContainer" style="width:100%; height:100%;">\
                <g>\
                    <g clip-path="url(#clip-s-Ellipse_2)">\
                            <ellipse id="s-Ellipse_2" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 2" cx="22.5" cy="22.5" rx="22.5" ry="22.5">\
                            </ellipse>\
                    </g>\
                </g>\
                <defs>\
                    <clipPath id="clip-s-Ellipse_2" class="clipPath">\
                            <ellipse cx="22.5" cy="22.5" rx="22.5" ry="22.5">\
                            </ellipse>\
                    </clipPath>\
                </defs>\
            </svg>\
            <div class="paddingLayer">\
                <div id="shapert-s-Ellipse_2" class="content firer" >\
                    <div class="valign">\
                        <span id="rtr-s-Ellipse_2_0"></span>\
                    </div>\
                </div>\
            </div>\
        </div>\
        <div id="s-Paragraph_35" class="richtext manualfit firer ie-background commentable non-processed" customid="$20,000"   datasizewidth="110.0px" datasizeheight="18.0px" dataX="486.0" dataY="142.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_35_0">$20,000</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_36" class="richtext autofit firer ie-background commentable non-processed" customid="12%"   datasizewidth="21.7px" datasizeheight="13.0px" dataX="496.0" dataY="184.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_36_0">12%</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_37" class="richtext manualfit firer ie-background commentable non-processed" customid="Since last month"   datasizewidth="99.3px" datasizeheight="13.0px" dataX="526.0" dataY="184.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_37_0">Since last month</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Path_65" class="path firer commentable non-processed" customid="Path 65"   datasizewidth="8.4px" datasizeheight="4.1px" dataX="484.0" dataY="188.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="8.376322746276855" height="4.139999866485596" viewBox="484.0000000000001 188.0 8.376322746276855 4.139999866485596" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_65-f305a" d="M484.0000000000001 188.0 L488.1400000000001 192.14 L492.3763231412116 188.12222300170944 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_65-f305a" fill="#B74141" fill-opacity="1.0"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
        <div id="s-Path_70" class="path firer commentable non-processed" customid="Path 70"   datasizewidth="28.0px" datasizeheight="28.0px" dataX="670.0" dataY="133.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="28.00262451171875" height="28.000003814697266" viewBox="670.0 133.00000000000003 28.00262451171875 28.000003814697266" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_70-f305a" d="M670.0 147.00150262529928 C670.0040278980553 143.28929588345574 671.4805519137083 139.73034914579037 674.1052873420537 137.1052873420537 C676.7301858028258 134.48060605082512 680.2894039506642 133.00407011746944 684.0015026252993 133.00000000000003 C684.5683573670245 133.00000000000003 685.0920128632293 133.3023026441398 685.3754131137772 133.79327101866662 C685.6588513534683 134.2840386304053 685.6588513534683 134.8888826706162 685.3754131137772 135.37959598996895 C685.0919748740861 135.87057523332425 684.5683573670245 136.17286700863556 684.0015026252993 136.17286700863556 C681.1308676134186 136.17604693084496 678.3785421799062 137.3180818198229 676.3493704167294 139.34812195063586 C674.3199272433794 141.37837914747965 673.1787826531742 144.13070458099202 673.1762318944081 147.00133979989798 C673.1762318944081 147.56862872544133 672.8735059352788 148.09288123086546 672.3823096258926 148.37655278807372 C671.8909070675477 148.66020268525963 671.2856667809057 148.66020268525963 670.7942480228307 148.37655278807372 C670.3030570961025 148.09290289088784 670.0003257543153 147.56862872544133 670.0003257543153 147.00133979989798 Z M685.6092640316663 159.410438673417 C685.6084160560225 158.9896310966568 685.4405155199703 158.5863962934759 685.1424514213236 158.28964549866004 C684.8443873097377 157.99263964867006 684.4405175340725 157.82622479089213 684.0197045487763 157.82707680352922 C683.5988969720161 157.82792477917292 683.1956621688353 157.99582531522512 682.8989113740193 158.29388941387188 C682.6019055240293 158.59195352545768 682.4354906662514 158.99582330112298 682.4363426788885 159.4166362864192 C682.4371906545322 159.83723220568456 682.6050911905844 160.24046160032935 682.9031552892311 160.53742946117612 C683.201219400817 160.83443531116612 683.6050891764822 161.0006385373274 684.0259021617785 160.99999815630696 C684.4464980810438 160.99915018066326 684.8497274756886 160.83124964461103 685.1466953365355 160.5331855459643 C685.4437011865253 160.2351214343785 685.6101160443034 159.8312516587132 685.6092640316663 159.410438673417 Z M690.2074355604066 137.83251092283334 C690.6740365391325 137.82827104461492 691.1150001253326 137.61882106062595 691.4130534198463 137.26012058327052 C691.7111175314321 136.90121388283453 691.8361946117071 136.4295114642109 691.7550006074086 135.9701121712097 C691.6738066031102 135.51072374703696 691.3946123045091 135.11025130836063 690.9916325823805 134.87536878338358 C690.5644645154605 134.6286065254082 690.0467842094473 134.59468749966098 689.5911838307677 134.78336410534607 C689.1353989407863 134.9720407175007 688.7932455301434 135.36210406624028 688.6656014396159 135.8384357518003 C688.5379790738062 136.3149953722196 688.6391022686811 136.82376988959925 688.9394989181891 137.21489699904433 C689.2398966707538 137.60623571422784 689.704983466409 137.83564229053297 690.1983017851575 137.83564229053297 Z M676.4401104354129 156.97912191413573 C676.2302357907698 157.3437541581253 676.1738488005044 157.7768431487292 676.2834479574304 158.1830573824455 C676.3928354568616 158.58923906143275 676.6591025132452 158.93540850418455 677.0237510345993 159.14527227999923 C677.3883832785889 159.35514692464233 677.8214722691928 159.41153391490766 678.227686502909 159.3021464154765 C678.6339007366253 159.1927589160453 678.980037624648 158.92649185966172 679.1901184664936 158.5618433383076 C679.3999931111367 158.19721109431802 679.4563801014021 157.7643392215013 679.3472042594657 157.35790786999786 C679.2378167600345 156.95172619101055 678.9715497036509 156.60555674825878 678.6071182483277 156.39569297244412 C678.242914753742 156.18581832780106 677.8104281550153 156.12879093063705 677.4044852797158 156.2371201684724 C676.9983036007285 156.3452377423434 676.6516999741586 156.60980613670205 676.4400995148281 156.97297308147068 Z M673.2671890826351 154.8182899248099 C673.7363298995424 154.81744194916624 674.1811033206488 154.60883858221734 674.481707270416 154.24866735828772 C674.7823166804756 153.88849071288288 674.9082403648516 153.4136080750575 674.8253476920586 152.95187514107744 C674.7426721047051 152.49015307592583 674.4596571864732 152.0886494959684 674.0526451548712 151.855449465634 C673.6456331232692 151.62224943529958 673.156097441478 151.58112867207123 672.7158014083695 151.74309336567862 C672.275700703635 151.90526972971986 671.9297483269141 152.25378907787598 671.7709530354364 152.69537684536647 C671.6123802314646 153.13674749506976 671.6571099244813 153.6260064099082 671.8934848431159 154.03113526171384 C672.0331878082247 154.2715370295294 672.2335217305672 154.47102433483167 672.4747755627761 154.60922402451547 C672.716023960571 154.7474454130389 672.9892864925379 154.81952198414817 673.2672325579489 154.81825205211842 Z M696.1334965772159 153.991751130245 C696.3448528113835 153.62669557126583 696.402303510598 153.19272744771004 696.293550957773 152.7854820727127 C696.1847984178871 152.37803044875662 695.9185313485644 152.03058024396387 695.553464946635 151.8194139166951 C695.1884093876558 151.60826934002228 694.7544412641 151.55103029830258 694.3471958891027 151.65999449568324 C693.9397442651466 151.7689586930639 693.5925111263848 152.03522576238655 693.3815619686595 152.40051469063928 C693.1706290494815 152.76557024961846 693.1133900077618 153.19953837317425 693.2225658626372 153.6069465735729 C693.3317417045736 154.0141865400342 693.598220418452 154.36141427026 693.9634659490255 154.57214636195442 C694.3280981930151 154.78350259612205 694.7618384334678 154.84095329533656 695.1688667424344 154.73198909795587 C695.5761067088956 154.62323655807003 695.9231173213343 154.35675783125254 696.1334153327232 153.991686020787 Z M698.0026249451157 147.00171813864037 C698.0026249451157 146.5809105618802 697.8355751019143 146.17745869266847 697.537934292379 145.8798395302165 C697.2405051273994 145.5824103652369 696.8368687210076 145.4153605090964 696.4162728534985 145.4153605090964 C695.9954652767384 145.4153605090964 695.5918505821253 145.5824103522978 695.2943942450747 145.8798395302165 C694.9969650800952 146.17748032681268 694.829703592338 146.5809051015878 694.829703592338 147.00171813864037 C694.829703592338 147.4225311756929 694.9969650930342 147.82597758461227 695.2943942450747 148.12359674706425 C695.5918234100542 148.42102591204383 695.995459816446 148.58807576818435 696.4162728534985 148.58807576818435 C696.8366571152692 148.58743977290598 697.2396639835907 148.41996221603375 697.5370660282555 148.12253848546843 C697.834495193235 147.82532097798364 698.0017566809922 147.42234122997684 698.0026033109714 147.00174531071144 Z M693.3771550598383 141.5859080401636 C693.6608049570242 142.07625233691263 694.184210806591 142.3785278089813 694.7506856568841 142.37917905883018 C695.2198264737914 142.37917905883018 695.664599894898 142.17184968678262 695.966072212301 141.81254138313577 C696.2677398839564 141.45342302520496 696.3947218558066 140.9792187834214 696.3133161940132 140.51748579768503 C696.2319105322199 140.05555210091677 695.9503826185181 139.65339183669624 695.5441955051166 139.41894349553723 C695.138225457746 139.1844789028919 694.649167357452 139.14165411438935 694.2084370887692 139.3019255739165 C693.7677013856721 139.46240326946327 693.4206473755543 139.80944101515578 693.2601696929465 140.25019296973915 C693.0999036548945 140.6909286728362 693.1427230348611 141.18000840727436 693.3771876145673 141.58595138608655 Z M690.2254533866324 159.3312574985112 C690.718125889946 159.32956154722385 691.1821869787344 159.09891488934994 691.4811085373716 158.7071474247626 C691.7798076214418 158.31537996017525 691.8798714646032 157.80701786307856 691.7514024817534 157.33128323849425 C691.6229334989036 156.85557024805416 691.2807746538464 156.46653802765655 690.8252231331386 156.2785451812626 C690.3698615581468 156.0905089630675 689.8528054075458 156.12463963337044 689.4262939472545 156.37118482531497 C689.0196889532776 156.6060727329499 688.7379276703023 157.00907957539334 688.657173258358 157.47140951859274 C688.5761909050847 157.93397821372355 688.7042360424425 158.40866006288257 689.0069625385437 158.76755043419774 C689.309688497673 159.12666879212856 689.7559488780229 159.33293990258028 690.2254967066771 159.3312575114503 Z M687.520614589166 151.73339573113688 C688.21277978044 150.89773949044366 688.496831332593 149.79702637271828 688.2956507673323 148.7307208087004 C688.0690282673105 147.5933751328168 687.3470034800775 146.61607305923553 686.32623064729 146.06555364909337 C685.7617094947873 145.76664290752836 685.1802017090961 145.50081543055975 684.584909350588 145.26908067356814 L684.3568052481024 145.17792261257074 C683.9953532491927 145.0386429495124 683.6555226107002 144.8482730903384 683.3479177379966 144.6127502619754 C683.1361382017784 144.45884471408323 683.020601856743 144.20487532302974 683.0439164253623 143.94413844300848 C683.0882233565351 143.67638979710034 683.2709622403738 143.45210093805736 683.5240850143872 143.35459864485597 C683.7068244384325 143.28103506274707 683.9016444617771 143.24181924285008 684.098591928898 143.2390622998207 C684.9041823505515 143.19369546702345 685.7078190744147 143.35586856394664 686.4326550323675 143.71011344271727 C687.1833237629767 144.07474568670685 687.3444527471896 143.52482873313735 687.411205365017 143.29375605600583 C687.5114808592037 142.9897547433716 687.599670332616 142.6859813655967 687.6878597995587 142.36372355243978 L687.7942787049352 141.9959164977845 L687.7940667245952 141.99612847812455 C687.858936182035 141.82208317227884 687.8500304063405 141.6293796850651 687.7698995546107 141.46190113127707 C687.6895521673523 141.29463963058083 687.5447640655884 141.16701726477118 687.3685967891978 141.10850829102174 C686.8562078687984 140.87637733935526 686.3158587319662 140.71186734727422 685.761052034811 140.61922769028274 C685.2596854003586 140.54015565652924 685.2596854003586 140.54015565652924 685.2565051811569 140.03560338139994 C685.2565051811569 138.9717942320059 685.1744645533487 138.80181921183768 684.1350608083073 138.80181921183768 L683.5728732967834 138.80181921183768 C682.9650986063743 138.82005082403717 682.7978371186172 138.9993961782116 682.7827499425896 139.59807512133204 L682.7827499425896 140.0144325080435 L682.7827499425896 140.23638775198577 C682.7827499425896 140.68624084945483 682.7827499425896 140.68624084945483 682.3360770643224 140.84416244239492 C680.7132823085068 141.4337022405474 679.8408849248835 142.5460291672686 679.7435780635689 144.14467369682689 C679.6615374357607 145.51223451221713 680.2845892155657 146.57604366161115 681.6491760089566 147.39053441160587 L681.6489640286165 147.39053441160587 C682.1524471862607 147.675877542872 682.67798588814 147.92072947485812 683.2202560645209 148.12296830171465 C683.4513287545915 148.21709495676672 683.6851583488742 148.30825301129457 683.9132896234308 148.41170333555553 L683.9130776430908 148.41170333555553 C684.1854881106643 148.5240594355109 684.4411561831514 148.67330314879862 684.6728638198282 148.8554076262377 C684.9384904822523 149.0521379755715 685.0805271012791 149.3745803517866 685.0468197330267 149.70338326991788 C685.0207444140534 149.86237937594012 684.9531163864947 150.01183474672266 684.8505126986826 150.13606522289666 C684.748120655426 150.2605073436264 684.6143494375427 150.35548062865766 684.46320083268 150.41144430393345 C684.0498120335536 150.58485465023378 683.5974244805612 150.6437869130947 683.1532752407452 150.58167439221478 C682.4191589595064 150.50090369643638 681.7055569191173 150.28763712980995 681.0470937910724 149.95246290444675 C680.9101477560539 149.86808864859876 680.754755197162 149.81805673348765 680.5942721060183 149.80661109373372 C680.2296398620286 149.80661109373372 680.1141035169933 150.17738673597415 680.050319486751 150.3749854011877 C679.9318145541306 150.7548784895564 679.8253956552236 151.14090413322566 679.7189712960242 151.52378206066584 C679.5487412077429 152.1529935484339 679.6551601131195 152.38999259660264 680.244716195106 152.67556361097175 L680.2449281754459 152.67556361097175 C680.8923745554707 152.9696008733766 681.5794383327251 153.16717785268963 682.2840858041436 153.26217281068236 C682.3955952741197 153.27425666739532 682.5056177330962 153.2975755900155 682.6122537239123 153.33213283919133 C682.6334528432835 153.44936785272108 682.640661043836 153.56871938944255 682.633665040985 153.68764763705258 L682.633665040985 154.44151832452033 C682.6088618213375 154.6431221530143 682.6750039215445 154.8451547309121 682.8142824944854 154.99312306481679 C682.9533505000488 155.14109684607473 683.1511391109785 155.21953390734384 683.3538066354826 155.2072362420138 L684.481383615389 155.2072362420138 C684.6717534745629 155.21783580169944 684.8572498286985 155.14491207010863 684.9895340549908 155.00796060714538 C685.1220299128997 154.87080292757113 685.1883863748695 154.6829729324566 685.1712152174402 154.49302636239406 L685.1712152174402 154.1495975723681 L685.1712152174402 153.45064857594477 C685.1712152174402 153.21660729828903 685.2288775612105 153.14664726331057 685.4752165300745 153.07372407516118 C686.2808069517281 152.85452032689497 686.9964712746794 152.3864432318759 687.5205609029458 151.73644609308514 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_70-f305a" fill="#FFFFFF" fill-opacity="1.0"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_8" class="group firer ie-background commentable non-processed" customid="Card 1" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Rectangle_24" class="rectangle manualfit firer commentable non-processed" customid="Rectangle 2"   datasizewidth="252.0px" datasizeheight="101.3px" datasizewidthpx="252.0" datasizeheightpx="101.3" dataX="202.0" dataY="111.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_24_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_51" class="richtext autofit firer ie-background commentable non-processed" customid="$20,000.00"   datasizewidth="45.7px" datasizeheight="13.0px" dataX="221.0" dataY="184.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_51_0">$20,000.00</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_64" class="richtext manualfit firer ie-background commentable non-processed" customid="last month"   datasizewidth="66.7px" datasizeheight="13.0px" dataX="288.0" dataY="184.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_64_0">last month</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_50" class="richtext manualfit firer ie-background commentable non-processed" customid="$20,000.00"   datasizewidth="123.7px" datasizeheight="18.0px" dataX="221.0" dataY="143.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_50_0">$20,000.00</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_81" class="richtext manualfit firer ie-background commentable non-processed" customid="GOAL"   datasizewidth="150.5px" datasizeheight="13.0px" dataX="221.0" dataY="124.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_81_0">GOAL</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_7" class="group firer ie-background commentable non-processed" customid="Products" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Rectangle_8" class="rectangle manualfit firer commentable non-processed" customid="Rectangle "   datasizewidth="252.0px" datasizeheight="262.0px" datasizewidthpx="252.0" datasizeheightpx="262.0" dataX="574.0" dataY="610.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_8_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_26" class="group firer ie-background commentable non-processed" customid="Product 4" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_48" class="richtext manualfit firer ie-background commentable non-processed" customid="Camsoda"   datasizewidth="92.1px" datasizeheight="26.0px" dataX="655.0" dataY="810.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_48_0">Camsoda<br /><br /></span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_49" class="richtext manualfit firer ie-background commentable non-processed" customid="Updated 5hr ago"   datasizewidth="92.1px" datasizeheight="12.2px" dataX="655.0" dataY="826.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_49_0">Updated 5hr ago</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Image_4" class="image firer ie-background commentable non-processed" customid="Icon"   datasizewidth="29.2px" datasizeheight="29.2px" dataX="605.0" dataY="809.0"   alt="image">\
            <div class="borderLayer">\
            	<div class="imageViewport">\
            		<img src="./images/29e00973-8d51-4f7e-a80c-a29b48deea04.png" />\
            	</div>\
            </div>\
          </div>\
\
        </div>\
\
\
        <div id="s-Group_25" class="group firer ie-background commentable non-processed" customid="Product 3" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_47" class="richtext manualfit firer ie-background commentable non-processed" customid="Updated 5hr ago"   datasizewidth="100.0px" datasizeheight="15.2px" dataX="655.0" dataY="757.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_47_0">Updated 5hr ago</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_46" class="richtext manualfit firer ie-background commentable non-processed" customid="Stripchat"   datasizewidth="100.0px" datasizeheight="14.0px" dataX="655.0" dataY="740.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_46_0">Stripchat</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Image_2" class="image firer ie-background commentable non-processed" customid="Icon"   datasizewidth="32.4px" datasizeheight="35.2px" dataX="603.0" dataY="737.0"   alt="image">\
            <div class="borderLayer">\
            	<div class="imageViewport">\
            		<img src="./images/602d30fe-e89d-4381-8d57-290ba80746f5.png" />\
            	</div>\
            </div>\
          </div>\
\
        </div>\
\
\
        <div id="s-Group_24" class="group firer ie-background commentable non-processed" customid="Product 1" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_43" class="richtext manualfit firer ie-background commentable non-processed" customid="Updated 5hr ago"   datasizewidth="100.0px" datasizeheight="11.2px" dataX="655.0" dataY="686.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_43_0">Updated 5hr ago</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_38" class="richtext manualfit firer ie-background commentable non-processed" customid="Chaturbate"   datasizewidth="100.0px" datasizeheight="14.0px" dataX="655.0" dataY="669.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_38_0">Chaturbate</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Image_3" class="image firer ie-background commentable non-processed" customid="Icon"   datasizewidth="32.2px" datasizeheight="28.2px" dataX="602.0" dataY="669.0"   alt="image">\
            <div class="borderLayer">\
            	<div class="imageViewport">\
            		<img src="./images/03d5d77a-2da1-4e2e-a90b-b0d75b2f6b92.png" />\
            	</div>\
            </div>\
          </div>\
\
        </div>\
\
        <div id="s-Paragraph_25" class="richtext manualfit firer ie-background commentable non-processed" customid="Last product"   datasizewidth="123.4px" datasizeheight="15.0px" dataX="590.0" dataY="625.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_25_0">Last product</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_31" class="group firer ie-background commentable non-processed" customid="By device" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Rectangle_6" class="rectangle manualfit firer commentable non-processed" customid="Rectangle "   datasizewidth="793.0px" datasizeheight="366.0px" datasizewidthpx="793.0" datasizeheightpx="366.0" dataX="340.0" dataY="227.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_6_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_20" class="richtext manualfit firer ie-background commentable non-processed" customid="Earnings"   datasizewidth="61.3px" datasizeheight="14.0px" dataX="356.0" dataY="238.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_20_0">Earnings</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_4" class="group firer ie-background commentable non-processed" customid="Chart" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Group_21" class="group firer ie-background commentable non-processed" customid="Lines" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Path_5" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="738.4px" datasizeheight="3.0px" dataX="383.0" dataY="283.0"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="738.4462280273438" height="2.0" viewBox="383.0000000000002 283.0 738.4462280273438 2.0" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_5-f305a" d="M384.0000000000002 284.0 L1120.4462278257329 284.0 "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_5-f305a" fill="none" stroke-width="1.0" stroke="#BCBCBC" stroke-linecap="square"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
            <div id="s-Path_6" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="738.4px" datasizeheight="3.0px" dataX="383.0" dataY="319.3"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="738.4462280273438" height="2.0" viewBox="383.0000000000002 319.29658385093177 738.4462280273438 2.0" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_6-f305a" d="M384.0000000000002 320.29658385093177 L1120.4462278257329 320.29658385093177 "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_6-f305a" fill="none" stroke-width="1.0" stroke="#BCBCBC" stroke-linecap="square"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
            <div id="s-Path_7" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="738.4px" datasizeheight="3.0px" dataX="383.0" dataY="355.6"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="738.4462280273438" height="2.0" viewBox="382.9999999999999 355.59316770186336 738.4462280273438 2.0" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_7-f305a" d="M383.9999999999999 356.59316770186336 L1120.4462278257324 356.59316770186336 "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_7-f305a" fill="none" stroke-width="1.0" stroke="#BCBCBC" stroke-linecap="square"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
            <div id="s-Path_8" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="738.4px" datasizeheight="3.0px" dataX="383.0" dataY="391.9"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="738.4462280273438" height="2.0" viewBox="383.00000000000034 391.8897515527951 738.4462280273438 2.0" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_8-f305a" d="M384.00000000000034 392.8897515527951 L1120.446227825733 392.8897515527951 "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_8-f305a" fill="none" stroke-width="1.0" stroke="#BCBCBC" stroke-linecap="square"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
            <div id="s-Path_12" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="738.4px" datasizeheight="3.0px" dataX="383.0" dataY="428.2"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="738.4462280273438" height="2.0" viewBox="383.00000000000034 428.1863354037268 738.4462280273438 2.0" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_12-f305a" d="M384.00000000000034 429.1863354037268 L1120.446227825733 429.1863354037268 "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_12-f305a" fill="none" stroke-width="1.0" stroke="#BCBCBC" stroke-linecap="square"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
            <div id="s-Path_13" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="738.4px" datasizeheight="3.0px" dataX="383.0" dataY="464.5"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="738.4462280273438" height="2.0" viewBox="383.00000000000034 464.48291925465855 738.4462280273438 2.0" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_13-f305a" d="M384.00000000000034 465.48291925465855 L1120.446227825733 465.48291925465855 "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_13-f305a" fill="none" stroke-width="1.0" stroke="#BCBCBC" stroke-linecap="square"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
            <div id="s-Path_14" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="738.4px" datasizeheight="3.0px" dataX="383.0" dataY="500.8"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="738.4462280273438" height="2.0" viewBox="382.9999999999999 500.77950310559015 738.4462280273438 2.0" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_14-f305a" d="M383.9999999999999 501.77950310559015 L1120.4462278257324 501.77950310559015 "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_14-f305a" fill="none" stroke-width="1.0" stroke="#BCBCBC" stroke-linecap="square"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
            <div id="s-Path_15" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="738.4px" datasizeheight="3.0px" dataX="383.0" dataY="537.1"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="738.4462280273438" height="2.0" viewBox="383.00000000000034 537.0760869565219 738.4462280273438 2.0" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_15-f305a" d="M384.00000000000034 538.0760869565219 L1120.446227825733 538.0760869565219 "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_15-f305a" fill="none" stroke-width="1.0" stroke="#BCBCBC" stroke-linecap="square"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
          </div>\
\
\
          <div id="s-Group_19" class="group firer ie-background commentable non-processed" customid="Ammount" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Paragraph_27" class="richtext autofit firer ie-background commentable non-processed" customid="0"   datasizewidth="5.3px" datasizeheight="9.0px" dataX="359.0" dataY="529.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_27_0">0</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_28" class="richtext autofit firer ie-background commentable non-processed" customid="5k"   datasizewidth="9.3px" datasizeheight="9.0px" dataX="358.0" dataY="493.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_28_0">5k</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_29" class="richtext autofit firer ie-background commentable non-processed" customid="10k"   datasizewidth="13.0px" datasizeheight="9.0px" dataX="358.0" dataY="457.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_29_0">10k</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_30" class="richtext autofit firer ie-background commentable non-processed" customid="15k"   datasizewidth="12.2px" datasizeheight="9.0px" dataX="358.0" dataY="421.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_30_0">15k</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_31" class="richtext autofit firer ie-background commentable non-processed" customid="20k"   datasizewidth="14.6px" datasizeheight="9.0px" dataX="358.0" dataY="385.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_31_0">20k</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_32" class="richtext autofit firer ie-background commentable non-processed" customid="30k"   datasizewidth="14.6px" datasizeheight="9.0px" dataX="358.0" dataY="349.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_32_0">30k</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_33" class="richtext autofit firer ie-background commentable non-processed" customid="40k"   datasizewidth="15.4px" datasizeheight="9.0px" dataX="358.0" dataY="313.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_33_0">40k</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_34" class="richtext autofit firer ie-background commentable non-processed" customid="50k"   datasizewidth="14.6px" datasizeheight="9.0px" dataX="358.0" dataY="277.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_34_0">50k</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
        </div>\
\
        <div id="s-Paragraph_62" class="richtext autofit firer ie-background commentable non-processed" customid="Last 7 days"   datasizewidth="54.9px" datasizeheight="12.0px" dataX="1059.0" dataY="238.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_62_0">Last 7 days</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Path_71" class="path firer ie-background commentable non-processed" customid="Progress"   datasizewidth="734.7px" datasizeheight="201.0px" dataX="384.0" dataY="337.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="734.7265014648438" height="201.0" viewBox="384.00000172502075 336.9999982276038 734.7265014648438 201.0" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_71-f305a" d="M384.12868045397244 392.64233869028817 L541.7909123553904 415.23648585981664 L649.7384002787651 369.00583512447054 L751.8508888549304 337.0000000000002 L858.3396269415027 386.194153987612 L964.098990109674 386.7868546380652 L1118.7264773646398 375.52554226179143 L1118.7264773646398 538.0000000000001 L384.00000000000045 538.0 Z "></path>\
            	      <linearGradient id="s-Path_71-f305a-gradient" x1="384.00000000000045" y1="437.50000000000017" x2="1118.7264773646398" y2="437.50000000000017" gradientUnits="userSpaceOnUse">\
            	        <stop stop-color="#C8C5E8" offset="0.0%" stop-opacity="0.22999999999999998"></stop>\
            	        <stop stop-color="#B1F4DB" offset="100.0%" stop-opacity="0.22999999999999998"></stop>\
            	      </linearGradient>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_71-f305a" fill="url(#s-Path_71-f305a-gradient)" fill-opacity="0.92"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_20" class="group firer ie-background commentable non-processed" customid="Days" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_63" class="richtext autofit firer ie-background commentable non-processed" customid="1 May"   datasizewidth="21.7px" datasizeheight="9.0px" dataX="430.0" dataY="550.5" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_63_0">1 May</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_65" class="richtext manualfit firer ie-background commentable non-processed" customid="2 May"   datasizewidth="21.9px" datasizeheight="18.0px" dataX="560.0" dataY="550.5" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_65_0">2 May</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_66" class="richtext autofit firer ie-background commentable non-processed" customid="3 May"   datasizewidth="23.3px" datasizeheight="9.0px" dataX="690.0" dataY="550.5" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_66_0">3 May</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_67" class="richtext autofit firer ie-background commentable non-processed" customid="4 May"   datasizewidth="24.1px" datasizeheight="9.0px" dataX="820.0" dataY="550.5" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_67_0">4 May</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_68" class="richtext autofit firer ie-background commentable non-processed" customid="5 May"   datasizewidth="23.3px" datasizeheight="9.0px" dataX="950.0" dataY="550.5" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_68_0">5 May</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_69" class="richtext autofit firer ie-background commentable non-processed" customid="6 May"   datasizewidth="23.7px" datasizeheight="9.0px" dataX="1080.0" dataY="550.5" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_69_0">6 May</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_13" class="group firer ie-background commentable non-processed" customid="1 May" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="shapewrapper-s-Ellipse_1" customid="Ellipse " class="shapewrapper shapewrapper-s-Ellipse_1 non-processed"   datasizewidth="9.0px" datasizeheight="9.0px" datasizewidthpx="8.999999999999973" datasizeheightpx="9.0" dataX="439.0" dataY="395.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_1" class="svgContainer" style="width:100%; height:100%;">\
                  <g>\
                      <g clip-path="url(#clip-s-Ellipse_1)">\
                              <ellipse id="s-Ellipse_1" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse " cx="4.499999999999987" cy="4.5" rx="4.499999999999987" ry="4.5">\
                              </ellipse>\
                      </g>\
                  </g>\
                  <defs>\
                      <clipPath id="clip-s-Ellipse_1" class="clipPath">\
                              <ellipse cx="4.499999999999987" cy="4.5" rx="4.499999999999987" ry="4.5">\
                              </ellipse>\
                      </clipPath>\
                  </defs>\
              </svg>\
              <div class="paddingLayer">\
                  <div id="shapert-s-Ellipse_1" class="content firer" >\
                      <div class="valign">\
                          <span id="rtr-s-Ellipse_1_0"></span>\
                      </div>\
                  </div>\
              </div>\
          </div>\
          <div id="s-Path_19" class="path firer ie-background commentable non-processed" customid="Path "   datasizewidth="3.0px" datasizeheight="138.2px" dataX="442.0" dataY="402.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="2.0" height="138.16168212890625" viewBox="442.0 401.99999960635705 2.0 138.16168212890625" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_19-f305a" d="M443.0 403.0 L443.0 539.1616867139901 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_19-f305a" fill="none" stroke-width="1.0" stroke="#3C2CD1" stroke-linecap="square"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_14" class="group firer ie-background commentable non-processed" customid="2 May" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="shapewrapper-s-Ellipse_5" customid="Ellipse 1" class="shapewrapper shapewrapper-s-Ellipse_5 non-processed"   datasizewidth="9.0px" datasizeheight="9.0px" datasizewidthpx="8.999999999999973" datasizeheightpx="9.0" dataX="568.0" dataY="396.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_5" class="svgContainer" style="width:100%; height:100%;">\
                  <g>\
                      <g clip-path="url(#clip-s-Ellipse_5)">\
                              <ellipse id="s-Ellipse_5" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 1" cx="4.499999999999987" cy="4.5" rx="4.499999999999987" ry="4.5">\
                              </ellipse>\
                      </g>\
                  </g>\
                  <defs>\
                      <clipPath id="clip-s-Ellipse_5" class="clipPath">\
                              <ellipse cx="4.499999999999987" cy="4.5" rx="4.499999999999987" ry="4.5">\
                              </ellipse>\
                      </clipPath>\
                  </defs>\
              </svg>\
              <div class="paddingLayer">\
                  <div id="shapert-s-Ellipse_5" class="content firer" >\
                      <div class="valign">\
                          <span id="rtr-s-Ellipse_5_0"></span>\
                      </div>\
                  </div>\
              </div>\
          </div>\
          <div id="s-Path_20" class="path firer ie-background commentable non-processed" customid="Path 19"   datasizewidth="3.0px" datasizeheight="138.2px" dataX="571.0" dataY="402.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="2.0" height="138.16168212890625" viewBox="571.0 401.99999960635705 2.0 138.16168212890625" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_20-f305a" d="M572.0 403.0 L572.0 539.1616867139901 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_20-f305a" fill="none" stroke-width="1.0" stroke="#3C2CD1" stroke-linecap="square"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_15" class="group firer ie-background commentable non-processed" customid="3 May" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="shapewrapper-s-Ellipse_6" customid="Ellipse 1" class="shapewrapper shapewrapper-s-Ellipse_6 non-processed"   datasizewidth="9.0px" datasizeheight="9.0px" datasizewidthpx="8.999999999999973" datasizeheightpx="9.0" dataX="697.0" dataY="347.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_6" class="svgContainer" style="width:100%; height:100%;">\
                  <g>\
                      <g clip-path="url(#clip-s-Ellipse_6)">\
                              <ellipse id="s-Ellipse_6" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 1" cx="4.499999999999987" cy="4.5" rx="4.499999999999987" ry="4.5">\
                              </ellipse>\
                      </g>\
                  </g>\
                  <defs>\
                      <clipPath id="clip-s-Ellipse_6" class="clipPath">\
                              <ellipse cx="4.499999999999987" cy="4.5" rx="4.499999999999987" ry="4.5">\
                              </ellipse>\
                      </clipPath>\
                  </defs>\
              </svg>\
              <div class="paddingLayer">\
                  <div id="shapert-s-Ellipse_6" class="content firer" >\
                      <div class="valign">\
                          <span id="rtr-s-Ellipse_6_0"></span>\
                      </div>\
                  </div>\
              </div>\
          </div>\
          <div id="s-Path_21" class="path firer ie-background commentable non-processed" customid="Path 19"   datasizewidth="3.0px" datasizeheight="187.7px" dataX="700.0" dataY="352.4"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="2.0" height="187.74354553222656" viewBox="699.9999999999999 352.41814764712854 2.0 187.74354553222656" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_21-f305a" d="M700.9999999999999 353.4181483567627 L700.9999999999999 539.1616867139901 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_21-f305a" fill="none" stroke-width="1.0" stroke="#3C2CD1" stroke-linecap="square"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_16" class="group firer ie-background commentable non-processed" customid="4 May" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="shapewrapper-s-Ellipse_7" customid="Ellipse 1" class="shapewrapper shapewrapper-s-Ellipse_7 non-processed"   datasizewidth="9.0px" datasizeheight="9.0px" datasizewidthpx="8.999999999999973" datasizeheightpx="9.0" dataX="825.0" dataY="369.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_7" class="svgContainer" style="width:100%; height:100%;">\
                  <g>\
                      <g clip-path="url(#clip-s-Ellipse_7)">\
                              <ellipse id="s-Ellipse_7" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 1" cx="4.499999999999987" cy="4.5" rx="4.499999999999987" ry="4.5">\
                              </ellipse>\
                      </g>\
                  </g>\
                  <defs>\
                      <clipPath id="clip-s-Ellipse_7" class="clipPath">\
                              <ellipse cx="4.499999999999987" cy="4.5" rx="4.499999999999987" ry="4.5">\
                              </ellipse>\
                      </clipPath>\
                  </defs>\
              </svg>\
              <div class="paddingLayer">\
                  <div id="shapert-s-Ellipse_7" class="content firer" >\
                      <div class="valign">\
                          <span id="rtr-s-Ellipse_7_0"></span>\
                      </div>\
                  </div>\
              </div>\
          </div>\
          <div id="s-Path_22" class="path firer ie-background commentable non-processed" customid="Path 19"   datasizewidth="3.0px" datasizeheight="167.5px" dataX="829.0" dataY="372.7"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="2.0" height="167.47450256347656" viewBox="828.9999999999998 372.687182986484 2.0 167.47450256347656" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_22-f305a" d="M829.9999999999998 373.6871834548198 L829.9999999999998 539.1616867139901 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_22-f305a" fill="none" stroke-width="1.0" stroke="#3C2CD1" stroke-linecap="square"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_17" class="group firer ie-background commentable non-processed" customid="5 May" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="shapewrapper-s-Ellipse_8" customid="Ellipse 1" class="shapewrapper shapewrapper-s-Ellipse_8 non-processed"   datasizewidth="9.0px" datasizeheight="9.0px" datasizewidthpx="8.999999999999973" datasizeheightpx="9.0" dataX="955.0" dataY="383.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_8" class="svgContainer" style="width:100%; height:100%;">\
                  <g>\
                      <g clip-path="url(#clip-s-Ellipse_8)">\
                              <ellipse id="s-Ellipse_8" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 1" cx="4.499999999999987" cy="4.5" rx="4.499999999999987" ry="4.5">\
                              </ellipse>\
                      </g>\
                  </g>\
                  <defs>\
                      <clipPath id="clip-s-Ellipse_8" class="clipPath">\
                              <ellipse cx="4.499999999999987" cy="4.5" rx="4.499999999999987" ry="4.5">\
                              </ellipse>\
                      </clipPath>\
                  </defs>\
              </svg>\
              <div class="paddingLayer">\
                  <div id="shapert-s-Ellipse_8" class="content firer" >\
                      <div class="valign">\
                          <span id="rtr-s-Ellipse_8_0"></span>\
                      </div>\
                  </div>\
              </div>\
          </div>\
          <div id="s-Path_23" class="path firer ie-background commentable non-processed" customid="Path 19"   datasizewidth="3.0px" datasizeheight="154.3px" dataX="958.0" dataY="385.8"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="2.0" height="154.31332397460938" viewBox="957.9999999999998 385.8483672973971 2.0 154.31332397460938" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_23-f305a" d="M958.9999999999998 386.84836793760303 L958.9999999999998 539.1616867139901 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_23-f305a" fill="none" stroke-width="1.0" stroke="#3C2CD1" stroke-linecap="square"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_18" class="group firer ie-background commentable non-processed" customid="6 May" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="shapewrapper-s-Ellipse_9" customid="Ellipse 1" class="shapewrapper shapewrapper-s-Ellipse_9 non-processed"   datasizewidth="9.0px" datasizeheight="9.0px" datasizewidthpx="8.999999999999973" datasizeheightpx="9.0" dataX="1084.0" dataY="373.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_9" class="svgContainer" style="width:100%; height:100%;">\
                  <g>\
                      <g clip-path="url(#clip-s-Ellipse_9)">\
                              <ellipse id="s-Ellipse_9" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 1" cx="4.499999999999987" cy="4.5" rx="4.499999999999987" ry="4.5">\
                              </ellipse>\
                      </g>\
                  </g>\
                  <defs>\
                      <clipPath id="clip-s-Ellipse_9" class="clipPath">\
                              <ellipse cx="4.499999999999987" cy="4.5" rx="4.499999999999987" ry="4.5">\
                              </ellipse>\
                      </clipPath>\
                  </defs>\
              </svg>\
              <div class="paddingLayer">\
                  <div id="shapert-s-Ellipse_9" class="content firer" >\
                      <div class="valign">\
                          <span id="rtr-s-Ellipse_9_0"></span>\
                      </div>\
                  </div>\
              </div>\
          </div>\
          <div id="s-Path_24" class="path firer ie-background commentable non-processed" customid="Path 19"   datasizewidth="3.0px" datasizeheight="163.0px" dataX="1087.0" dataY="377.2"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="2.0" height="162.9518585205078" viewBox="1086.9999999999998 377.2098346588473 2.0 162.9518585205078" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_24-f305a" d="M1087.9999999999998 378.20983373310605 L1087.9999999999998 539.1616867139901 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_24-f305a" fill="none" stroke-width="1.0" stroke="#3C2CD1" stroke-linecap="square"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
      </div>\
\
\
      <div id="s-Group_2" class="group firer ie-background commentable non-processed" customid="Menu lateral" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Rectangle_7" class="rectangle manualfit firer commentable non-processed" customid="Background"   datasizewidth="180.0px" datasizeheight="828.0px" datasizewidthpx="180.00000000000023" datasizeheightpx="828.0" dataX="0.4" dataY="44.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_7_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Rectangle_1" class="rectangle manualfit firer commentable non-processed" customid="Rectangle "   datasizewidth="180.0px" datasizeheight="83.0px" datasizewidthpx="180.0" datasizeheightpx="83.0" dataX="0.4" dataY="0.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_1_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_33" class="group firer ie-background commentable non-processed" customid="Support" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_8" class="richtext manualfit firer ie-background commentable non-processed" customid="Support"   datasizewidth="110.0px" datasizeheight="17.0px" dataX="50.8" dataY="366.7" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_8_0">Support</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_9" class="path firer commentable non-processed" customid="Support_icn"   datasizewidth="15.9px" datasizeheight="12.1px" dataX="20.9" dataY="366.7"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="16.666664123535156" height="12.903030395507812" viewBox="20.853364792988316 366.7333348592124 16.666664123535156 12.903030395507812" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_9-f305a" d="M35.063914484946366 372.591979063047 L34.53120657782 372.591979063047 C34.47966043350391 369.6877863715981 32.10807448000341 367.3333333333335 29.186635170942225 367.3333333333335 C26.26519586188104 367.3333333333335 23.893619357665926 369.6877863715981 23.84206376406445 372.591979063047 L23.30935585693811 372.591979063047 C22.278187442843688 372.591979063047 21.45336326710941 373.4169567101013 21.45336326710941 374.4479716528757 L21.45336326710941 375.4962637670493 C21.45336326710941 376.5101550097446 22.278218172226957 377.352256356878 23.30935585693811 377.352256356878 L24.271691681628454 377.352256356878 C24.512277174270196 377.352256356878 24.70131959919243 377.16321393195574 24.70131959919243 376.92262843931405 L24.70131959919243 372.6779043890183 C24.70131959919243 370.2032169317289 26.7119781503753 368.19258881726853 29.186635170942225 368.19258881726853 C31.661292191509176 368.19258881726853 33.67195074269202 370.2032473684514 33.67195074269202 372.6779043890183 L33.67195074269202 377.1632199607681 C33.67195074269202 377.73032879790486 33.207921895117636 378.1771112034634 32.65805949999671 378.1771112034634 L31.231694827731985 378.1771112034634 L31.231694827731985 377.79903864537226 C31.231694827731985 377.1116953306786 30.68170969067438 376.56171025215315 29.99436643451284 376.56171025215315 C29.30702311981915 376.56171025215315 28.757038041293697 377.11169538921075 28.757038041293697 377.79903864537226 L28.757038041293697 378.60673912102743 C28.757038041293697 378.8473246136692 28.946080466215932 379.0363670385914 29.186665958857674 379.0363670385914 L32.658059617061014 379.0363670385914 C33.63767267168265 379.0363670385914 34.42812664678979 378.28019119302576 34.514052206889744 377.3178553683355 L35.064037343947376 377.3178553683355 C36.077928586642685 377.3178553683355 36.92002993377608 376.49300046321787 36.92002993377608 375.4618627785067 L36.92002993377608 374.4135706643332 C36.919910067588916 373.4169259221858 36.095052286721824 372.59197894598276 35.06391454347852 372.59197894598276 Z M22.31255822879237 375.4962944379004 L22.31255822879237 374.4480023237269 C22.31255822879237 373.8980171866692 22.759340575818783 373.4512655690261 23.30929498349309 373.4512655690261 L23.842002890619455 373.4512655690261 L23.842002890619455 376.4758153629043 L23.30929498349309 376.4758153629043 C22.759309846435485 376.4759352597865 22.31255822879237 376.0290330158779 22.31255822879237 375.49632510875153 Z M29.616232827380173 378.1944190457137 L29.616232827380173 377.8163464876225 C29.616232827380173 377.61016191035816 29.788133099960305 377.43827392953136 29.994305385471364 377.43827392953136 C30.200477670982423 377.43827392953136 30.372377943562526 377.61017420211147 30.372377943562526 377.8163464876225 L30.372377943562526 378.1944190457137 Z M36.060651590840024 375.4962944379004 C36.060651590840024 376.046279574958 35.61386924381364 376.49303119260117 35.063914836139304 376.49303119260117 L34.53120692901297 376.49303119260117 L34.53120692901297 373.45123478111066 L35.063914836139304 373.45123478111066 C35.613899973196936 373.45123478111066 36.060651590840024 373.8980171281371 36.060651590840024 374.4479715358114 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_9-f305a" fill="#6C6C6C" fill-opacity="1.0" stroke-width="0.2" stroke="#6C6C6C" stroke-linecap="butt"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_34" class="group firer ie-background commentable non-processed" customid="Schedule" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Union_1" class="path firer click commentable non-processed" customid="Schedule_icn"   datasizewidth="14.5px" datasizeheight="14.5px" dataX="22.0" dataY="331.5"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="14.504798889160156" height="14.50482177734375" viewBox="21.99744652186149 331.4790488603014 14.504798889160156 14.50482177734375" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Union_1-f305a" d="M34.50580604881314 333.84650016813214 C34.72727240670012 333.84650016813214 34.938810611807895 333.93449555742757 35.095487236527916 334.090635243457 C35.251619103061856 334.24744219310196 35.33962752484976 334.4588396472908 35.33962752484976 334.6803294636643 L35.33962752484976 335.57871651399705 L22.998232472190097 335.57871651399705 L22.998232472190097 334.6803294636643 C22.998232472190097 334.4588396472908 23.08623568098102 334.24731708117406 23.242370154013447 334.090635243457 C23.399171890661382 333.93449555742757 23.61059019683824 333.84650016813214 23.832051341728228 333.84650016813214 L26.097114175557085 333.84650016813214 L26.097114175557085 333.9807452667776 C26.097114175557085 334.256908995647 26.321260013900105 334.4810678664825 26.597449807754426 334.4810678664825 C26.873642208107253 334.4810678664825 27.097788046450273 334.256908995647 27.097788046450273 333.9807452667776 L27.097788046450273 333.84650016813214 L32.929893598383075 333.84650016813214 L32.929893598383075 333.9807452667776 C32.929893598383075 334.256908995647 33.154039436726094 334.4810678664825 33.43022923058042 334.4810678664825 C33.706421630933235 334.4810678664825 33.930567469276255 334.256908995647 33.930567469276255 333.9807452667776 L33.930567469276255 333.84650016813214 Z M32.06367334601522 339.67838416769257 C31.78748355216089 339.67838416769257 31.563337713817877 339.90254303852805 31.563337713817877 340.17870676739744 L31.563337713817877 341.84649047038954 C31.563337713817877 342.12265419925893 31.78748355216089 342.3468130700944 32.06367334601522 342.3468130700944 L33.7314257710254 342.3468130700944 C34.007615564879714 342.3468130700944 34.231761403222734 342.12265419925893 34.231761403222734 341.84649047038954 C34.231761403222734 341.5702850375443 34.007615564879714 341.3461261667087 33.7314257710254 341.3461261667087 L32.56401158471107 341.3461261667087 L32.56401158471107 340.17870676739744 C32.56401158471107 339.90254303852805 32.33986574636805 339.67838416769257 32.06367334601522 339.67838416769257 Z M32.666235849307554 339.312515186513 C34.23123228402763 339.312515186513 35.501514540067475 340.582651478637 35.501514540067475 342.1478016967684 C35.501514540067475 343.71278509899605 34.23134696996155 344.98304650304783 32.666235849307554 344.98304650304783 C31.10123941458747 344.98304650304783 29.830957158547623 343.71291021092395 29.830957158547623 342.1478016967684 C29.830957158547623 340.5827765905649 31.101124728653556 339.312515186513 32.666235849307554 339.312515186513 Z M26.597449807754426 331.4790488603014 C26.321260013900105 331.4790488603014 26.097114175557085 331.7032077311369 26.097114175557085 331.9793714600063 L26.097114175557085 332.8456047448666 L23.832051341728228 332.8456047448666 C23.345668295991143 332.8456047448666 22.878724516052795 333.0390277854127 22.534523356887874 333.3826685474005 C22.190976428845975 333.72689316505193 21.99744652186149 334.19393599193324 21.99744652186149 334.6802043517364 L21.99744652186149 343.68626137027945 C21.99744652186149 344.1726548420106 22.190879988401548 344.6396142609399 22.534523356887874 344.98379717461535 C22.878724516052795 345.3273545286512 23.34578298192506 345.52086097714925 23.832051341728228 345.52086097714925 L28.50176542995251 345.52086097714925 C28.777957830305336 345.52086097714925 29.00210366864835 345.29674381028974 29.00210366864835 345.0205383774444 C29.00210366864835 344.744332944599 28.777957830305336 344.5202157777395 28.50176542995251 344.5202157777395 L23.832051341728228 344.5202157777395 C23.610584983841242 344.5202157777395 23.399046778733474 344.4321786844681 23.242370154013447 344.2760807024147 C23.086238287479517 344.1192737527697 22.998232472190097 343.9078345946049 22.998232472190097 343.68638648220735 L22.998232472190097 336.57940341738265 L35.33962752484976 336.57940341738265 L35.33962752484976 338.01598027760053 C35.33962752484976 338.2921857104458 35.56377075669428 338.51634458128126 35.83996576354561 338.51634458128126 C36.116155557399935 338.51634458128126 36.34029878924446 338.2921857104458 36.34029878924446 338.01598027760053 L36.34041347517838 338.0158968696485 L36.34041347517838 336.0898404434779 C36.34049167013332 336.0862539015445 36.34052816111229 336.08266735961115 36.34052816111229 336.0790391137018 C36.34052816111229 336.07541086779247 36.34049167013332 336.0718243258591 36.34041347517838 336.0682377839257 L36.34041347517838 334.6802043517364 C36.34041347517838 334.19381088000523 36.14698000863832 333.72689316505193 35.803334033653485 333.3826685474005 C35.459132874488574 333.0391528973406 34.9920770151148 332.8456047448666 34.50580604881314 332.8456047448666 L33.930567469276255 332.8456047448666 L33.930567469276255 331.9793714600063 C33.930567469276255 331.7032077311369 33.706421630933235 331.4790488603014 33.43022923058042 331.4790488603014 C33.154039436726094 331.4790488603014 32.929893598383075 331.7032077311369 32.929893598383075 331.9793714600063 L32.929893598383075 332.8456047448666 L27.097788046450273 332.8456047448666 L27.097788046450273 331.9793714600063 C27.097788046450273 331.7032077311369 26.873642208107253 331.4790488603014 26.597449807754426 331.4790488603014 Z M32.666235849307554 338.3118282831274 C30.54883897489083 338.3118282831274 28.83022594468748 340.03028231692235 28.83022594468748 342.1478434007445 C28.83022594468748 344.2652376686626 30.54866694598996 345.98385851836144 32.666235849307554 345.98385851836144 C34.78363272372427 345.98385851836144 36.502245753927625 344.2654044845665 36.502245753927625 342.1478434007445 C36.502245753927625 340.0304491328262 34.78380214612665 338.3118282831274 32.666235849307554 338.3118282831274 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Union_1-f305a" fill="#6C6C6C" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_6" class="richtext manualfit firer click ie-background commentable non-processed" customid="Hours"   datasizewidth="110.0px" datasizeheight="17.0px" dataX="51.1" dataY="331.8" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_6_0">Hours</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_36" class="group firer ie-background commentable non-processed" customid="Products" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_4" class="richtext manualfit firer click ie-background commentable non-processed" customid="My Pags"   datasizewidth="110.0px" datasizeheight="17.0px" dataX="52.1" dataY="294.1" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_4_0">My Pags</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Union_2" class="path firer click commentable non-processed" customid="Product_icn"   datasizewidth="15.9px" datasizeheight="14.8px" dataX="22.0" dataY="293.6"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="15.852066040039062" height="14.80975341796875" viewBox="21.9974394667222 293.55469564502 15.852066040039062 14.80975341796875" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Union_2-f305a" d="M34.742864949807085 294.5887748378672 C35.297572652128 295.73621709227194 35.85452250360947 296.8835995560325 36.40927504891363 298.03104181043744 L24.338545461785486 298.03104181043744 C24.03113186421257 298.03104181043744 23.722791511653213 298.0264977214719 23.414496002077158 298.02440504892223 C23.965720899368648 296.98057998105713 24.515764931435683 295.93795072607753 25.06788668839141 294.8931092172597 C25.12050245535903 294.7917042845583 25.174104767957374 294.69023956121276 25.22761739458923 294.5887748378672 Z M31.138057110757757 299.0684692793645 L31.138057110757757 301.6764773922759 C30.879238359324518 301.4205136441071 30.62001602104243 301.16454989593825 30.360031352045723 300.9078088693938 C30.26019592375684 300.8079584934439 30.127804489656683 300.7586910025561 29.995278526606853 300.7586910025561 C29.86158664599361 300.7586910025561 29.72773034110861 300.8088553531081 29.626998053155432 300.9078088693938 C29.357387090429427 301.17076812294357 29.087806023025564 301.4329500981177 28.817851264094884 301.69507228264746 L28.817851264094884 299.0684692793645 Z M35.50790119104897 299.0684692793645 C35.90446263924554 299.0684692793645 36.30712273315879 299.0838354749446 36.70772004984434 299.0838354749446 C36.74233883288332 299.0838354749446 36.7769426682612 299.08371589365623 36.811516608316936 299.0834767310791 L36.811516608316936 307.3193390276239 L24.338545461785486 307.3193390276239 C23.94198401358892 307.3193390276239 23.539308972014624 307.3039728320436 23.13872660299012 307.3039728320436 C23.104107819951196 307.3039728320436 23.069503984573316 307.30409241333217 23.03491509685648 307.3043315759093 L23.03491509685648 299.0684692793645 L27.780378952184662 299.0684692793645 L27.780378952184662 302.9203021652624 C27.780378952184662 303.2285229365331 28.04127542850665 303.4310936393585 28.30933183448144 303.4310936393585 C28.435295774321276 303.4310936393585 28.56284416623464 303.386370237436 28.66563923141547 303.2868188147074 C29.10634112275278 302.8569838729683 29.54696827578482 302.42924160377896 29.98925461919555 302.00036331234827 C30.42194456419918 302.42930139442336 30.8550530437127 302.8569838729683 31.290284091098272 303.2868188147074 C31.391614285494256 303.38702793452325 31.518176131776954 303.43181112708993 31.64363185114047 303.43181112708993 C31.912121739286192 303.43181112708993 32.17552942266798 303.22666942656036 32.17552942266798 302.9203021652624 L32.17552942266798 299.0684692793645 Z M24.90888841758607 293.55469564502 C24.73256580760011 293.55469564502 24.545734991881716 293.65155648875583 24.45938235387831 293.8106593931888 C23.764181585157075 295.1456051080601 23.058651982636206 296.4770231749189 22.356709818772288 297.8117895178572 C22.295035769195977 297.9306533186888 22.231986534801194 298.049636700809 22.16931099193323 298.1685602922848 C22.064542835491068 298.26434490442347 21.99745773260736 298.40120568918377 21.99745773260736 298.54972564957865 L21.99745773260736 298.6315790416001 C21.99744278494626 298.63331297028407 21.99744278494626 298.6350468989685 21.99745773260736 298.63678082765244 L21.99745773260736 307.83808265740953 C21.99745773260736 308.1181420352238 22.236007455629192 308.35682628719513 22.51618641473192 308.35682628719513 L35.50790119104897 308.35682628719513 C35.9067047884061 308.35682628719513 36.30706294251456 308.3644794896631 36.70689792848543 308.3644794896631 C36.90335503793318 308.3644794896631 37.09966267077016 308.36262597969034 37.295611559741474 308.3571252404167 C37.300155648706834 308.357244821705 37.30469973767225 308.3573644029936 37.30927372195981 308.3573644029936 C37.31623933201854 308.3573644029936 37.323234837399525 308.35718503106085 37.330260238102596 308.35682628719513 C37.59993099147283 308.35682628719513 37.86274076841181 308.11826161651237 37.84898892022716 307.83808265740953 L37.84898892022716 298.54972564957865 C37.84898892022716 298.3642550710178 37.744295502090324 298.196901057674 37.59215820771624 298.10500283741334 C36.99151634292616 296.8626130398897 36.38824368978766 295.6202232423661 35.787990464185384 294.3777138635539 C35.6981400734914 294.18751982409475 35.60467234881844 294.00085343264794 35.51480701046347 293.8106593931888 C35.43533029655242 293.65155648875583 35.23814075171225 293.55469564502 35.06530094675577 293.55469564502 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Union_2-f305a" fill="#6C6C6C" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_37" class="group firer ie-background commentable non-processed" customid="Dashboard" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_2" class="richtext manualfit firer ie-background commentable non-processed" customid="Dashboard"   datasizewidth="110.0px" datasizeheight="17.0px" dataX="52.1" dataY="261.3" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_2_0">Dashboard</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_42" class="path firer commentable non-processed" customid="Dashboard_icn"   datasizewidth="16.3px" datasizeheight="16.3px" dataX="21.4" dataY="259.7"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="17.065711975097656" height="17.066011428833008" viewBox="21.397448047740397 259.6547138974473 17.065711975097656 17.066011428833008" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_42-f305a" d="M29.18647605967584 266.2044320609018 L21.99744652186149 266.2044320609018 L21.99744652186149 260.25471427891705 L29.18647605967584 260.25471427891705 Z M22.98901608581957 265.21283241118766 L28.19490649571773 265.21283241118766 L28.19490649571773 261.24625329779457 L22.98901608581957 261.24625329779457 Z M29.18647605967584 276.1206984111991 L21.99744652186149 276.1206984111991 L21.99744652186149 267.6920567193191 L29.18647605967584 267.6920567193191 Z M22.98901608581957 275.12909876148495 L28.19490649571773 275.12909876148495 L28.19490649571773 268.6835961975211 L22.98901608581957 268.6835961975211 Z M37.86316034166353 268.6835961975211 L30.67413080384921 268.6835961975211 L30.67413080384921 260.25495450564114 L37.86316034166353 260.25495450564114 Z M31.665700367807318 267.69208691990633 L36.87159077770542 267.69208691990633 L36.87159077770542 261.2465843559425 L31.665700367807318 261.2465843559425 Z M37.86316034166353 276.1207286117863 L30.67413080384921 276.1207286117863 L30.67413080384921 270.17101082980156 L37.86316034166353 270.17101082980156 Z M31.665700367807318 275.1291289620722 L36.87159077770542 275.1291289620722 L36.87159077770542 271.162549848679 L31.665700367807318 271.162549848679 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_42-f305a" fill="#685CDB" fill-opacity="1.0" stroke-width="0.2" stroke="#6C6C6C" stroke-linecap="butt"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Path_18" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="181.4px" datasizeheight="3.0px" dataX="-1.0" dataY="233.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="181.3787078857422" height="2.0" viewBox="-0.9999991599279383 232.99999999999997 181.3787078857422 2.0" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_18-f305a" d="M0.0 234.0000000000001 L179.37871580725903 234.0000000000001 "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_18-f305a" fill="none" stroke-width="1.0" stroke="#BCBCBC" stroke-linecap="square" opacity="0.5"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_3" class="group firer ie-background commentable non-processed" customid="User" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_1" class="richtext manualfit firer click ie-background commentable non-processed" customid="Monitor"   datasizewidth="81.2px" datasizeheight="18.0px" dataX="42.2" dataY="203.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_1_0">Monitor</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_14" class="richtext manualfit firer click ie-background commentable non-processed" customid="Jacob Devies"   datasizewidth="86.0px" datasizeheight="16.4px" dataX="39.8" dataY="185.3" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_14_0">Jacob Devies</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Mask_2" class="clippingmask firer ie-background commentable non-processed" customid="User Picture"   datasizewidth="0.0px" datasizeheight="0.0px" dataX="48.8" dataY="109.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="70.00068664550781" height="70.30477905273438" viewBox="47.79848203109616 107.99999576045009 70.00068664550781 70.30477905273438" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <clipPath id="s-Path_26-f305a_clipping">\
              	        <path d="M48.79882535385007 143.1523929162118 C48.715411544389156 161.80424110827738 63.99492415918321 177.22103210935262 82.64677235124857 177.30444591881354 C101.41362270050246 177.3883740348423 116.882753469879 161.91924326546558 116.79882535385009 143.1523929162117 C116.882239163311 124.50054472414622 101.60272654851693 109.083753723071 82.95087835645157 109.00033991361008 C64.18402800719767 108.91641179758135 48.71489723782114 124.38554256695804 48.79882535385006 143.1523929162119 Z " fill="black"></path>\
              	      </clipPath>\
              	    </defs>\
              	    <image xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADIASwDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD2QCnAU0U4V0GA4ClApBSg0gFApwFNBpQaQxwFLTc0uaBi5pM0owaNtIBM0uaMUlAC5opKM0AHNLRmjNABRRRQMWikzRmkAtGaTNFAD6XNMozQA/NGaSjNADqUU3NGaQx1FJmjNADqKQGjNAC0YozRmkMXFGKKBQAUUtFAGbQKMUorUyHUopBSikMKWiigBaUUlKKAClBpKKQx2aMU2lDYpALim1KHU9RQQjCi47EWaXNVr2+tNPiMt3dQ26gZzI4X+dcvcfEjw1bsVXUDKQcZjiLf/rouFmdjRmuFj+Knh0yFHa5UD+Ix9a2LHxv4e1DiLUoFb+7Idh/WgLM6LNFMRw4BUggjII70/NAhaKSloGFFFFAC0UlLQA6im06kACiloxQMKWkxS4oAM0oNJRQA7NGaQUUhj80bqbRQBSxRilpau5mJS0YpcUAJSijFLigYlLRRQAUZoopALupMikqOZgkZZiAFGSTQA24uobWB55nVIkGWZjgCvK/FXxY2M9roGDjg3br/AOgj+prn/iB4xuNbvDY2rlNOiJCgH/WEdWP9K8+kY5OMgepppFJFy+1W7v53nvLmSaRuSztk1QMzMeuBUXPOB9TTSwHrTKJRKSc5JPtU8TZJ4OR+Oapq4b/61PilKvwTQB6F4c8e3+h28NuH82BTxBKxxj0Hp+FevaH410bWo4VjuFiunH/HvIcNn0BPBr5wS7fbtBPTt1qaWY+V50ZaNx14IqeUp2Z7x4/8dr4WtIYLSNZNQuAWCueI19SP5CvJx8Rdea4MzvA5Jzjy8fketcjqOsXeq3AnvZ2llChSxOScDAzTFlKjanbvTUdBI9+8FeN59ZkS3ulPzEIC3VW9M9xx9frXoAyeRXzP4Q1m40/Wbd8loxIpZc+hr6et3SaJJE5DDOfXNZtuLsDinqiPmkqyUGKYUFFyeUiFOFO2Uu2ncVhoop2yjbilcdhKKXFLgUBYSjFLtFLtouFhtFP20bKLjsNop200baLhYoilFGKMVZnYWikpaBi0tJQKQC0YoooAMUlLSZoASqOsRGXRrxQSuYm5H0q9msXxVdG20JsHBkcL+HU/yoBK7seM32g20E+zzN5VQDkd6oTaBCTkd63ZcyyGVu5zVW9uRa25bPzEcVzubb0PS5IqOpxWrWq20pVO3tWOcn1rQ1G5eWRiSTz61lls10x2OGW44DPfFAbac9/WmE0b8ep+tUSWEkJPersc26IxO559DWWrmrUDjfgjNAyJxtY49afCxLYzU80G8EpyfvGq6Icg0xGjayeTOrAng1778MfEE2p6bNZXDl3t8MjH+6TjH514HAhOMA5r3/4UaGbPww16wxJeSZyf7q8D9c1nNmiWh3u/3pDIaf8AZs96T7MfWs9CdRBIacCT2pwgxUgXFAJDBRUmKNtAyOlAFP20baAG4FGBRtoxQAUooAFLigAzRmjFGKAM4UUUoq7mdgxSijFLii47Bil20BTTtvFK47DcU4LTCcUoegQ7bTdtP3Umam47DNlcN8RLwQx21uzBUClmJ7Z/+sP1rvga8x+LWmC4FhcNcMqElTGO+3n8etLmNYR1OGGpQTttifOOgpLuFb2DaSQRVaKW1cB3sFj+bb8nUH8K1I408rchOPesXo7nbH3lZnn+rWJtJjuJIPesdsEcHmux8Q2ck43gY29K5D7NK0m0DJrqhK6OKrG0rIj2E9OTSY5Oa7nQLd9LitoDDCZrpizuy5IHZR796XxXpFvNYvexosdxAwV8DG8H+tT7X3rF/Vnycxw6/Spo2AJOenOKi8pkweoNOKZXk49q1Oexfsz5k+GbCk/N9K9v8BaZ4Z1rQ20q9063mnhywZ1AfYxyMEc8GvCrM7W49u/Wu18O6rNptyt3DKVdWBz7YrOom1oaQPXovhF4YS6EoF5sB5i835T+OM4/Gu7traG0to7e3jWKGNQqIowFA7Vk6FrS6vpMN2owxGHX0Nai3S96zTBroWqKiEyHoacJFPequTYfRSbhRmgQtJS0UwEoxS0lIYmKMUtFACYopaKLAJilpaKBGaFpwWpttJtpcxXKNAp4AxQFpcYpXHYbTSaeaTFFxWIiM0Bal2UBMU7i5RoSnhOKUCndBU3KSKN9d2+nWkl1cyiOCIZZjXiviTxA/iPVWmIZbdPlijP8K/4nrXp/jfS7zVdEENmvmMsgd4wcFgAeleJ39vJZTMjJNBNGcNFKhVh+FQ3c6aUVubENhbx6bwMu77iSfaomHlrtHFTxMzWyE8HGcVUvZxDbsx7CovdnSkooyNWnVIyG59RWBpq+ZekoASBzT7y8Esjbv1o0mSK31Eg4AcDH1rdK0Tlb5pGpdxTyXkNvbozgJukBX5c545q34i3RaUqbt5cruI5yFHJ/M/pV+0IxN50gWMfr9KpajE2oAMrCMKMKMZwKyT1Ohr3X5nGKNwbj1wKqSEhvYGta9tGthnJJU9azJNrSbsbT6V1J3PPkrE4CqQy9j+FaNrL+4mG45wdtZSfMm0ZLdc+1bFpbt5caxguxYYAHODQwSPbvhZdG40e7J+6HXH5V3pIrmvAOhjRPDMEEoIml+dgfeurZFxwK500VJXZBmlDEd6GjI6CmYNWmZtEyzMO9SrcMe1VOalifaeRmmCLSzEjpUgfNMUgjpUg5pJjsLmjNGKTFPUQuaM0mKMYpXGOzRSCjOKpMQ6ikzRmi4itvFG4VCaTNZmpY3Ck3A1CH9aeMHvQA+k5BpMEUc0BYduFGaZSgGkA4HikLYFGw03YaQxu/mvAPiBZ+I7fxVcaleQMyMSLdl5XYOgH+HvXvxBzXP+NYTL4Svtqhiihj8m44B5x6HFCdmUkeK6Pra3sOyUbJVHT1rN1i8Lyna2VHQZq5dadClt9utwySKcurDBx347Vy9zcqZSytweq9aqMU3dGk5NKzI5Gzv3Ae/PSqjSGNsg8rzmiWUbsdeOtUXlIbitkjnbO303VhcqEfqAOa248MuOK4DRZm8/aM/hXc2mQnPSsJqzOqlPmWpT1Oz82Iggc964+6spYZCCp4r0fAYdKqXWlLOCyrkn070QnYmcOY57w14fGt3iW8c4iuGBOxlODjnrXtPhfwPaac0dw0HmSkbkaQ/KPw715pb6PcWktvOFZQZxGjdGyeR/I171o2rC8t44psC5UYPoxHWnJOaunoZfAadtbpDGi53bR1I6k96tHBqHPzc07PvUrTQl6jtgNHlKRSc460oammhakbwDtULQkVc3D1qNiDTvYLXKys6nrxVtXGOtQlcmk2GlcLFnd70Z96rYcd6N7LTuFiwWxRvqDzz3FOEy9xSuwsShs0/NRo6tUmRTTExRRxTc0lO4rFDfSbhUAkNG+nYdycGng4qqHxS+ZRYdy6rU7cKo+YaPNNLlDmLhcZpysKo+ZThJilyhc0A1GQapecfWlE9KzHoWiM1FLF5kTp03qV/MU1Zx3qTzk6ZqbMdz5h8UalcwaxqFm42ASFCoHTb8uP0ripWJkPPWvoD4hfDNtcuJNT0dl+1uS0tuxx5h9QfWvItV8B+JtN/eXGjXaof4lXePzXNbwasTK7ZyzE9zg1EcjrjmtlfDGtTSrFHpV0GbGA0ZX+dW7LwldSSE3Loux9roDk1baSuSotuyF8O2jL+/cfe6CuriJUdarQWiwDYozgelWgm31NcspXZ2whyosJJxzU8NyEfLGqQkCoc9qzJ7tpJBFFlmY7QB1JpJXHex09rcNrPiG1iQ/6NZnzD6Fu1dYLiVLyTy2I2sCMfSsDw5YjTrIbj87nLH1NbPmeWjs33nNdUFaJyTd2dfo3iSK9Y2t2wS4Xox4Df/XreIYV4/M8sU3noSGHORV+28UXVq4milZVYfMpY7QR1yKTimSj1As3rSbmx1rhrPx5MbvbNCJ4CMnYApH09auX3jq3t7gCC3SWHjJMmG/Ko5R6nW7m9aA5HesCw8YaVfKfmaJh1B5xW1BPDdR77eVZV9VOaTVgJfMb1pfMNNxSYqR6jzKaUOWqPFG4igCTqaXYcVEHIpwmYUtR6DihHSlBYdTTPOPpS+eO4paj0JlfAp3mVX3oR6U35fWkFkUc07NNxRXQYjs0ZoxRigLC5ozSYpcUBYXNLmmgU4CgApRQBTZZYraB553WOJBlmboKAJAKGKxqXkZUUdSxwK5PUPHNvDKYLGAytj/WPwv1A6n9K5S81i91KV2uZTKVU7VI4A9h2oGkz0O68V6RZyFPPMzjtEMj8+lc9qfi+7vNy2J+ywA4yP8AWH8e34VxSS+bgs+3GSWPYVZOpRGHdDbzMq/3sLmmrFWNe3mYtJdTsXcKWLMck1xl+hs5hdD7vST3B7/hWpBrD3bm3NuI1bqS+SaZdQOwMTpw2QDSm7qxUNHcqgpIucc1E64ziqtykumuVAJhHf8Au/X2pkd+JDhuK5bNHYpJkd5L5cZ5xV7w/p21ft0y/M/+rB7L6/j/ACqG3sDqd+FYZt0w0h9fQfjXTy7II/nZUUcc1rBGE30Jo5grxqfuqeafJOTIjLyMYrFl1GJMiI72+nFVFncdAR+NbJmNjoh85PXBHSs10+dgRgHr/jVEXc4+67j8aPOuX6yt+dK4WJpB5LbZOTj8KiMrNwOBSlS+C5LEdyaNoApDFikMcmRWzZXkqsFglaJvvAq2ORWIoyc1csnCTEt0IxVRYmejaX4qcMsOoxfJ90XK9j/tCusxkZHI9a8wtpZrdxuIkgkHUjIYdq7/AEO8iutORVPzRDaQT0HapqRtqgiy/g0mD6U8mm7qxuaWG80lOzRmi4WG0lOpKLhYZRmnYpuKLisR7KTbUgajIq7i5SPbRipODSbadxco3FGKdijFFxcomKUClpRRcLABWD4p1KKCxk0/ZvedPmOeEHb8at63rUWi2YcgPPJxHGT1PqfYVwdxfTXZaedt7yNlj/n2xVxFYwLsGNxKPXFWLdhJLn1GKmuId8UgPbkVRs2KS4qS0MlUwz4HQ1ciYKoz0NNvYsqGFRKcoKAGXNt5Moni6Z5xUk8pzFMjHnqM06KfAKtyDSuiOvHSgRZLQXEWJ4g4I6965q+0DE4OmS7lJG6Nzjyx659K21yBipYTmKQfxZFK1yk7FYMNNsEhtgGOcFyOp7mqEm+Zt0rFj6k1fvE2xQE5zvbP5VXxjNMkhSPHRafsOOaduoJNAAqAU8YFR0UAOLelCqWNCing4NAx2McCpI14I74pEjy2TU9uR57Z9eKtIkn0zUXtR5THdCW5U9K7HSr4WF0k4z5LjDgenrXn6x+auYzye1dXo5efQhn78TFee9UtVYTPSgVdQykMpGQR3pMViaFfbUjtXyVkXdEfQ91rbJFc0otM1i7iUmaCwpm8bsVJQ7NGaTNN3Cgdh9FR7qTfRYBgal3Cq+6l3VpYgn3elG6oQ1KGosBMGpd1RA0oNFgJBUF9fQadZtc3BIVeAB1Y9gKe0qRAGR1QE4BY4ya4zxnrEUsqWMJ3G3fdIwPG4jGPw/rTSEcxrOrz6pey3U5xnhUHRF9BRHJvgxVG8GX3D7rjIpbWQqQpqiTWK7hx/EprKKbJz9a1YT8g9jVa4i+YmkxoG+eAiqIO0mrUTYytRSJz9aAI9uelIjYUinim4xn60CJF+/TbJtzXGf7wpy8N9RRZABrj3IoAW/x5MGf75/lVLtV7UMfZ4M/3z/KqOKAEpKUdKAMmgAxSiigUDFHSnRjc+KUDipbZf3uT2poRNL8jBR7GoGYxkkdnz+tTTD92G7hhUEgJcj3NUwGbjHIdvTqK7bRGSTSt6kAu3zD0NcQRzzV/TdRmswyLh42+8h6U4sTR1uoxt5KBCV2HIKnGDWloWvTn/R71jKBwJD94fX1rl01fKbQWHpk5qaK92SB0AyatpSJTaPSTioiQGrK0G/lvLV1mddyNhBn5iuOv0zV+QkSrzXO42djZSuTZpM0wmmbqOUfMSZpM1HupN1HKLmGUtNzRmgB1LTc0oNADhUi1jarr0GmEQj95ctjCdlB7muduPHc8tqEt4FinY4Lg52/T3qlFsTaKuv6t9q1+dHb9zC3lRjsMdT9c1hXCNFK4wWVzkkdjVKWctcZY5LEkk9+avLcs0fBzgcg079CbEIlCkQuflPTPb2pwtyj7hyOxpjCGTJPyn8xU6Sbdu11OBhgT1pDLlsxbd7EVJIuSfcVHblS+VIIYYNTHnFAFB1KNkU0nc3NWpl4NU26mkA3GKT1oBoPFABuqW2GTIQeuP61ARmp7D5pZF/2R/OgQl8S1pblxz5pH6VTIwK0NTBFomO0/9DWcTkUAABIO1SfoKUgjsfyNT2hwHz7VZ8wDPNAGZvA60odT3H51peYuetKNhJ4U8egppAU0AIzkVPAuI2b3q0EQJ91c/QUbQIzjpuq0hXIpF3DHY1FIvzA9OuassOD6Cq+oabqEUytKuIHUNGF5BHv70NARKIWPc+/Spgio4x0NQKnkr8xAPuelSG5i8vOc+/aklYY4MNxq4k6WloZ5OdoJUZ/z3rFkuWP3AAPWpoplWymaZBIOM57VaZLRvQaqszQXEb+XKqhiBx0HUfhXeWN017p1ncvjfIgLY9e9eRQ3O2w80cbhtA9if8K7zw7qqQaV++4UNuJ9M5/oKbXMK9jrCaTNZw1uwlt3mhnEgTkjBB/Ws+fxNCq5jQsTSUGxOdjfLADk0Zri7nxJPJCybdpzwRTR4julUD2q/Ysz9qjr91AaiiuY6xQ1UtQ1ux0xW+0TDeF3eWOSf/10UUC6HmY1N77UZ5rg/NO2SfT0H4VVmzZ3LuV3Kxz/ALpooq7kEE0RkG+M8jmlt52VxngiiipKReaIZDD7pqRYUbuRRRTQy1BAIjlASx9auEcUUU2IhlHy1QbqaKKkRFmhjmiigBAasadj7XKPVM/rRRQBdvo99uo/2/6GsRl2sRRRQBPajKvUvUUUUACjPBp8Y+bmiiqQi5GgZcdxTwmYF9yTRRWgkBTfKiAfedV/M12V/bR3ugpAAPPRy0Z9u4ooo6hLTY8+mtSjFHXoeDjofSqEgzIfbgUUVDLFEQHMhxntT9qLFIpYhHUgn07/ANKKKEIqRkNDFFgvjBA/vEf0rftLwW9m8LNuIO5j2JP9KKKuBmw0qVftEhC5gcbNucdT1FWpoZbe5ML/ADbeQw6MD0NFFbxMZFaYjeaN3A+lFFbLYwe5/9k=" width="127.0" height="84.66666666666667" x="25.298485438842633" y="106.99999999999999" id="s-Image_5-f305a" clip-path="url(#s-Path_26-f305a_clipping)"></image>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_38" class="group firer ie-background commentable non-processed" customid="Logo" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_13" class="richtext manualfit firer ie-background commentable non-processed" customid="Dashboard"   datasizewidth="113.9px" datasizeheight="24.0px" dataX="49.4" dataY="32.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_13_0">Dashboard</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_10" class="path firer commentable non-processed" customid="Logo"   datasizewidth="20.0px" datasizeheight="20.2px" dataX="20.4" dataY="31.7"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="20.0" height="20.189903259277344" viewBox="20.420029526595556 31.692122413438497 20.0 20.189903259277344" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_10-f305a" d="M27.08701347617344 31.692122413438483 L22.64267479269941 31.692122413438483 C21.415400726166922 31.692122413438483 20.42041026608902 32.68711296434165 20.42041026608902 33.91438694004887 L20.42041026608902 38.3587256235229 C20.42041026608902 39.58599969005539 21.41540081699219 40.58099015013329 22.64267479269941 40.58099015013329 L27.08701347617344 40.58099015013329 C28.314287542705927 40.58099015013329 29.309278002783827 39.585999599230135 29.309278002783827 38.3587256235229 L29.309278002783827 33.91438694004887 C29.309278002783827 32.687112873516384 28.31428745188066 31.692122413438483 27.08701347617344 31.692122413438483 Z M22.64267479269941 33.91438694004887 L27.08701347617344 33.91438694004887 L27.08701347617344 38.3587256235229 L22.64267479269941 38.3587256235229 Z M38.197764999985196 42.80287393725021 L33.75342631651114 42.80287393725021 C32.52615224997865 42.80287393725021 31.53116178990075 43.797864488153365 31.53116178990075 45.025138463860586 L31.53116178990075 49.46947714733463 C31.53116178990075 50.69675121386712 32.52615234080392 51.69174167394502 33.75342631651114 51.69174167394502 L38.197764999985196 51.69174167394502 C39.425039066517655 51.69174167394502 40.420029526595584 50.69675112304185 40.420029526595584 49.46947714733463 L40.420029526595584 45.025138463860586 C40.420029526595584 43.79786439732811 39.42503897569239 42.80287393725021 38.197764999985196 42.80287393725021 Z M33.75342631651114 45.025138463860586 L38.197764999985196 45.025138463860586 L38.197764999985196 49.46947714733463 L33.75342631651114 49.46947714733463 Z M35.975690843121555 31.882406421122738 C38.43028656862319 31.882406421122738 40.420029526595584 33.872149379095134 40.420029526595584 36.32674510459677 C40.420029526595584 38.78134083009843 38.43028656862319 40.771274157817544 35.975690843121555 40.771274157817544 C33.52109511761989 40.771274157817544 31.53116178990075 38.78134064844791 31.53116178990075 36.32674510459677 C31.53116178990075 33.872149379095134 33.5210952992704 31.882406421122738 35.975690843121555 31.882406421122738 Z M35.975690843121555 34.10467094773311 C34.74841677658904 34.10467094773311 33.75342631651114 35.099471038064294 33.75342631651114 36.32674510459677 C33.75342631651114 37.554019171129255 34.748416867414306 38.549009631207156 35.975690843121555 38.549009631207156 C37.202964909654014 38.549009631207156 38.197764999985196 37.554019080304 38.197764999985196 36.32674510459677 C38.197764999985196 35.099471038064294 37.202964909654014 34.10467094773311 35.975690843121555 34.10467094773311 Z M24.864368210069586 42.99315794493447 C27.318963935571247 42.99315794493447 29.308897263290362 44.9830914543041 29.308897263290362 47.43768699815523 C29.308897263290362 49.89209217225962 27.31896375392074 51.88202568162927 24.864368210069586 51.88202568162927 C22.40977266621846 51.88202568162927 20.420029526595556 49.89209217225962 20.420029526595556 47.43768699815523 C20.420029526595556 44.98309127265358 22.409772484567952 42.99315794493447 24.864368210069586 42.99315794493447 Z M24.864368210069586 45.21542247154484 C23.637094143537126 45.21542247154484 22.642294053205944 46.21041302244801 22.642294053205944 47.43768699815523 C22.642294053205944 48.6648181965524 23.637094143537126 49.659761155018884 24.864368210069586 49.659761155018884 C26.091642276602073 49.659761155018884 27.086632736680002 48.664770604115716 27.086632736680002 47.43768699815523 C27.086632736680002 46.210412931622756 26.091642185776806 45.21542247154484 24.864368210069586 45.21542247154484 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_10-f305a" fill="#FFFFFF" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
      </div>\
\
\
      <div id="s-Group_46" class="group firer ie-background commentable non-processed" customid="Top bar" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Paragraph_61" class="richtext manualfit firer ie-background commentable non-processed" customid="Together everyone achives"   datasizewidth="273.5px" datasizeheight="15.0px" dataX="333.0" dataY="51.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_61_0">Together everyone achives more</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_15" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="135.5px" datasizeheight="26.0px" dataX="202.0" dataY="43.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_15_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_39" class="group firer ie-background commentable non-processed" customid="Top Icons" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Union_5" class="path firer commentable non-processed" customid="Logout_icn"   datasizewidth="17.6px" datasizeheight="16.0px" dataX="1239.9" dataY="48.4"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="18.471176147460938" height="15.984161376953125" viewBox="1239.9095407089744 48.44858170667348 18.471176147460938 15.984161376953125" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Union_5-f305a" d="M1252.8154555981241 53.03276951919389 C1252.719542369117 53.03276951919389 1252.623601794927 53.06938471909149 1252.550371395132 53.1426151188864 C1252.4040336488652 53.288939192562 1252.4040336488652 53.526213344548864 1252.550371395132 53.672674144138966 L1255.2700139052185 56.38857036416857 L1245.7643497899319 56.38857036416857 C1245.5574014457763 56.38857036416857 1245.3897754746251 56.55619633532007 1245.3897754746251 56.763117334292474 C1245.3897754746251 56.970065678448066 1245.5574014457763 57.13769164959939 1245.7643497899319 57.13769164959939 L1255.2700139052185 57.13769164959939 L1252.5541450303717 59.85358786962894 C1252.4745842207365 59.92123985211322 1252.4268942217661 60.01897153578284 1252.4225736828685 60.123238718164316 C1252.418130090648 60.22750590054625 1252.4574661362437 60.32890183872411 1252.5310246782333 60.40297993918858 C1252.6014795419644 60.47394068880334 1252.697105646551 60.51359120400241 1252.796765165615 60.51359120400241 C1252.8011540674697 60.51359120400241 1252.805556641916 60.51353651363638 1252.8099592163621 60.51337244253915 C1252.914226398744 60.50970818803131 1253.0123409149742 60.46267447345025 1253.0804167477932 60.383646894881394 L1256.4388427303259 57.02921330905133 C1256.4569999317687 57.011575666083615 1256.4733250059576 56.99213324104488 1256.4875718462463 56.97107745021492 L1256.4874351203316 56.971132140581005 C1256.4874351203316 56.971132140581005 1256.496841863248 56.95245538066291 1256.502461298333 56.94304863774664 L1256.5023245724185 56.94304863774664 C1256.5167081386217 56.92070762332088 1256.5268942192504 56.89601492316609 1256.532376928421 56.87000965423226 C1256.532376928421 56.858770784061676 1256.532376928421 56.84936404114569 1256.5417836713373 56.83812517097567 L1256.5416606180142 56.83812517097567 C1256.5513271401678 56.78991561353013 1256.5513271401678 56.74025676139138 1256.5416606180142 56.692047203946174 C1256.5416606180142 56.680808333775815 1256.5416606180142 56.67140159085983 1256.532253875098 56.660162720689584 L1256.532376928421 56.660299446603915 C1256.5268942192504 56.6342941776702 1256.5167081386217 56.60949209678364 1256.5023245724185 56.58715108235799 C1256.5023245724185 56.57774433944172 1256.4929178295024 56.56844697725711 1256.4872983944172 56.5590402343409 L1256.4874351203316 56.5590402343409 C1256.4737215111095 56.53828524052318 1256.4580390487188 56.51881547030109 1256.4405381316656 56.50104110141888 L1253.0804304203848 53.1426151188864 C1253.007268383547 53.06938471909149 1252.9113688271314 53.03276951919389 1252.8154555981241 53.03276951919389 Z M1241.2842517501956 49.44858170667351 C1241.0772897334487 49.44858170667351 1240.9095407089744 49.61620767782483 1240.9095407089744 49.82315602198037 L1240.9095407089744 63.05816985318944 C1240.9095407089744 63.157596938198594 1240.948999807893 63.25284021022475 1241.0192906005268 63.32314467544995 C1241.0895813931604 63.39331241476066 1241.1848246651866 63.432744168496754 1241.2842517501956 63.432744168496754 L1250.7113121372483 63.432744168496754 C1250.9181374280809 63.432744168496754 1251.0858864525553 63.26511819734543 1251.0858864525553 63.05816985318944 C1251.0858864525553 62.85135823494875 1250.9181374280809 62.683595537882525 1250.7113121372483 62.683595537882525 L1241.6588534106854 62.683540847516895 L1241.6588534106854 50.19773033728717 L1250.7113121372483 50.19773033728717 C1250.9181374280809 50.19773033728717 1251.0858864525553 50.029967640221344 1251.0858864525553 49.82315602198037 C1251.0858864525553 49.61620767782483 1250.9181374280809 49.44858170667351 1250.7113121372483 49.44858170667351 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Union_5-f305a" fill="#FFFFFF" fill-opacity="1.0" stroke-width="1.0" stroke="#3C2CD1" stroke-linecap="butt"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_48" class="path firer commentable non-processed" customid="Notification_icn"   datasizewidth="15.7px" datasizeheight="17.9px" dataX="1201.2" dataY="47.2"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="16.24847412109375" height="18.440032958984375" viewBox="1201.2282971653403 47.22064573646523 16.24847412109375 18.440032958984375" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_48-f305a" d="M1212.98055658397 47.97064573646523 C1210.9178378668244 47.97064573646523 1209.2342030373206 49.65218093855483 1209.2342030373206 51.71379648924719 C1209.2342030373206 52.706382723717724 1209.629159163339 53.658754427512385 1210.3317384000513 54.36065745549936 C1211.034175294895 55.06256048348632 1211.9872943444102 55.456804900160336 1212.9805567197275 55.456804900160336 C1213.9738190950447 55.456804900160336 1214.9269026439413 55.06256051742573 1215.6295173133935 54.36065745549936 C1216.3319542082372 53.658754427512385 1216.7267679923866 52.70638265583892 1216.7267679923866 51.71379648924719 C1216.7267679923866 50.721067912907834 1216.3318118663683 49.76855386724432 1215.6295173133935 49.06679324900501 C1214.9269380766812 48.36489022101805 1213.9739613690347 47.97064580434402 1212.9805567197275 47.97064580434402 Z M1208.5281360514882 48.78529173791041 C1205.585431527825 49.01730799987821 1203.276052068348 51.46969810419151 1203.276052068348 54.463008454349996 L1203.276052068348 59.04674896568926 L1202.1240314692564 61.46894246774054 L1202.1241704761644 61.46894246774054 C1202.0507701300414 61.62338746816159 1202.061613808446 61.80452706717405 1202.1529466145876 61.9490963792319 C1202.244279779214 62.09353402336407 1202.4033156442993 62.18111264119971 1202.574166749953 62.18111264119971 L1206.0815466939764 62.18111264119971 C1206.1719083006813 63.701398467113286 1207.443074160358 64.91068013870506 1208.987951258054 64.91068013870506 C1210.5328283557499 64.91068013870506 1211.8031438042149 63.701398331355655 1211.8940712741514 62.18111264119971 L1215.4017001976574 62.18111264119971 C1215.5726900815425 62.18111264119971 1215.7315835877894 62.09339522816317 1215.822920333023 61.948957584030985 C1215.9141147024484 61.8045199398988 1215.9249583766107 61.62324510932303 1215.8515576783664 61.46894601440845 L1214.6996793532649 59.04675251235719 L1214.6996793532649 56.59911341579535 C1214.6996793532649 56.46704973121856 1214.6472688370363 56.34026732367281 1214.5538502104557 56.2468486843648 C1214.4602927929163 56.15329126682561 1214.3336491720868 56.10088074635467 1214.201446700794 56.10088074635467 C1214.0693830162172 56.10088074635467 1213.9426006086715 56.153291262583174 1213.8491819693634 56.2468486843648 C1213.7556245518242 56.340267306703126 1213.7032140313531 56.467049722733705 1213.7032140313531 56.59911341579535 L1213.7032140313531 59.15924643845404 C1213.7034920807655 59.233201956895805 1213.7201738707224 59.30632472110177 1213.7521475954352 59.373191103687184 L1214.6139121494073 61.18483614117341 L1211.9003235957707 61.18483614117341 L1211.9003235957707 61.184697098668906 L1210.9038582908286 61.184697098668906 L1210.9038582908286 61.18483614117341 L1207.0735169324703 61.18483614117341 L1207.0735169324703 61.184697098668906 L1206.0770516275284 61.184697098668906 L1206.0770516275284 61.18483614117341 L1203.3637478933867 61.18483614117341 L1204.2252277636212 59.373191103687184 C1204.257201488334 59.30632472110177 1204.2738837415438 59.233201965380644 1204.2741613277033 59.15924643845404 L1204.2741613277033 54.46304756951544 C1204.2741613277033 51.979962700338604 1206.1706127953719 49.97112428358095 1208.6084295563169 49.778949049125764 C1208.882706623628 49.75726240504392 1209.0876226092844 49.517458754850026 1209.0660562549422 49.24317099662579 C1209.055630024868 49.11138490245081 1208.993349876339 48.98918977896193 1208.8928421181442 48.90327666959783 C1208.7923343599498 48.81750235543466 1208.6617974056132 48.77510276337509 1208.5300219938663 48.785530060419454 Z M1212.98055658397 48.96601140465222 C1213.7102538812426 48.96601140465222 1214.4094878464616 49.25544187372024 1214.9252654069949 49.77076393348544 C1215.4410073650913 50.28597209260403 1215.7304449614346 50.98424513142028 1215.7304449614346 51.71280362586356 C1215.7304449614346 52.441255346935456 1215.4410144923665 53.13938600994349 1214.9252654069949 53.65484331824168 C1214.4095234488984 54.17005147736023 1213.7102539491216 54.459453437327205 1212.98055658397 54.459453437327205 C1212.251001628566 54.459453437327205 1211.5518744367184 54.17002296825919 1211.0361324446824 53.65484331824168 C1210.520390486586 53.139528419691146 1210.2309528902426 52.44121971055916 1210.2309528902426 51.71280362586356 C1210.2309528902426 50.19016905729589 1211.4559646619782 48.96597590403351 1212.9806988579599 48.96597590403351 Z M1207.0804145762486 62.17985267469069 L1210.895453063706 62.17985267469069 C1210.8088459953071 63.160623651622885 1209.9993460393298 63.913097277001704 1208.9880049569722 63.913097277001704 C1207.9768062164835 63.913097277001704 1207.1670358143494 63.16058801524656 1207.0804144404908 62.17985267469069 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_48-f305a" fill="#3C2CD1" fill-opacity="1.0" stroke-width="0.5" stroke="#3C2CD1" stroke-linecap="butt"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
      </div>\
\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;