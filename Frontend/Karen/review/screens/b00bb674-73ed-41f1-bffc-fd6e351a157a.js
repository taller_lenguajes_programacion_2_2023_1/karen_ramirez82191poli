var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1280" deviceHeight="800">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1280" height="870">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1677255252909.css" />\
      <div class="freeLayout">\
      <div id="t-Rectangle_28" class="rectangle manualfit firer ie-background commentable non-processed" customid="Background" rotationdeg="180.0"  datasizewidth="1280.0px" datasizeheight="800.0px" datasizewidthpx="1280.0" datasizeheightpx="800.0000000000003" dataX="0.0" dataY="0.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-t-Rectangle_28_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-b00bb674-73ed-41f1-bffc-fd6e351a157a" class="screen growth-vertical devWeb canvas PORTRAIT firer ie-background commentable non-processed" alignment="left" name="Ingreso" width="1280" height="800">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/b00bb674-73ed-41f1-bffc-fd6e351a157a-1677255252909.css" />\
      <div class="freeLayout">\
      <div id="s-Group_1" class="group firer ie-background commentable non-processed" customid="Card" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Rectangle_6" class="rectangle manualfit firer commentable non-processed" customid="Rectangle "   datasizewidth="920.0px" datasizeheight="562.2px" datasizewidthpx="919.9999999999997" datasizeheightpx="562.2222222222227" dataX="180.0" dataY="118.9" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_6_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Button_1" class="button multiline manualfit firer click commentable non-processed" customid="Button Filled"   datasizewidth="302.0px" datasizeheight="39.6px" dataX="721.1" dataY="483.2" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Button_1_0">SING IN</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Input_22" class="text firer focusin focusout commentable non-processed" customid="Password"  datasizewidth="302.0px" datasizeheight="46.0px" dataX="721.1" dataY="378.4" ><div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder="Password"/></div></div>  </div></div></div>\
        <div id="s-Input_21" class="text firer focusin focusout commentable non-processed" customid="Email Adress"  datasizewidth="302.0px" datasizeheight="46.0px" dataX="721.1" dataY="309.6" ><div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder="Email Adress"/></div></div>  </div></div></div>\
        <div id="s-Paragraph_18" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="215.7px" datasizeheight="13.0px" dataX="721.1" dataY="235.2" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_18_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_16" class="richtext manualfit firer ie-background commentable non-processed" customid="Sing IN"   datasizewidth="260.1px" datasizeheight="29.0px" dataX="723.1" dataY="204.8" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_16_0">Sing IN </span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_17" class="richtext manualfit firer click ie-background commentable non-processed" customid="Don&acute;t Have an account? Si"   datasizewidth="161.5px" datasizeheight="28.0px" dataX="881.3" dataY="159.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_17_0">Don&acute;t Have an account? Sign up</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_2" class="group firer ie-background commentable non-processed" customid="Go back" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Path_5" class="path firer click commentable non-processed" customid="Arrow left"   datasizewidth="16.6px" datasizeheight="13.6px" dataX="718.0" dataY="160.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="16.637741088867188" height="13.631800651550293" viewBox="717.9999999999998 159.99999999999997 16.637741088867188 13.631800651550293" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_5-b00bb" d="M717.9999999999998 166.82030010223386 C717.9999999999998 167.08399963378903 718.1142597198484 167.34770011901853 718.3163995742796 167.5410003662109 L724.1171393394468 173.33299922943112 C724.3281397819517 173.5351991653442 724.5566401481626 173.63180065155026 724.8115401268003 173.63180065155026 C725.3652396202085 173.63180065155026 725.7695393562315 173.2363004684448 725.7695393562315 172.70020008087155 C725.7695393562315 172.4189004898071 725.6640400886533 172.18159961700437 725.4794402122495 172.00590038299558 L723.5019402503965 170.00190067291257 L720.953119754791 167.67280006408689 L723.0009398460386 167.7959003448486 L733.6533389091489 167.7959003448486 C734.2334399223325 167.7959003448486 734.6377406120298 167.39159965515134 734.6377406120298 166.82030010223386 C734.6377406120298 166.24020004272458 734.2334399223325 165.83590030670163 733.6533389091489 165.83590030670163 L723.0009398460386 165.83590030670163 L720.9619097709653 165.95899963378903 L723.5019402503965 163.62989997863767 L725.4794402122495 161.62596988677976 C725.6640400886533 161.45018959045407 725.7695393562315 161.21288967132566 725.7695393562315 160.9316401481628 C725.7695393562315 160.39549970626828 725.3652396202085 159.99999999999997 724.8115401268003 159.99999999999997 C724.5566401481626 159.99999999999997 724.3193402290342 160.09667968749997 724.0908398628233 160.31640005111691 L718.3163995742796 166.09080028533933 C718.1142597198484 166.28419971466062 717.9999999999998 166.54780006408689 717.9999999999998 166.82030010223386 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_5-b00bb" fill="#8E8E93" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_15" class="richtext manualfit firer click ie-background commentable non-processed" customid="Go back"   datasizewidth="49.9px" datasizeheight="15.0px" dataX="741.0" dataY="159.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_15_0">Go back</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
      </div>\
\
\
      <div id="s-Image_1" class="image firer ie-background commentable non-processed" customid="Image 1"   datasizewidth="455.0px" datasizeheight="453.0px" dataX="216.0" dataY="158.0"   alt="image">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
        		<img src="./images/2f163bf0-1421-4116-a9bd-1d5383f1c1ea.jfif" />\
        	</div>\
        </div>\
      </div>\
\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;