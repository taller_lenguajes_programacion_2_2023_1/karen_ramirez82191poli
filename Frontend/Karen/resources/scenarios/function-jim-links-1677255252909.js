(function(window, undefined) {

  var jimLinks = {
    "b00bb674-73ed-41f1-bffc-fd6e351a157a" : {
      "Button_1" : [
        "f305af33-bfa4-400d-a093-dac3e98c8d4a"
      ],
      "Paragraph_17" : [
        "3edcb03a-862a-4b39-9342-843b05ecb2f5"
      ],
      "Path_5" : [
        "3459375f-7ebf-46d3-85a5-c94ba9ffd713"
      ],
      "Paragraph_15" : [
        "3459375f-7ebf-46d3-85a5-c94ba9ffd713"
      ]
    },
    "3459375f-7ebf-46d3-85a5-c94ba9ffd713" : {
    },
    "3edcb03a-862a-4b39-9342-843b05ecb2f5" : {
      "Button_1" : [
        "f305af33-bfa4-400d-a093-dac3e98c8d4a"
      ],
      "Path_5" : [
        "3459375f-7ebf-46d3-85a5-c94ba9ffd713"
      ],
      "Paragraph_15" : [
        "3459375f-7ebf-46d3-85a5-c94ba9ffd713"
      ]
    },
    "8ab921b5-bc06-4878-8244-e825bf936254" : {
      "Union_5" : [
        "b00bb674-73ed-41f1-bffc-fd6e351a157a"
      ]
    },
    "d12245cc-1680-458d-89dd-4f0d7fb22724" : {
    },
    "f305af33-bfa4-400d-a093-dac3e98c8d4a" : {
      "Union_1" : [
        "ae13c4b1-02df-4827-bc48-80c2b10ef78a"
      ],
      "Paragraph_6" : [
        "ae13c4b1-02df-4827-bc48-80c2b10ef78a"
      ],
      "Paragraph_4" : [
        "8ab921b5-bc06-4878-8244-e825bf936254"
      ],
      "Union_2" : [
        "8ab921b5-bc06-4878-8244-e825bf936254"
      ],
      "Paragraph_1" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ],
      "Paragraph_14" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ],
      "Image_5" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ],
      "Path_26" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ]
    }    
  }

  window.jimLinks = jimLinks;
})(window);