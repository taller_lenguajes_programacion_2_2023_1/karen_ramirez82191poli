(function(window, undefined) {
  var dictionary = {
    "b00bb674-73ed-41f1-bffc-fd6e351a157a": "Ingreso",
    "ae13c4b1-02df-4827-bc48-80c2b10ef78a": "Hours ",
    "3459375f-7ebf-46d3-85a5-c94ba9ffd713": "Principal inicio",
    "3edcb03a-862a-4b39-9342-843b05ecb2f5": "Welcome",
    "8ab921b5-bc06-4878-8244-e825bf936254": "My pags ",
    "d12245cc-1680-458d-89dd-4f0d7fb22724": "Profile",
    "f305af33-bfa4-400d-a093-dac3e98c8d4a": "Dashboard",
    "f39803f7-df02-4169-93eb-7547fb8c961a": "Template 1",
    "bb8abf58-f55e-472d-af05-a7d1bb0cc014": "default"
  };

  var uriRE = /^(\/#)?(screens|templates|masters|scenarios)\/(.*)(\.html)?/;
  window.lookUpURL = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, url;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      url = folder + "/" + canvas;
    }
    return url;
  };

  window.lookUpName = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, canvasName;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      canvasName = dictionary[canvas];
    }
    return canvasName;
  };
})(window);