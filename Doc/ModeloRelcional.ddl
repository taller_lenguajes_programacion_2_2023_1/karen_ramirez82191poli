-- Generado por Oracle SQL Developer Data Modeler 22.2.0.165.1149
--   en:        2023-02-24 03:05:01 COT
--   sitio:      Oracle Database 11g
--   tipo:      Oracle Database 11g



-- predefined type, no DDL - MDSYS.SDO_GEOMETRY

-- predefined type, no DDL - XMLTYPE

CREATE TABLE modelo (
    nickname         NVARCHAR2(100) NOT NULL,
    produccion_total INTEGER NOT NULL,
    horas_totales    DATE NOT NULL
);

ALTER TABLE modelo ADD CONSTRAINT modelo_pk PRIMARY KEY ( nickname );

CREATE TABLE monitor (
    id_monitor NVARCHAR2(15) NOT NULL,
    modelos    NVARCHAR2(100) NOT NULL
);

ALTER TABLE monitor ADD CONSTRAINT monitor_pk PRIMARY KEY ( id_monitor );

CREATE TABLE pagina (
    id_pagina       NVARCHAR2(100) NOT NULL,
    produccion      INTEGER NOT NULL,
    horas           DATE NOT NULL,
    nickname        NVARCHAR2(100) NOT NULL,
    modelo_nickname NVARCHAR2(100) NOT NULL
);

ALTER TABLE pagina ADD CONSTRAINT pagina_pk PRIMARY KEY ( id_pagina );

CREATE TABLE persona (
    id_persona NVARCHAR2(15) NOT NULL,
    nombre     NVARCHAR2(100) NOT NULL,
    apellido   NVARCHAR2(100) NOT NULL,
    direccion  NVARCHAR2(50) NOT NULL,
    movil      NVARCHAR2(100) NOT NULL,
    password   NVARCHAR2(20) NOT NULL
);

COMMENT ON COLUMN persona.direccion IS
    '																																																																																																																															'
    ;

ALTER TABLE persona ADD CONSTRAINT persona_pk PRIMARY KEY ( id_persona );

CREATE TABLE producto (
    id_producto NVARCHAR2(15) NOT NULL,
    nombre      NVARCHAR2(100) NOT NULL,
    detalle     NVARCHAR2(1),
    imagen      NVARCHAR2(1000)
);

COMMENT ON COLUMN producto.id_producto IS
    '			';

ALTER TABLE producto ADD CONSTRAINT producto_pk PRIMARY KEY ( id_producto );

CREATE TABLE relation_10 (
    persona_id_persona NVARCHAR2(15) NOT NULL,
    venta_id_venta     INTEGER NOT NULL,
    venta_fecha        NVARCHAR2(100) NOT NULL
);

ALTER TABLE relation_10
    ADD CONSTRAINT relation_10_pk PRIMARY KEY ( persona_id_persona,
                                                venta_id_venta,
                                                venta_fecha );

CREATE TABLE relation_11 (
    venta_id_venta       INTEGER NOT NULL,
    venta_fecha          NVARCHAR2(100) NOT NULL,
    producto_id_producto NVARCHAR2(15) NOT NULL
);

ALTER TABLE relation_11
    ADD CONSTRAINT relation_11_pk PRIMARY KEY ( venta_id_venta,
                                                venta_fecha,
                                                producto_id_producto );

CREATE TABLE rol (
    id_rol             NVARCHAR2(15) NOT NULL,
    horario            DATE NOT NULL,
    persona_id_persona NVARCHAR2(15),
    modelo_nickname    NVARCHAR2(100),
    monitor_id_monitor NVARCHAR2(15)
);

ALTER TABLE rol ADD CONSTRAINT rol_pk PRIMARY KEY ( id_rol );

CREATE TABLE venta (
    id_venta INTEGER NOT NULL,
    fecha    NVARCHAR2(100) NOT NULL,
    cantidad INTEGER NOT NULL,
    valor    NUMBER NOT NULL
);

ALTER TABLE venta ADD CONSTRAINT venta_pk PRIMARY KEY ( id_venta,
                                                        fecha );

ALTER TABLE pagina
    ADD CONSTRAINT pagina_modelo_fk FOREIGN KEY ( modelo_nickname )
        REFERENCES modelo ( nickname );

ALTER TABLE relation_10
    ADD CONSTRAINT relation_10_persona_fk FOREIGN KEY ( persona_id_persona )
        REFERENCES persona ( id_persona );

ALTER TABLE relation_10
    ADD CONSTRAINT relation_10_venta_fk FOREIGN KEY ( venta_id_venta,
                                                      venta_fecha )
        REFERENCES venta ( id_venta,
                           fecha );

ALTER TABLE relation_11
    ADD CONSTRAINT relation_11_producto_fk FOREIGN KEY ( producto_id_producto )
        REFERENCES producto ( id_producto );

ALTER TABLE relation_11
    ADD CONSTRAINT relation_11_venta_fk FOREIGN KEY ( venta_id_venta,
                                                      venta_fecha )
        REFERENCES venta ( id_venta,
                           fecha );

ALTER TABLE rol
    ADD CONSTRAINT rol_modelo_fk FOREIGN KEY ( modelo_nickname )
        REFERENCES modelo ( nickname );

ALTER TABLE rol
    ADD CONSTRAINT rol_monitor_fk FOREIGN KEY ( monitor_id_monitor )
        REFERENCES monitor ( id_monitor );

ALTER TABLE rol
    ADD CONSTRAINT rol_persona_fk FOREIGN KEY ( persona_id_persona )
        REFERENCES persona ( id_persona );



-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                             9
-- CREATE INDEX                             0
-- ALTER TABLE                             17
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE MATERIALIZED VIEW LOG             0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
